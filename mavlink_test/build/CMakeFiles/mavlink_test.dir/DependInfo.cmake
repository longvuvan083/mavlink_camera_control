
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/root/mavlink_test/camera-control/PlatformController/Codegen/Platform_Controller_ert_rtw/Platform_Controller.cpp" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/Platform_Controller_ert_rtw/Platform_Controller.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/Platform_Controller_ert_rtw/Platform_Controller.cpp.o.d"
  "/root/mavlink_test/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/MWDSP_EPH_R_B.cpp" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/MWDSP_EPH_R_B.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/MWDSP_EPH_R_B.cpp.o.d"
  "/root/mavlink_test/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/binSearch_s32_s32.cpp" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/binSearch_s32_s32.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/binSearch_s32_s32.cpp.o.d"
  "/root/mavlink_test/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/const_params.cpp" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/const_params.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/camera-control/PlatformController/Codegen/slprj/ert/_sharedutils/const_params.cpp.o.d"
  "/root/mavlink_test/main.cpp" "CMakeFiles/mavlink_test.dir/main.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/main.cpp.o.d"
  "/root/mavlink_test/tcp-client/client.cpp" "CMakeFiles/mavlink_test.dir/tcp-client/client.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/tcp-client/client.cpp.o.d"
  "/root/mavlink_test/tcp-client/mavlink_camera.cpp" "CMakeFiles/mavlink_test.dir/tcp-client/mavlink_camera.cpp.o" "gcc" "CMakeFiles/mavlink_test.dir/tcp-client/mavlink_camera.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
