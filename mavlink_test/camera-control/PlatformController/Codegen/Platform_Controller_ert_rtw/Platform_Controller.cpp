#include "Platform_Controller.h"
#include "rtwtypes.h"
#include <math.h>
#include "MWDSP_EPH_R_B.h"
#include "binSearch_s32_s32.h"
#include <string.h>
#include "zero_crossing_types.h"

#define IN_ABSOLUTE                    ((uint8_T)1U)
#define IN_ACTIVE                      ((uint8_T)1U)
#define IN_DONE                        ((uint8_T)2U)
#define IN_MANUAL                      ((uint8_T)2U)
#define IN_NO_ACTIVE_CHILD             ((uint8_T)0U)
#define IN_STANDBY                     ((uint8_T)3U)
#define IN_TEST                        ((uint8_T)4U)
#define IN_TRACK                       ((uint8_T)5U)

real32_T sig_in_TBE[2];
real32_T sig_in_Enc_LOS[2];
Enum_TrackState sig_in_TrackState;
Timing_type sig_in_Timing;
struct_e3pw6ZVcvAHrsIk2w3e97G Controller_param =
{
    0.724F,
    0.25F,
    0.009F,
    0.5F,
    255U,
    74.0F,
    true,
    14.0F,
    false,
    X_AXIS
} ;

DW rtDW;
PrevZCX rtPrevZCX;
ExtY rtY;
extern const int32_T rtCP_pooled_gKnWb2puVIgY[8];

#define rtCP_Histogram_BIN_BOUNDARY    rtCP_pooled_gKnWb2puVIgY

static void PlatCon_Embedded(real32_T rtu_u, real32_T *rty_y1, int16_T *rty_y2);
static void PlatCon_OutputScaling(const real32_T rtu_deg_per_sec[2], real32_T rty_rounding_error[2], int16_T
    rty_Rates_rounded[2]);
static void PlatCon_PIDController_Init(void);
static void PlatCon_PIDController(boolean_T rtu_reset, const real32_T rtu_TBE[2], real32_T rty_Out[2]);
static void PlatCon_TBERateProfiling_Init(void);
static void PlatCon_TBERateProfiling(boolean_T rtu_enable, const real32_T rtu_TBE[2], real32_T rty_Bled_TBE[2]);
static void PlatCon_TimingCheckModule_Init(uint32_T rty_timestamp_stats[8]);
static void PlatCon_TimingCheckModule(const Timing_type *rtu_sig_in_timing, uint32_T rty_timestamp_stats[8], real32_T
    *rty_timestamp_var, boolean_T *rty_frame_count_skipped);
static void PlatCon_TimingCheckModule_Term(void);
static void PlatCon_eML_blk_kernel(const real32_T TBE[2], boolean_T enable, real32_T BleedRate, real32_T TBE_Offset[2]);
static void PlatCon_SystemCore_setup(dsp_simulink_MovingVariance *obj);
static void PlatCon_eML_blk_kernel_k(int32_T framecount, boolean_T *rst, boolean_T *y);
static void PlatCon_eML_blk_kernel_n(boolean_T *test_done, real32_T *test_rate);
static void PlatCon_eML_blk_kernel_h(real32_T rate_val, Enum_AxesToTest sel_axes, real32_T y[2]);
const Timing_type Platform_Controller_rtZTiming_t =
{
    0,
    0.0F
};

static void PlatCon_Embedded(real32_T rtu_u, real32_T *rty_y1, int16_T *rty_y2)
{
    *rty_y1 = rtu_u;
    *rty_y2 = 0;
    if (rtu_u > 1.0F)
    {
        *rty_y1 = 0.0F;
        *rty_y2 = 1;
    }
    else if (rtu_u < -1.0F)
    {
        *rty_y1 = 0.0F;
        *rty_y2 = -1;
    }
}

static void PlatCon_OutputScaling(const real32_T rtu_deg_per_sec[2], real32_T rty_rounding_error[2], int16_T
    rty_Rates_rounded[2])
{
    real32_T u0;
    rtDW.NoOfLevels[0] = (real32_T)Controller_param.NoOfRateLevels * rtu_deg_per_sec[0];
    rtDW.Product[0] = rtDW.NoOfLevels[0] / Controller_param.PlatformMaxRate;
    u0 = rtDW.Product[0];
    if (u0 > 255.0F)
    {
        u0 = 255.0F;
    }
    else if (u0 < -255.0F)
    {
        u0 = -255.0F;
    }

    rtDW.Saturation[0] = u0;
    rtDW.rounded[0] = roundf(rtDW.Saturation[0]);
    rty_rounding_error[0] = rtDW.Saturation[0] - rtDW.rounded[0];
    rty_Rates_rounded[0] = (int16_T)rtDW.rounded[0];
    rtDW.NoOfLevels[1] = (real32_T)Controller_param.NoOfRateLevels * rtu_deg_per_sec[1];
    rtDW.Product[1] = rtDW.NoOfLevels[1] / Controller_param.PlatformMaxRate;
    u0 = rtDW.Product[1];
    if (u0 > 255.0F)
    {
        u0 = 255.0F;
    }
    else if (u0 < -255.0F)
    {
        u0 = -255.0F;
    }

    rtDW.Saturation[1] = u0;
    rtDW.rounded[1] = roundf(rtDW.Saturation[1]);
    rty_rounding_error[1] = rtDW.Saturation[1] - rtDW.rounded[1];
    rty_Rates_rounded[1] = (int16_T)rtDW.rounded[1];
}

static void PlatCon_PIDController_Init(void)
{
    rtDW.Constant = 0.0F;
    rtDW.icLoad_g = true;
    rtDW.Integrator_DSTATE = 0.0F;
    rtDW.Integrator_PrevResetState = 2;
    rtDW.icLoad_k = true;
    rtDW.Integrator_DSTATE_e = 0.0F;
    rtDW.Integrator_PrevResetState_o = 2;
}

static void PlatCon_PIDController(boolean_T rtu_reset, const real32_T rtu_TBE[2], real32_T rty_Out[2])
{
    real32_T u0;
    boolean_T zcEvent;
    rtDW.PlatformGain[0] = Controller_param.Control_Gain * rtu_TBE[0];
    rtDW.PlatformGain[1] = Controller_param.Control_Gain * rtu_TBE[1];
    rtDW.Constant = 0.0F;
    if (rtu_reset)
    {
        rtDW.Switch = 0.0F;
    }
    else
    {
        rtDW.Switch = rtDW.PlatformGain[0];
    }

    rtDW.DerivativeGain = Controller_param.PID_D * rtDW.Switch;
    rtDW.Tsamp = rtDW.DerivativeGain * 30.0F;
    zcEvent = (rtu_reset && (rtPrevZCX.UD_Reset_ZCE != POS_ZCSIG));
    rtDW.icLoad_g = (zcEvent || rtDW.icLoad_g);
    rtPrevZCX.UD_Reset_ZCE = rtu_reset;
    if (rtDW.icLoad_g)
    {
        rtDW.UD_DSTATE = 0.0F;
    }

    rtDW.UD = rtDW.UD_DSTATE;
    rtDW.Diff = rtDW.Tsamp - rtDW.UD;
    if (rtu_reset && (rtDW.Integrator_PrevResetState <= 0))
    {
        rtDW.Integrator_DSTATE = 0.0F;
    }

    rtDW.Integrator = rtDW.Integrator_DSTATE;
    rtDW.ProportionalGain = Controller_param.PID_P * rtDW.Switch;
    rtDW.Sum_f = (rtDW.ProportionalGain + rtDW.Integrator) + rtDW.Diff;
    u0 = rtDW.Sum_f;
    if (u0 > 2.0F)
    {
        u0 = 2.0F;
    }
    else if (u0 < -2.0F)
    {
        u0 = -2.0F;
    }

    rty_Out[0] = u0;
    rtDW.IntegralGain = Controller_param.PID_I * rtDW.Switch;
    if (rtu_reset)
    {
        rtDW.Switch1_m = 0.0F;
    }
    else
    {
        rtDW.Switch1_m = rtDW.PlatformGain[1];
    }

    rtDW.DerivativeGain_g = Controller_param.PID_D * rtDW.Switch1_m;
    rtDW.Tsamp_f = rtDW.DerivativeGain_g * 30.0F;
    zcEvent = (rtu_reset && (rtPrevZCX.UD_Reset_ZCE_j != POS_ZCSIG));
    rtDW.icLoad_k = (zcEvent || rtDW.icLoad_k);
    rtPrevZCX.UD_Reset_ZCE_j = rtu_reset;
    if (rtDW.icLoad_k)
    {
        rtDW.UD_DSTATE_g = 0.0F;
    }

    rtDW.UD_n = rtDW.UD_DSTATE_g;
    rtDW.Diff_j = rtDW.Tsamp_f - rtDW.UD_n;
    if (rtu_reset && (rtDW.Integrator_PrevResetState_o <= 0))
    {
        rtDW.Integrator_DSTATE_e = 0.0F;
    }

    rtDW.Integrator_m = rtDW.Integrator_DSTATE_e;
    rtDW.ProportionalGain_c = Controller_param.PID_P * rtDW.Switch1_m;
    rtDW.Sum_k = (rtDW.ProportionalGain_c + rtDW.Integrator_m) + rtDW.Diff_j;
    u0 = rtDW.Sum_k;
    if (u0 > 2.0F)
    {
        u0 = 2.0F;
    }
    else if (u0 < -2.0F)
    {
        u0 = -2.0F;
    }

    rty_Out[1] = u0;
    rtDW.IntegralGain_b = Controller_param.PID_I * rtDW.Switch1_m;
    rtDW.icLoad_g = false;
    rtDW.UD_DSTATE = rtDW.Tsamp;
    rtDW.Integrator_DSTATE += 0.0333333351F * rtDW.IntegralGain;
    rtDW.Integrator_PrevResetState = (int8_T)rtu_reset;
    rtDW.icLoad_k = false;
    rtDW.UD_DSTATE_g = rtDW.Tsamp_f;
    rtDW.Integrator_DSTATE_e += 0.0333333351F * rtDW.IntegralGain_b;
    rtDW.Integrator_PrevResetState_o = (int8_T)rtu_reset;
}

static void PlatCon_eML_blk_kernel(const real32_T TBE[2], boolean_T enable, real32_T BleedRate, real32_T TBE_Offset[2])
{
    if (enable)
    {
        if (!rtDW.active)
        {
            rtDW.TBE_Store[0] = TBE[0];
            TBE_Offset[0] = rtDW.TBE_Store[0];
            rtDW.TBE_Store[1] = TBE[1];
            TBE_Offset[1] = rtDW.TBE_Store[1];
            rtDW.nCount = BleedRate;
        }
        else
        {
            TBE_Offset[0] = rtDW.TBE_Store[0] * rtDW.nCount / BleedRate;
            TBE_Offset[1] = rtDW.TBE_Store[1] * rtDW.nCount / BleedRate;
            rtDW.nCount--;
            if (rtDW.nCount <= 0.0F)
            {
                rtDW.nCount = 0.0F;
            }
        }
    }
    else
    {
        TBE_Offset[0] = 0.0F;
        TBE_Offset[1] = 0.0F;
    }

    rtDW.active = enable;
}

static void PlatCon_TBERateProfiling_Init(void)
{
    rtDW.active = false;
    rtDW.TBE_Store[0] = 0.0F;
    rtDW.TBE_Store[1] = 0.0F;
    rtDW.nCount = 0.0F;
}

static void PlatCon_TBERateProfiling(boolean_T rtu_enable, const real32_T rtu_TBE[2], real32_T rty_Bled_TBE[2])
{
    PlatCon_eML_blk_kernel(rtu_TBE, rtu_enable, Controller_param.BleedRate, rtDW.TBE_Offset);
    if (Controller_param.BleedEnable)
    {
        rtDW.Sum[0] = rtu_TBE[0] - rtDW.TBE_Offset[0];
        rty_Bled_TBE[0] = rtDW.Sum[0];
        rtDW.Sum[1] = rtu_TBE[1] - rtDW.TBE_Offset[1];
        rty_Bled_TBE[1] = rtDW.Sum[1];
    }
    else
    {
        rty_Bled_TBE[0] = rtu_TBE[0];
        rty_Bled_TBE[1] = rtu_TBE[1];
    }
}

static void PlatCon_SystemCore_setup(dsp_simulink_MovingVariance *obj)
{
    dsp_simulink_MovingVariance *obj_0;
    dsp_simulink_MovingVariance *obj_1;
    g_dsp_internal_ExponentialMovin *iobj_0;
    g_dsp_internal_ExponentialMovin *obj_2;
    g_dsp_internal_ExponentialMovin *obj_3;
    g_dsp_internal_ExponentialMovin *obj_4;
    real32_T varargin_2;
    boolean_T flag;
    obj->isSetupComplete = false;
    obj->isInitialized = 1;
    obj_0 = obj;
    obj_0->NumChannels = 1;
    obj_1 = obj_0;
    iobj_0 = &obj_0->_pobj0;
    varargin_2 = obj_1->ForgettingFactor;
    iobj_0->isInitialized = 0;
    iobj_0->isInitialized = 0;
    obj_2 = iobj_0;
    obj_3 = obj_2;
    obj_4 = obj_3;
    flag = (obj_4->isInitialized == 1);
    if (flag)
    {
        obj_3->TunablePropsChanged = true;
    }

    obj_2->ForgettingFactor = varargin_2;
    obj_0->pStatistic = iobj_0;
    obj->isSetupComplete = true;
    obj->TunablePropsChanged = false;
}

static void PlatCon_eML_blk_kernel_k(int32_T framecount, boolean_T *rst, boolean_T *y)
{
    if (!rtDW.fault_frames_not_empty)
    {
        rtDW.fault_frames_not_empty = true;
        rtDW.count_prev = framecount - 1;
        rtDW.count_g = MAX_int8_T;
    }

    rtDW.fault_frames = (rtDW.fault_frames || (framecount - rtDW.count_prev != 1));
    rtDW.count_g--;
    *rst = false;
    if (rtDW.count_g <= 0)
    {
        rtDW.count_g = MAX_int8_T;
        rtDW.fault_frames = false;
        *rst = true;
    }

    *y = rtDW.fault_frames;
}

static void PlatCon_TimingCheckModule_Init(uint32_T rty_timestamp_stats[8])
{
    dsp_simulink_MovingVariance *b_obj;
    dsp_simulink_MovingVariance *obj;
    g_dsp_internal_ExponentialMovin *obj_0;
    int32_T i;
    boolean_T flag;
    rtDW.icLoad = true;
    rtDW.Histogram_EPHPState = 5U;
    for (i = 0; i < 8; i++)
    {
        rty_timestamp_stats[i] = 0U;
    }

    rtDW.fault_frames_not_empty = false;
    rtDW.fault_frames = false;
    rtDW.obj.matlabCodegenIsDeleted = true;
    b_obj = &rtDW.obj;
    b_obj->isInitialized = 0;
    b_obj->NumChannels = -1;
    b_obj->matlabCodegenIsDeleted = false;
    rtDW.objisempty = true;
    b_obj = &rtDW.obj;
    obj = b_obj;
    flag = (obj->isInitialized == 1);
    if (flag)
    {
        b_obj->TunablePropsChanged = true;
    }

    b_obj = &rtDW.obj;
    b_obj->ForgettingFactor = 0.925F;
    PlatCon_SystemCore_setup(&rtDW.obj);
    b_obj = &rtDW.obj;
    obj_0 = b_obj->pStatistic;
    if (obj_0->isInitialized == 1)
    {
        obj_0->pwN = 1.0F;
        obj_0->puN = 1.0F;
        obj_0->pvN = 1.0F;
        obj_0->pmN = 0.0F;
        obj_0->psN = 0.0F;
    }
}

static void PlatCon_TimingCheckModule(const Timing_type *rtu_sig_in_timing, uint32_T rty_timestamp_stats[8], real32_T
    *rty_timestamp_var, boolean_T *rty_frame_count_skipped)
{
    dsp_simulink_MovingVariance *obj;
    dsp_simulink_MovingVariance *obj_0;
    g_dsp_internal_ExponentialMovin *obj_1;
    g_dsp_internal_ExponentialMovin *obj_2;
    g_dsp_internal_ExponentialMovin *obj_3;
    g_dsp_internal_ExponentialMovin *obj_4;
    int32_T i;
    real32_T ForgettingFactor;
    real32_T lambda;
    real32_T pmLocal;
    real32_T pmLocal_prev;
    real32_T psLocal;
    real32_T puLocal;
    real32_T u0;
    real32_T v;
    real32_T v_prev;
    real32_T y;
    real32_T z;
    real32_T z_0;
    boolean_T flag;
    if (rtDW.icLoad)
    {
        rtDW.Delay_DSTATE = rtu_sig_in_timing->Timestamp;
    }

    rtDW.Delay = rtDW.Delay_DSTATE;
    rtDW.elapsed_time = rtu_sig_in_timing->Timestamp - rtDW.Delay;
    rtDW.DataTypeConversion = (int32_T)floorf(rtDW.elapsed_time);
    PlatCon_eML_blk_kernel_k(rtu_sig_in_timing->FrameCount, &rtDW.rst, rty_frame_count_skipped);
    if (MWDSP_EPH_R_B(rtDW.rst, &rtDW.Histogram_EPHPState) != 0U)
    {
        for (i = 0; i < 8; i++)
        {
            rty_timestamp_stats[i] = 0U;
        }
    }

    binSearch_s32_s32(0, 7, 1, rtDW.DataTypeConversion, &rty_timestamp_stats[0], &rtCP_Histogram_BIN_BOUNDARY[0]);
    u0 = rtDW.elapsed_time;
    if (rtDW.obj.ForgettingFactor != 0.925F)
    {
        obj = &rtDW.obj;
        obj_0 = obj;
        flag = (obj_0->isInitialized == 1);
        if (flag)
        {
            obj->TunablePropsChanged = true;
        }

        obj = &rtDW.obj;
        obj->ForgettingFactor = 0.925F;
    }

    obj = &rtDW.obj;
    obj_0 = obj;
    if (obj_0->TunablePropsChanged)
    {
        obj_0->TunablePropsChanged = false;
        obj_1 = obj_0->pStatistic;
        obj_2 = obj_1;
        flag = (obj_2->isInitialized == 1);
        if (flag)
        {
            obj_1->TunablePropsChanged = true;
        }

        obj_0->pStatistic->ForgettingFactor = obj_0->ForgettingFactor;
    }

    obj_1 = obj->pStatistic;
    if (obj_1->isInitialized != 1)
    {
        obj_2 = obj_1;
        obj_3 = obj_2;
        obj_3->isSetupComplete = false;
        obj_3->isInitialized = 1;
        obj_4 = obj_3;
        obj_4->pwN = 1.0F;
        obj_4->puN = 1.0F;
        obj_4->pvN = 0.0F;
        obj_4->pmN = 0.0F;
        obj_4->psN = 0.0F;
        obj_4->plambda = obj_4->ForgettingFactor;
        obj_3->isSetupComplete = true;
        obj_3->TunablePropsChanged = false;
        obj_2->pwN = 1.0F;
        obj_2->puN = 1.0F;
        obj_2->pvN = 1.0F;
        obj_2->pmN = 0.0F;
        obj_2->psN = 0.0F;
    }

    obj_2 = obj_1;
    if (obj_2->TunablePropsChanged)
    {
        obj_2->TunablePropsChanged = false;
        obj_2->plambda = obj_2->ForgettingFactor;
    }

    ForgettingFactor = obj_1->pwN;
    pmLocal = obj_1->pmN;
    puLocal = obj_1->puN;
    psLocal = obj_1->psN;
    v_prev = obj_1->pvN;
    lambda = obj_1->plambda;
    y = 0.0F;
    pmLocal_prev = pmLocal;
    z = 1.0F / ForgettingFactor;
    z_0 = 1.0F / ForgettingFactor;
    pmLocal = (1.0F - z) * pmLocal + z_0 * u0;
    z_0 = ForgettingFactor - 1.0F;
    z = z_0 / ForgettingFactor;
    v = z * z;
    z = 1.0F / ForgettingFactor;
    z *= z;
    puLocal = v * puLocal + z;
    v = (1.0F - puLocal) * ForgettingFactor;
    if (v != 0.0F)
    {
        z = 1.0F / v;
        z_0 = ForgettingFactor - 1.0F;
        z_0 /= ForgettingFactor;
        u0 = pmLocal_prev - u0;
        y = u0 * u0;
        psLocal = (lambda * v_prev * psLocal + z_0 * y) * z;
        y = psLocal;
    }

    v_prev = v;
    ForgettingFactor = lambda * ForgettingFactor + 1.0F;
    obj_1->pwN = ForgettingFactor;
    obj_1->pmN = pmLocal;
    obj_1->puN = puLocal;
    obj_1->psN = psLocal;
    obj_1->pvN = v_prev;
    *rty_timestamp_var = y;
    rtDW.icLoad = false;
    rtDW.Delay_DSTATE = rtu_sig_in_timing->Timestamp;
}

static void PlatCon_TimingCheckModule_Term(void)
{
    dsp_simulink_MovingVariance *obj;
    g_dsp_internal_ExponentialMovin *obj_0;
    obj = &rtDW.obj;
    if (!obj->matlabCodegenIsDeleted)
    {
        obj->matlabCodegenIsDeleted = true;
        if ((obj->isInitialized == 1) && obj->isSetupComplete)
        {
            obj_0 = obj->pStatistic;
            if (obj_0->isInitialized == 1)
            {
                obj_0->isInitialized = 2;
            }

            obj->NumChannels = -1;
        }
    }
}

static void PlatCon_eML_blk_kernel_n(boolean_T *test_done, real32_T *test_rate)
{
    static const real32_T b[16] =
    {
        0.0009F, 0.0015F, 0.0025F, 0.004F, 0.006F, 0.01F, 0.015F, 0.025F, 0.04F, 0.07F, 0.1F, 0.2F, 0.3F, 0.5F, 0.8F,
        1.25F
    };

    const real32_T *TEST_RATES;
    *test_done = false;
    TEST_RATES = &b[0];
    if ((uint8_T)((uint32_T)rtDW.current_rate_idx - ((rtDW.current_rate_idx / 2) << 1)) != 0)
    {
        *test_rate = TEST_RATES[rtDW.current_rate_idx - 1];
    }
    else
    {
        *test_rate = -TEST_RATES[rtDW.current_rate_idx - 1];
    }

    rtDW.count++;
    if (rtDW.count > 60.0)
    {
        rtDW.current_rate_idx++;
        if (rtDW.current_rate_idx < 9)
        {
            rtDW.count = 0.0;
        }
        else if (rtDW.current_rate_idx < 12)
        {
            rtDW.count = 30.0;
        }
        else if (rtDW.current_rate_idx < 16)
        {
            rtDW.count = 45.0;
        }
        else
        {
            rtDW.count = 55.0;
        }
    }

    if (rtDW.current_rate_idx > 16)
    {
        *test_done = true;
        *test_rate = 0.0F;
        rtDW.current_rate_idx = 1U;
    }
}

static void PlatCon_eML_blk_kernel_h(real32_T rate_val, Enum_AxesToTest sel_axes, real32_T y[2])
{
    switch (sel_axes)
    {
      case X_AXIS:
        y[0] = rate_val;
        y[1] = 0.0F;
        break;

      case Y_AXIS:
        y[0] = 0.0F;
        y[1] = rate_val;
        break;

      case XY_AXIS:
        y[0] = rate_val;
        y[1] = rate_val;
        break;

      default:
        y[0] = 0.0F;
        y[1] = 0.0F;
        break;
    }
}

void Platform_Controller_step(void)
{
    int32_T i;
    rtDW.Delay1 = rtDW.Delay1_DSTATE;
    if (rtDW.is_active_c6_Platform_Controlle == 0U)
    {
        rtDW.is_active_c6_Platform_Controlle = 1U;
        rtDW.reset_PID = true;
        rtDW.en_bleed = false;
        rtDW.test_en = false;
        rtDW.is_c6_Platform_Controller = IN_STANDBY;
    }
    else
    {
        switch (rtDW.is_c6_Platform_Controller)
        {
          case IN_ABSOLUTE:
            if (sig_in_TrackState != ABSOLUTE)
            {
                switch (sig_in_TrackState)
                {
                  case ABSOLUTE:
                    rtDW.is_c6_Platform_Controller = IN_ABSOLUTE;
                    break;

                  case MANUAL:
                    rtDW.is_c6_Platform_Controller = IN_MANUAL;
                    break;

                  case TRACK:
                    rtDW.is_c6_Platform_Controller = IN_TRACK;
                    rtDW.reset_PID = false;
                    rtDW.en_bleed = true;
                    break;

                  case TEST:
                    rtDW.is_c6_Platform_Controller = IN_TEST;
                    rtDW.is_TEST = IN_ACTIVE;
                    rtDW.test_en = true;
                    break;

                  default:
                    rtDW.is_c6_Platform_Controller = IN_STANDBY;
                    break;
                }
            }
            break;

          case IN_MANUAL:
            if (sig_in_TrackState != MANUAL)
            {
                switch (sig_in_TrackState)
                {
                  case ABSOLUTE:
                    rtDW.is_c6_Platform_Controller = IN_ABSOLUTE;
                    break;

                  case MANUAL:
                    rtDW.is_c6_Platform_Controller = IN_MANUAL;
                    break;

                  case TRACK:
                    rtDW.is_c6_Platform_Controller = IN_TRACK;
                    rtDW.reset_PID = false;
                    rtDW.en_bleed = true;
                    break;

                  case TEST:
                    rtDW.is_c6_Platform_Controller = IN_TEST;
                    rtDW.is_TEST = IN_ACTIVE;
                    rtDW.test_en = true;
                    break;

                  default:
                    rtDW.is_c6_Platform_Controller = IN_STANDBY;
                    break;
                }
            }
            break;

          case IN_STANDBY:
            if (sig_in_TrackState != STANDBY)
            {
                switch (sig_in_TrackState)
                {
                  case ABSOLUTE:
                    rtDW.is_c6_Platform_Controller = IN_ABSOLUTE;
                    break;

                  case MANUAL:
                    rtDW.is_c6_Platform_Controller = IN_MANUAL;
                    break;

                  case TRACK:
                    rtDW.is_c6_Platform_Controller = IN_TRACK;
                    rtDW.reset_PID = false;
                    rtDW.en_bleed = true;
                    break;

                  case TEST:
                    rtDW.is_c6_Platform_Controller = IN_TEST;
                    rtDW.is_TEST = IN_ACTIVE;
                    rtDW.test_en = true;
                    break;

                  default:
                    rtDW.is_c6_Platform_Controller = IN_STANDBY;
                    break;
                }
            }
            break;

          case IN_TEST:
            if (sig_in_TrackState != TEST)
            {
                rtDW.reset_PID = true;
                rtDW.en_bleed = false;
                switch (sig_in_TrackState)
                {
                  case ABSOLUTE:
                    if (rtDW.is_TEST == IN_ACTIVE)
                    {
                        rtDW.test_en = false;
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }
                    else
                    {
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }

                    rtDW.is_c6_Platform_Controller = IN_ABSOLUTE;
                    break;

                  case MANUAL:
                    if (rtDW.is_TEST == IN_ACTIVE)
                    {
                        rtDW.test_en = false;
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }
                    else
                    {
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }

                    rtDW.is_c6_Platform_Controller = IN_MANUAL;
                    break;

                  case TRACK:
                    if (rtDW.is_TEST == IN_ACTIVE)
                    {
                        rtDW.test_en = false;
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }
                    else
                    {
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }

                    rtDW.is_c6_Platform_Controller = IN_TRACK;
                    rtDW.reset_PID = false;
                    rtDW.en_bleed = true;
                    break;

                  case TEST:
                    rtDW.is_c6_Platform_Controller = IN_TEST;
                    rtDW.is_TEST = IN_ACTIVE;
                    rtDW.test_en = true;
                    break;

                  default:
                    if (rtDW.is_TEST == IN_ACTIVE)
                    {
                        rtDW.test_en = false;
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }
                    else
                    {
                        rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
                    }

                    rtDW.is_c6_Platform_Controller = IN_STANDBY;
                    break;
                }
            }
            else if ((rtDW.is_TEST == IN_ACTIVE) && rtDW.Delay1)
            {
                rtDW.test_en = false;
                rtDW.is_TEST = IN_DONE;
            }
            else
            {
            }
            break;

          default:
            if (sig_in_TrackState != TRACK)
            {
                switch (sig_in_TrackState)
                {
                  case ABSOLUTE:
                    rtDW.reset_PID = true;
                    rtDW.en_bleed = false;
                    rtDW.is_c6_Platform_Controller = IN_ABSOLUTE;
                    break;

                  case MANUAL:
                    rtDW.reset_PID = true;
                    rtDW.en_bleed = false;
                    rtDW.is_c6_Platform_Controller = IN_MANUAL;
                    break;

                  case TRACK:
                    rtDW.is_c6_Platform_Controller = IN_TRACK;
                    rtDW.reset_PID = false;
                    rtDW.en_bleed = true;
                    break;

                  case TEST:
                    rtDW.reset_PID = true;
                    rtDW.en_bleed = false;
                    rtDW.is_c6_Platform_Controller = IN_TEST;
                    rtDW.is_TEST = IN_ACTIVE;
                    rtDW.test_en = true;
                    break;

                  default:
                    rtDW.reset_PID = true;
                    rtDW.en_bleed = false;
                    rtDW.is_c6_Platform_Controller = IN_STANDBY;
                    break;
                }
            }
            break;
        }
    }

    PlatCon_TimingCheckModule(&sig_in_Timing, rtDW.Histogram, &rtDW.MovingVariance, &rtDW.y_a);
    if (rtDW.test_en)
    {
        if (!rtDW.RateTest_Module_MODE)
        {
            rtDW.current_rate_idx = 1U;
            rtDW.count = 0.0;
            rtDW.RateTest_Module_MODE = true;
        }

        PlatCon_eML_blk_kernel_n(&rtDW.test_done, &rtDW.test_rate);
        PlatCon_eML_blk_kernel_h(rtDW.test_rate, Controller_param.AxesToTest, rtDW.y);
    }
    else
    {
        rtDW.RateTest_Module_MODE = false;
    }

    rtDW.Deg2Rad[0] = 0.0174532924F * sig_in_Enc_LOS[0];
    rtDW.Deg2Rad[1] = 0.0174532924F * sig_in_Enc_LOS[1];
    rtDW.Secant = cosf(rtDW.Deg2Rad[1]);
    rtDW.VectorConcatenate1[0] = rtDW.Secant * sig_in_TBE[0];
    rtDW.VectorConcatenate1[1] = sig_in_TBE[1];
    PlatCon_TBERateProfiling(rtDW.en_bleed, rtDW.VectorConcatenate1, rtDW.Switch1);
    PlatCon_PIDController(rtDW.reset_PID, rtDW.Switch1, rtDW.VectorConcatenate);
    if (rtDW.test_en)
    {
        rtDW.MultiportSwitch[0] = rtDW.y[0];
        rtDW.MultiportSwitch[1] = rtDW.y[1];
    }
    else
    {
        rtDW.MultiportSwitch[0] = rtDW.VectorConcatenate[0];
        rtDW.MultiportSwitch[1] = rtDW.VectorConcatenate[1];
    }

    rtDW.rate_demands_dps[0] = 57.2957802F * rtDW.MultiportSwitch[0];
    rtDW.rate_demands_dps[1] = 57.2957802F * rtDW.MultiportSwitch[1];
    PlatCon_OutputScaling(rtDW.rate_demands_dps, rtDW.e, rtDW.rate_input_demand);
    rtDW.UnitDelay[0] = rtDW.UnitDelay_DSTATE[0];
    rtDW.u[0] = rtDW.UnitDelay[0] + rtDW.e[0];
    rtDW.UnitDelay[1] = rtDW.UnitDelay_DSTATE[1];
    rtDW.u[1] = rtDW.UnitDelay[1] + rtDW.e[1];
    PlatCon_Embedded(rtDW.u[0], &rtDW.y1_a, &rtDW.y2_o);
    PlatCon_Embedded(rtDW.u[1], &rtDW.y1, &rtDW.y2);
    if (Controller_param.RoundDitherEnable)
    {
        rtDW.TmpSignalConversionAtSum3Inport[0] = rtDW.y2_o;
        rtDW.TmpSignalConversionAtSum3Inport[1] = rtDW.y2;
        rtDW.adjusted_round[0] = (int16_T)(rtDW.TmpSignalConversionAtSum3Inport[0] + rtDW.rate_input_demand[0]);
        rtY.sig_out_rate_demands[0] = rtDW.adjusted_round[0];
        rtDW.adjusted_round[1] = (int16_T)(rtDW.TmpSignalConversionAtSum3Inport[1] + rtDW.rate_input_demand[1]);
        rtY.sig_out_rate_demands[1] = rtDW.adjusted_round[1];
    }
    else
    {
        rtY.sig_out_rate_demands[0] = rtDW.rate_input_demand[0];
        rtY.sig_out_rate_demands[1] = rtDW.rate_input_demand[1];
    }

    rtDW.UnitDelay_DSTATE[0] = rtDW.y1_a;
    rtDW.UnitDelay_DSTATE[1] = rtDW.y1;
    for (i = 0; i < 8; i++)
    {
        rtY.sig_out_status.timestamp_stats[i] = rtDW.Histogram[i];
    }

    rtY.sig_out_status.timestamp_variance = rtDW.MovingVariance;
    rtY.sig_out_status.frame_count_skipped = rtDW.y_a;
    rtY.sig_out_status.test_active = rtDW.test_en;
    rtY.sig_out_status.rate_demands_dps[0] = rtDW.rate_demands_dps[0];
    rtY.sig_out_status.rate_demands_dps[1] = rtDW.rate_demands_dps[1];
    rtDW.Delay1_DSTATE = rtDW.test_done;
}

void Platform_Controller_initialize(void)
{
    (void) memset((void *)&rtDW, 0,
                  sizeof(DW));
    (void)memset(&sig_in_TBE[0], 0, sizeof(real32_T) << 1U);
    (void)memset(&sig_in_Enc_LOS[0], 0, sizeof(real32_T) << 1U);
    sig_in_TrackState = STANDBY;
    sig_in_Timing = Platform_Controller_rtZTiming_t;
    (void)memset(&rtY, 0, sizeof(ExtY));
    rtPrevZCX.UD_Reset_ZCE = POS_ZCSIG;
    rtPrevZCX.UD_Reset_ZCE_j = POS_ZCSIG;
    rtDW.Delay1_DSTATE = true;
    rtDW.is_TEST = IN_NO_ACTIVE_CHILD;
    rtDW.is_active_c6_Platform_Controlle = 0U;
    rtDW.is_c6_Platform_Controller = IN_NO_ACTIVE_CHILD;
    rtDW.test_en = false;
    rtDW.reset_PID = false;
    rtDW.en_bleed = false;
    PlatCon_TimingCheckModule_Init(rtDW.Histogram);
    rtDW.current_rate_idx = 1U;
    rtDW.count = 0.0;
    PlatCon_TBERateProfiling_Init();
    PlatCon_PIDController_Init();
}

void Platform_Controller_terminate(void)
{
    PlatCon_TimingCheckModule_Term();
}
