#ifndef RTW_HEADER_Platform_Controller_h_
#define RTW_HEADER_Platform_Controller_h_
#ifndef Platform_Controller_COMMON_INCLUDES_
#define Platform_Controller_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif

#include <string.h>
#include "zero_crossing_types.h"

#ifndef DEFINED_TYPEDEF_FOR_Enum_AxesToTest_
#define DEFINED_TYPEDEF_FOR_Enum_AxesToTest_

typedef enum
{
    NONE = 0,
    X_AXIS,
    Y_AXIS,
    XY_AXIS
}
Enum_AxesToTest;

#endif

#ifndef DEFINED_TYPEDEF_FOR_Status_type_
#define DEFINED_TYPEDEF_FOR_Status_type_

typedef struct
{
    uint32_T timestamp_stats[8];
    real32_T timestamp_variance;
    boolean_T frame_count_skipped;
    boolean_T test_active;
    real32_T rate_demands_dps[2];
}
Status_type;

#endif

#ifndef DEFINED_TYPEDEF_FOR_Timing_type_
#define DEFINED_TYPEDEF_FOR_Timing_type_

typedef struct
{
    int32_T FrameCount;
    real32_T Timestamp;
}
Timing_type;

#endif

#ifndef DEFINED_TYPEDEF_FOR_Enum_TrackState_
#define DEFINED_TYPEDEF_FOR_Enum_TrackState_

typedef enum
{
    STANDBY = 0,
    MANUAL,
    ABSOLUTE,
    TRACK,
    TEST
}
Enum_TrackState;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_e3pw6ZVcvAHrsIk2w3e97G_
#define DEFINED_TYPEDEF_FOR_struct_e3pw6ZVcvAHrsIk2w3e97G_

typedef struct
{
    real32_T PID_P;
    real32_T PID_I;
    real32_T PID_D;
    real32_T Control_Gain;
    uint16_T NoOfRateLevels;
    real32_T PlatformMaxRate;
    boolean_T BleedEnable;
    real32_T BleedRate;
    boolean_T RoundDitherEnable;
    Enum_AxesToTest AxesToTest;
}
struct_e3pw6ZVcvAHrsIk2w3e97G;

#endif

#ifndef struct_tag_LMHBAAohN8awXsbYpAnSuE
#define struct_tag_LMHBAAohN8awXsbYpAnSuE

struct tag_LMHBAAohN8awXsbYpAnSuE
{
    int32_T isInitialized;
    boolean_T isSetupComplete;
    boolean_T TunablePropsChanged;
    real32_T ForgettingFactor;
    real32_T pwN;
    real32_T puN;
    real32_T pmN;
    real32_T pvN;
    real32_T psN;
    real32_T plambda;
};

#endif

#ifndef typedef_g_dsp_internal_ExponentialMovin
#define typedef_g_dsp_internal_ExponentialMovin

typedef struct tag_LMHBAAohN8awXsbYpAnSuE g_dsp_internal_ExponentialMovin;

#endif

#ifndef struct_tag_BlgwLpgj2bjudmbmVKWwDE
#define struct_tag_BlgwLpgj2bjudmbmVKWwDE

struct tag_BlgwLpgj2bjudmbmVKWwDE
{
    uint32_T f1[8];
};

#endif

#ifndef typedef_cell_wrap
#define typedef_cell_wrap

typedef struct tag_BlgwLpgj2bjudmbmVKWwDE cell_wrap;

#endif

#ifndef struct_tag_lIi9sPMZbKupSYUnQueqNF
#define struct_tag_lIi9sPMZbKupSYUnQueqNF

struct tag_lIi9sPMZbKupSYUnQueqNF
{
    boolean_T matlabCodegenIsDeleted;
    int32_T isInitialized;
    boolean_T isSetupComplete;
    boolean_T TunablePropsChanged;
    cell_wrap inputVarSize;
    real32_T ForgettingFactor;
    g_dsp_internal_ExponentialMovin *pStatistic;
    int32_T NumChannels;
    g_dsp_internal_ExponentialMovin _pobj0;
};

#endif

#ifndef typedef_dsp_simulink_MovingVariance
#define typedef_dsp_simulink_MovingVariance

typedef struct tag_lIi9sPMZbKupSYUnQueqNF dsp_simulink_MovingVariance;

#endif

typedef struct
{
    dsp_simulink_MovingVariance obj;
    real_T count;
    real32_T Deg2Rad[2];
    real32_T VectorConcatenate1[2];
    real32_T MultiportSwitch[2];
    real32_T rate_demands_dps[2];
    real32_T Switch1[2];
    real32_T Sum[2];
    real32_T TBE_Offset[2];
    real32_T y[2];
    real32_T PlatformGain[2];
    real32_T VectorConcatenate[2];
    real32_T NoOfLevels[2];
    real32_T Product[2];
    real32_T Saturation[2];
    real32_T rounded[2];
    real32_T e[2];
    real32_T UnitDelay[2];
    real32_T u[2];
    real32_T UnitDelay_DSTATE[2];
    real32_T TBE_Store[2];
    real32_T Secant;
    real32_T Delay;
    real32_T elapsed_time;
    real32_T MovingVariance;
    real32_T test_rate;
    real32_T Constant;
    real32_T Switch;
    real32_T DerivativeGain;
    real32_T Tsamp;
    real32_T UD;
    real32_T Diff;
    real32_T Integrator;
    real32_T ProportionalGain;
    real32_T Sum_f;
    real32_T IntegralGain;
    real32_T Switch1_m;
    real32_T DerivativeGain_g;
    real32_T Tsamp_f;
    real32_T UD_n;
    real32_T Diff_j;
    real32_T Integrator_m;
    real32_T ProportionalGain_c;
    real32_T Sum_k;
    real32_T IntegralGain_b;
    real32_T y1;
    real32_T y1_a;
    real32_T Delay_DSTATE;
    real32_T UD_DSTATE;
    real32_T Integrator_DSTATE;
    real32_T UD_DSTATE_g;
    real32_T Integrator_DSTATE_e;
    real32_T nCount;
    int32_T DataTypeConversion;
    int32_T count_prev;
    uint32_T Histogram[8];
    uint32_T Histogram_EPHPState;
    int16_T rate_input_demand[2];
    int16_T TmpSignalConversionAtSum3Inport[2];
    int16_T adjusted_round[2];
    int16_T y2;
    int16_T y2_o;
    int8_T count_g;
    int8_T Integrator_PrevResetState;
    int8_T Integrator_PrevResetState_o;
    uint8_T current_rate_idx;
    uint8_T is_active_c6_Platform_Controlle;
    uint8_T is_c6_Platform_Controller;
    uint8_T is_TEST;
    boolean_T Delay1;
    boolean_T rst;
    boolean_T y_a;
    boolean_T test_done;
    boolean_T test_en;
    boolean_T reset_PID;
    boolean_T en_bleed;
    boolean_T Delay1_DSTATE;
    boolean_T icLoad;
    boolean_T fault_frames;
    boolean_T fault_frames_not_empty;
    boolean_T objisempty;
    boolean_T active;
    boolean_T icLoad_g;
    boolean_T icLoad_k;
    boolean_T RateTest_Module_MODE;
}
DW;

typedef struct
{
    ZCSigState UD_Reset_ZCE;
    ZCSigState UD_Reset_ZCE_j;
}
PrevZCX;

typedef struct
{
    int16_T sig_out_rate_demands[2];
    Status_type sig_out_status;
}
ExtY;

extern DW rtDW;
extern PrevZCX rtPrevZCX;
extern ExtY rtY;
extern const Timing_type Platform_Controller_rtZTiming_t;
extern real32_T sig_in_TBE[2];
extern real32_T sig_in_Enc_LOS[2];
extern Enum_TrackState sig_in_TrackState;
extern Timing_type sig_in_Timing;
extern struct_e3pw6ZVcvAHrsIk2w3e97G Controller_param;
extern void Platform_Controller_initialize(void);
extern void Platform_Controller_step(void);
extern void Platform_Controller_terminate(void);

#endif
