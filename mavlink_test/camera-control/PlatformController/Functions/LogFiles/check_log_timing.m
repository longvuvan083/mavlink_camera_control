%%

Tests = {'D:\Work\Vizgard\Engineering\Projects\platform_controller\PlatformController\Data\fixed-timing'};
%         'D:\Work\Vizgard\Engineering\Projects\platform_controller\PlatformController\Data\bleed'};
%          'D:\Work\Vizgard\Engineering\Projects\platform_controller\Data\Test_SerialCam'};

for testid = 1:size(Tests,1)

%fname = ['D:\Work\Vizgard\Engineering\Projects\platform_controller\Data\test' num2str(testid) '\log_camera_info.txt'];
A = importfile(fullfile(Tests{testid},'log_camera_info.txt'));


A.timestamp = A.timestamp - A.timestamp(1);
A.framecount = A.framecount - A.framecount(1);

figure(1)
subplot(2,2,1);
plot(A.framecount,A.timestamp,'b.-')
grid on
title({['TEST ' num2str(testid) ' LOG:']; 'framecount vs timestamp'})
xlabel('timestamp')
ylabel('framecount')


% plot framecount (checking for dropped frames)
subplot(2,2,2);
plot(diff(A.framecount),'b.-')
grid on
title('framecount increment - should be 1')
xlabel('log entry (row)')
ylabel('framecount difference')

k = A.timestamp;
subplot(2,2,4);
plot(A.framecount(2:end),diff(k),'r.-')
grid on
title('time per framecount/sample')
xlabel('framecount')
ylabel('time [ms]')


[H,bins] = hist(diff(k),15:40);
subplot(2,2,3);
bar(bins,H)
grid on
title('distribution of sample period')
xlabel('sample time [ms]')
ylabel('occurence')


exportgraphics(gcf,fullfile(Tests{testid},'Test_timing_chk.png'),'Resolution',300)


end


% %% Segment track sections
% idx = (A.TrackState == 'Track');
% trk_start = find(diff(idx)>0)+1;
% trk_end = find(diff(idx)<0);
% 
% for n = 1:length(trk_start)
%     segments.(['Track' num2str(n)']) = A(trk_start(n):trk_end(n),:);
% end
% 
% 
% %%
% 
% tmp = dir(fullfile(Tests{testid},'*.mp4'));
% vidObj = VideoReader(fullfile(Tests{testid},tmp.name));
% 
% %% Load in the video file and step through each frame
% 
% for frame_idx = 742:770
%     if(hasFrame(vidObj))
%         frame = read(vidObj,frame_idx);
%         imshow(frame);
%         title(sprintf('Frame (%d) : Time = %.3f sec', frame_idx, vidObj.CurrentTime));
%         pause(0.85);
%     end
% end
% 
% 
% 
% %% load video frames at rate change events
% for idx = 1:17
%     frame_idx = f(idx);
%     if(hasFrame(vidObj))
%         frame = read(vidObj,frame_idx);
%         imshow(frame);
%         title(sprintf('Frame (%d) : Time = %.3f sec', frame_idx, vidObj.CurrentTime));
%         [X,Y]=ginput(1); 
%         pos(idx,:) = [X Y];
%         %pause(0.85);
%     end
% end









