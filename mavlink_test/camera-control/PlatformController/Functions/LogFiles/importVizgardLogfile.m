function cl = importVizgardLogfile(filename)
%IMPORTFILE Import data from a text file
%  CL = IMPORTFILE(FILENAME) reads data from text file FILENAME for the
%  default selection.  Returns the data as a table.
%
%


%% Set up the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 12);

% Specify range and delimiter
opts.DataLines = [2, Inf];
opts.Delimiter = "\t";

% Specify column names and types
opts.VariableNames = ["framecount", "timestamp", "TrackState", "Tgt_Pos_X", "Tgt_Pos_Y", "Platform_Az", "Platform_El", "FieldofView", "Rate_Demand_Az", "Rate_Demand_El", "Var11", "Var12"];
opts.SelectedVariableNames = ["framecount", "timestamp", "TrackState", "Tgt_Pos_X", "Tgt_Pos_Y", "Platform_Az", "Platform_El", "FieldofView", "Rate_Demand_Az", "Rate_Demand_El"];
opts.VariableTypes = ["double", "double", "categorical", "double", "double", "double", "double", "double", "double", "double", "string", "string"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Specify variable properties
opts = setvaropts(opts, ["Var11", "Var12"], "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["TrackState", "Var11", "Var12"], "EmptyFieldRule", "auto");

% Import the data
cl = readtable(filename, opts);

cl.timestamp = seconds((cl.timestamp - min(cl.timestamp))/1000);
cl = mergevars(cl,{'Tgt_Pos_X','Tgt_Pos_Y'},'NewVariableName','TBE');
cl = mergevars(cl,{'Platform_Az','Platform_El'},'NewVariableName','LOS');
cl = mergevars(cl,{'Rate_Demand_Az','Rate_Demand_El'},'NewVariableName','RateDemand');

FoV = cl.FieldofView/1920*pi/180;
TBE_pixels = cl.TBE - [1920 1080]/2;
cl.TBE = TBE_pixels .* FoV;



end