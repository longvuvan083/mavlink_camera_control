

function plotVizgardLogfile(vl)


figure

tiledlayout(2,1)
nexttile
plot(vl.timestamp,vl.TBE(:,1),'b.-')
title('azimuth [rads]')
ax(1) = gca;
grid on
nexttile
plot(vl.timestamp,vl.TBE(:,2),'b.-')
title('elevation rads]')
ax(2) = gca;
grid on


% link axes
linkaxes(ax,'xy')




