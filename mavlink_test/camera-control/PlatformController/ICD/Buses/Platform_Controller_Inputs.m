%% Create signal and parameter objects
% % inputs
sig_in_TBE = Simulink.Signal;
sig_in_TBE.DataType = 'single';
sig_in_TBE.Dimensions = 2;
sig_in_TBE.CoderInfo.StorageClass = 'ExportedGlobal';

sig_in_Enc_LOS = Simulink.Signal;
sig_in_Enc_LOS.DataType = 'single';
sig_in_Enc_LOS.Dimensions = 2;
sig_in_Enc_LOS.CoderInfo.StorageClass = 'ExportedGlobal';

% outputs
sig_out_rate_demands = Simulink.Signal;
sig_out_rate_demands.DataType = 'single';
sig_out_rate_demands.Dimensions = 2;
sig_out_rate_demands.CoderInfo.StorageClass = 'ExportedGlobal';