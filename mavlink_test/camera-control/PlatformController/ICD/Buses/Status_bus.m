function Status_bus() 
% STATUS_BUS initializes a set of bus objects in the MATLAB base workspace 

clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'timestamp_stats';
elems(1).Dimensions = [8 1];
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'uint32';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'timestamp_variance';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

elems(3) = Simulink.BusElement;
elems(3).Name = 'frame_count_skipped';
elems(3).Dimensions = 1;
elems(3).DimensionsMode = 'Fixed';
elems(3).DataType = 'boolean';
elems(3).Complexity = 'real';
elems(3).Min = [];
elems(3).Max = [];
elems(3).DocUnits = '';
elems(3).Description = '';

elems(4) = Simulink.BusElement;
elems(4).Name = 'test_active';
elems(4).Dimensions = 1;
elems(4).DimensionsMode = 'Fixed';
elems(4).DataType = 'boolean';
elems(4).Complexity = 'real';
elems(4).Min = [];
elems(4).Max = [];
elems(4).DocUnits = '';
elems(4).Description = '';

elems(5) = Simulink.BusElement;
elems(5).Name = 'rate_demands_dps';
elems(5).Dimensions = [2 1];
elems(5).DimensionsMode = 'Fixed';
elems(5).DataType = 'single';
elems(5).Complexity = 'real';
elems(5).Min = [];
elems(5).Max = [];
elems(5).DocUnits = '';
elems(5).Description = '';

slBus1 = Simulink.Bus;
slBus1.HeaderFile = '';
slBus1.Description = '';
slBus1.DataScope = 'Auto';
slBus1.Alignment = -1;
slBus1.PreserveElementDimensions = 0;
slBus1.Elements = elems;
clear elems;
assignin('base','Status_type', slBus1);

