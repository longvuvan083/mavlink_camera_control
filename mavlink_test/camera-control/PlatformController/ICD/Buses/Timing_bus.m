function Timing_bus() 
% NAV_SYSTEM_BUS_NAV2_3 initializes a set of bus objects in the MATLAB base workspace 


clear elems;
elems(1) = Simulink.BusElement;
elems(1).Name = 'FrameCount';
elems(1).Dimensions = 1;
elems(1).DimensionsMode = 'Fixed';
elems(1).DataType = 'int32';
elems(1).Complexity = 'real';
elems(1).Min = [];
elems(1).Max = [];
elems(1).DocUnits = '';
elems(1).Description = '';

elems(2) = Simulink.BusElement;
elems(2).Name = 'Timestamp';
elems(2).Dimensions = 1;
elems(2).DimensionsMode = 'Fixed';
elems(2).DataType = 'single';
elems(2).Complexity = 'real';
elems(2).Min = [];
elems(2).Max = [];
elems(2).DocUnits = '';
elems(2).Description = '';

slBus2 = Simulink.Bus;
slBus2.HeaderFile = '';
slBus2.Description = '';
slBus2.DataScope = 'Auto';
slBus2.Alignment = -1;
slBus2.PreserveElementDimensions = 0;
slBus2.Elements = elems;
clear elems;
assignin('base','Timing_type', slBus2);

