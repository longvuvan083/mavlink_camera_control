
% External interface enumerated type
classdef(Enumeration) Enum_AxesToTest < Simulink.IntEnumType
  enumeration
    NONE(0)
    X_AXIS(1)
    Y_AXIS(2)
    XY_AXIS(3)     
    
    
  end
  methods (Static = true)
    function retVal = addClassNameToEnumNames()
      retVal = false;
    end
    
  end
end


