
% External interface enumerated type
classdef(Enumeration) Enum_TrackState < Simulink.IntEnumType
  enumeration
    STANDBY(0)
    MANUAL(1)
    ABSOLUTE(2)
    TRACK(3)
    TEST(4)
    
    
  end
  methods (Static = true)
    function retVal = addClassNameToEnumNames()
      retVal = false;
    end
    
  end
end


