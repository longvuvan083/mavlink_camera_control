% -------------------------------------------------------------------
%  Generated by MATLAB on 23-Sep-2022 12:11:21
%  MATLAB version: 9.12.0.1884302 (R2022a)
% -------------------------------------------------------------------
                                                                     

Platform_Controller_RefConfig = Simulink.ConfigSet;
Platform_Controller_RefConfig.set_param('Name', 'Configuration');
Platform_Controller_RefConfig.set_param('Description', '');
Platform_Controller_RefConfig.set_param('SystemTargetFile', 'ert.tlc');
Platform_Controller_RefConfig.set_param('HardwareBoard', 'None');
Platform_Controller_RefConfig.set_param('TargetLang', 'C');
Platform_Controller_RefConfig.set_param('CodeInterfacePackaging', ['Non' ...
   'reusable function']);
Platform_Controller_RefConfig.set_param('GenerateAllocFcn', 'off');
Platform_Controller_RefConfig.set_param('Solver', 'FixedStepDiscrete');
Platform_Controller_RefConfig.set_param('StartTime', '0.0');
Platform_Controller_RefConfig.set_param('StopTime', 'Inf');
Platform_Controller_RefConfig.set_param('SampleTimeConstraint', ['Uncon' ...
   'strained']);
Platform_Controller_RefConfig.set_param('SolverType', 'Fixed-step');
Platform_Controller_RefConfig.set_param('SolverName', ['FixedStepDiscre' ...
   'te']);
Platform_Controller_RefConfig.set_param('FixedStep', 'auto');
Platform_Controller_RefConfig.set_param('ConcurrentTasks', 'off');
Platform_Controller_RefConfig.set_param('EnableMultiTasking', 'on');
Platform_Controller_RefConfig.set_param('AllowMultiTaskInputOutput', ['o' ...
   'ff']);
Platform_Controller_RefConfig.set_param('PositivePriorityOrder', 'off');
Platform_Controller_RefConfig.set_param('AutoInsertRateTranBlk', 'off');
Platform_Controller_RefConfig.set_param('Decimation', '1');
Platform_Controller_RefConfig.set_param('LoadExternalInput', 'off');
Platform_Controller_RefConfig.set_param('SaveFinalState', 'off');
Platform_Controller_RefConfig.set_param('LoadInitialState', 'off');
Platform_Controller_RefConfig.set_param('LimitDataPoints', 'on');
Platform_Controller_RefConfig.set_param('MaxDataPoints', '1000');
Platform_Controller_RefConfig.set_param('SaveFormat', 'Array');
Platform_Controller_RefConfig.set_param('SaveOutput', 'on');
Platform_Controller_RefConfig.set_param('SaveState', 'off');
Platform_Controller_RefConfig.set_param('SignalLogging', 'on');
Platform_Controller_RefConfig.set_param('DSMLogging', 'off');
Platform_Controller_RefConfig.set_param('StreamToWks', 'on');
Platform_Controller_RefConfig.set_param('InspectSignalLogs', 'off');
Platform_Controller_RefConfig.set_param('SaveTime', 'on');
Platform_Controller_RefConfig.set_param('ReturnWorkspaceOutputs', 'on');
Platform_Controller_RefConfig.set_param('TimeSaveName', 'tout');
Platform_Controller_RefConfig.set_param('OutputSaveName', 'yout');
Platform_Controller_RefConfig.set_param('SignalLoggingName', 'logsout');
Platform_Controller_RefConfig.set_param('ReturnWorkspaceOutputsName', ['o' ...
   'ut']);
Platform_Controller_RefConfig.set_param('LoggingToFile', 'off');
Platform_Controller_RefConfig.set_param('DatasetSignalFormat', ['timese' ...
   'ries']);
Platform_Controller_RefConfig.set_param('LoggingIntervals', ['[-inf, in' ...
   'f]']);
Platform_Controller_RefConfig.set_param('BlockReduction', 'on');
Platform_Controller_RefConfig.set_param('BooleanDataType', 'on');
Platform_Controller_RefConfig.set_param('ConditionallyExecuteInputs', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('DefaultParameterBehavior', ['I' ...
   'nlined']);
Platform_Controller_RefConfig.set_param(['UseDivisionForNetSlopeComputa' ...
   'tion'], 'on');
Platform_Controller_RefConfig.set_param('GainParamInheritBuiltInType', ...
   'on');
Platform_Controller_RefConfig.set_param('UseFloatMulNetSlope', 'on');
Platform_Controller_RefConfig.set_param(['InheritOutputTypeSmallerThanS' ...
   'ingle'], 'off');
Platform_Controller_RefConfig.set_param('DefaultUnderspecifiedDataType', ...
   'single');
Platform_Controller_RefConfig.set_param('UseSpecifiedMinMax', 'on');
Platform_Controller_RefConfig.set_param('InlineInvariantSignals', 'on');
Platform_Controller_RefConfig.set_param('MultiThreadedLoops', 'off');
Platform_Controller_RefConfig.set_param('OptimizationCustomize', 'on');
Platform_Controller_RefConfig.set_param('OptimizeBlockOrder', 'speed');
Platform_Controller_RefConfig.set_param('DataBitsets', 'off');
Platform_Controller_RefConfig.set_param('StateBitsets', 'off');
Platform_Controller_RefConfig.set_param('EnableMemcpy', 'on');
Platform_Controller_RefConfig.set_param('BooleansAsBitfields', 'off');
Platform_Controller_RefConfig.set_param('StrengthReduction', 'on');
Platform_Controller_RefConfig.set_param('OptimizeBlockIOStorage', 'off');
Platform_Controller_RefConfig.set_param('AdvancedOptControl', '');
Platform_Controller_RefConfig.set_param('BitwiseOrLogicalOp', ['Same as' ...
   ' modeled']);
Platform_Controller_RefConfig.set_param('MemcpyThreshold', 64);
Platform_Controller_RefConfig.set_param('PassReuseOutputArgsAs', ['Indi' ...
   'vidual arguments']);
Platform_Controller_RefConfig.set_param('PassReuseOutputArgsThreshold', ...
   12);
Platform_Controller_RefConfig.set_param('RollThreshold', 5);
Platform_Controller_RefConfig.set_param(['ActiveStateOutputEnumStorageT' ...
   'ype'], 'Native Integer');
Platform_Controller_RefConfig.set_param('ZeroExternalMemoryAtStartup', ...
   'on');
Platform_Controller_RefConfig.set_param('ZeroInternalMemoryAtStartup', ...
   'on');
Platform_Controller_RefConfig.set_param('InitFltsAndDblsToZero', 'off');
Platform_Controller_RefConfig.set_param('NoFixptDivByZeroProtection', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('EfficientFloat2IntCast', 'on');
Platform_Controller_RefConfig.set_param('EfficientMapNaN2IntZero', 'on');
Platform_Controller_RefConfig.set_param('LifeSpan', 'inf');
Platform_Controller_RefConfig.set_param('MaxStackSize', ['Inherit from ' ...
   'target']);
Platform_Controller_RefConfig.set_param('BufferReusableBoundary', 'on');
Platform_Controller_RefConfig.set_param('SimCompilerOptimization', ['of' ...
   'f']);
Platform_Controller_RefConfig.set_param('AccelVerboseBuild', 'off');
Platform_Controller_RefConfig.set_param('UseRowMajorAlgorithm', 'off');
Platform_Controller_RefConfig.set_param('LabelGuidedReuse', 'on');
Platform_Controller_RefConfig.set_param('DenormalBehavior', ['GradualUn' ...
   'derflow']);
Platform_Controller_RefConfig.set_param('EfficientTunableParamExpr', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('RTPrefix', 'error');
Platform_Controller_RefConfig.set_param('ConsistencyChecking', 'none');
Platform_Controller_RefConfig.set_param('ArrayBoundsChecking', 'none');
Platform_Controller_RefConfig.set_param('SignalInfNanChecking', 'none');
Platform_Controller_RefConfig.set_param('StringTruncationChecking', ['e' ...
   'rror']);
Platform_Controller_RefConfig.set_param('SignalRangeChecking', 'none');
Platform_Controller_RefConfig.set_param('ReadBeforeWriteMsg', ['EnableA' ...
   'llAsError']);
Platform_Controller_RefConfig.set_param('WriteAfterWriteMsg', ['UseLoca' ...
   'lSettings']);
Platform_Controller_RefConfig.set_param('WriteAfterReadMsg', ['UseLocal' ...
   'Settings']);
Platform_Controller_RefConfig.set_param('AlgebraicLoopMsg', 'warning');
Platform_Controller_RefConfig.set_param('ArtificialAlgebraicLoopMsg', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('SaveWithDisabledLinksMsg', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('SaveWithParameterizedLinksMsg', ...
   'none');
Platform_Controller_RefConfig.set_param(['UnderspecifiedInitializationD' ...
   'etection'], 'Simplified');
Platform_Controller_RefConfig.set_param(['MergeDetectMultiDrivingBlocks' ...
   'Exec'], 'error');
Platform_Controller_RefConfig.set_param('SignalResolutionControl', ['Tr' ...
   'yResolveAllWithWarning']);
Platform_Controller_RefConfig.set_param('BlockPriorityViolationMsg', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('MinStepSizeMsg', 'warning');
Platform_Controller_RefConfig.set_param('TimeAdjustmentMsg', 'none');
Platform_Controller_RefConfig.set_param('MaxConsecutiveZCsMsg', 'error');
Platform_Controller_RefConfig.set_param('MaskedZcDiagnostic', 'warning');
Platform_Controller_RefConfig.set_param('IgnoredZcDiagnostic', ['warnin' ...
   'g']);
Platform_Controller_RefConfig.set_param('SolverPrmCheckMsg', 'warning');
Platform_Controller_RefConfig.set_param('InheritedTsInSrcMsg', 'none');
Platform_Controller_RefConfig.set_param('MultiTaskDSMMsg', 'warning');
Platform_Controller_RefConfig.set_param('MultiTaskCondExecSysMsg', ['no' ...
   'ne']);
Platform_Controller_RefConfig.set_param('MultiTaskRateTransMsg', ['erro' ...
   'r']);
Platform_Controller_RefConfig.set_param('SingleTaskRateTransMsg', ['non' ...
   'e']);
Platform_Controller_RefConfig.set_param('TasksWithSamePriorityMsg', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('SigSpecEnsureSampleTimeMsg', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('CheckMatrixSingularityMsg', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param('IntegerOverflowMsg', 'warning');
Platform_Controller_RefConfig.set_param('Int32ToFloatConvMsg', ['warnin' ...
   'g']);
Platform_Controller_RefConfig.set_param('ParameterDowncastMsg', 'error');
Platform_Controller_RefConfig.set_param('ParameterOverflowMsg', 'error');
Platform_Controller_RefConfig.set_param('ParameterUnderflowMsg', 'none');
Platform_Controller_RefConfig.set_param('ParameterPrecisionLossMsg', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param('ParameterTunabilityLossMsg', ['e' ...
   'rror']);
Platform_Controller_RefConfig.set_param('FixptConstUnderflowMsg', ['non' ...
   'e']);
Platform_Controller_RefConfig.set_param('FixptConstOverflowMsg', 'none');
Platform_Controller_RefConfig.set_param('FixptConstPrecisionLossMsg', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param('UnderSpecifiedDataTypeMsg', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param('UnnecessaryDatatypeConvMsg', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('VectorMatrixConversionMsg', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param('FcnCallInpInsideContextMsg', ['e' ...
   'rror']);
Platform_Controller_RefConfig.set_param('SignalLabelMismatchMsg', ['non' ...
   'e']);
Platform_Controller_RefConfig.set_param('UnconnectedInputMsg', ['warnin' ...
   'g']);
Platform_Controller_RefConfig.set_param('UnconnectedOutputMsg', ['warni' ...
   'ng']);
Platform_Controller_RefConfig.set_param('UnconnectedLineMsg', 'none');
Platform_Controller_RefConfig.set_param('SFcnCompatibilityMsg', 'none');
Platform_Controller_RefConfig.set_param(['FrameProcessingCompatibilityM' ...
   'sg'], 'error');
Platform_Controller_RefConfig.set_param('UniqueDataStoreMsg', 'error');
Platform_Controller_RefConfig.set_param('BusObjectLabelMismatch', ['war' ...
   'ning']);
Platform_Controller_RefConfig.set_param('RootOutportRequireBusObject', ...
   'warning');
Platform_Controller_RefConfig.set_param('AssertControl', ['UseLocalSett' ...
   'ings']);
Platform_Controller_RefConfig.set_param('AllowSymbolicDim', 'on');
Platform_Controller_RefConfig.set_param('ModelReferenceIOMsg', 'none');
Platform_Controller_RefConfig.set_param(['ModelReferenceVersionMismatch' ...
   'Message'], 'none');
Platform_Controller_RefConfig.set_param(['ModelReferenceIOMismatchMessa' ...
   'ge'], 'none');
Platform_Controller_RefConfig.set_param('UnknownTsInhSupMsg', 'warning');
Platform_Controller_RefConfig.set_param(['ModelReferenceDataLoggingMess' ...
   'age'], 'warning');
Platform_Controller_RefConfig.set_param(['ModelReferenceNoExplicitFinal' ...
   'ValueMsg'], 'none');
Platform_Controller_RefConfig.set_param(['ModelReferenceSymbolNameMessa' ...
   'ge'], 'warning');
Platform_Controller_RefConfig.set_param(['ModelReferenceExtraNoncontSig' ...
   's'], 'error');
Platform_Controller_RefConfig.set_param('StateNameClashWarn', 'none');
Platform_Controller_RefConfig.set_param(['OperatingPointInterfaceChecks' ...
   'umMismatchMsg'], 'warning');
Platform_Controller_RefConfig.set_param(['NonCurrentReleaseOperatingPoi' ...
   'ntMsg'], 'error');
Platform_Controller_RefConfig.set_param(['PregeneratedLibrarySubsystemC' ...
   'odeDiagnostic'], 'warning');
Platform_Controller_RefConfig.set_param('InitInArrayFormatMsg', ['warni' ...
   'ng']);
Platform_Controller_RefConfig.set_param('StrictBusMsg', ['ErrorOnBusTre' ...
   'atedAsVector']);
Platform_Controller_RefConfig.set_param('BusNameAdapt', 'WarnAndRepair');
Platform_Controller_RefConfig.set_param('NonBusSignalsTreatedAsBus', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('SFUnusedDataAndEventsDiag', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('SFUnexpectedBacktrackingDiag', ...
   'warning');
Platform_Controller_RefConfig.set_param(['SFInvalidInputDataAccessInCha' ...
   'rtInitDiag'], 'warning');
Platform_Controller_RefConfig.set_param(['SFNoUnconditionalDefaultTrans' ...
   'itionDiag'], 'warning');
Platform_Controller_RefConfig.set_param(['SFTransitionOutsideNaturalPar' ...
   'entDiag'], 'warning');
Platform_Controller_RefConfig.set_param(['SFUnreachableExecutionPathDia' ...
   'g'], 'warning');
Platform_Controller_RefConfig.set_param(['SFUndirectedBroadcastEventsDi' ...
   'ag'], 'warning');
Platform_Controller_RefConfig.set_param(['SFTransitionActionBeforeCondi' ...
   'tionDiag'], 'warning');
Platform_Controller_RefConfig.set_param(['SFOutputUsedAsStateInMooreCha' ...
   'rtDiag'], 'error');
Platform_Controller_RefConfig.set_param(['SFTemporalDelaySmallerThanSam' ...
   'pleTimeDiag'], 'warning');
Platform_Controller_RefConfig.set_param('SFSelfTransitionDiag', ['warni' ...
   'ng']);
Platform_Controller_RefConfig.set_param(['SFExecutionAtInitializationDi' ...
   'ag'], 'none');
Platform_Controller_RefConfig.set_param('SFMachineParentedDataDiag', ['w' ...
   'arning']);
Platform_Controller_RefConfig.set_param('IntegerSaturationMsg', ['warni' ...
   'ng']);
Platform_Controller_RefConfig.set_param('AllowedUnitSystems', 'all');
Platform_Controller_RefConfig.set_param('UnitsInconsistencyMsg', ['warn' ...
   'ing']);
Platform_Controller_RefConfig.set_param('AllowAutomaticUnitConversions', ...
   'on');
Platform_Controller_RefConfig.set_param('RCSCRenamedMsg', 'warning');
Platform_Controller_RefConfig.set_param('RCSCObservableMsg', 'warning');
Platform_Controller_RefConfig.set_param('ForceCombineOutputUpdateInSim', ...
   'off');
Platform_Controller_RefConfig.set_param('UnderSpecifiedDimensionMsg', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param(['DebugExecutionForFMUViaOutOfP' ...
   'rocess'], 'off');
Platform_Controller_RefConfig.set_param(['ArithmeticOperatorsInVariantC' ...
   'onditions'], 'warning');
Platform_Controller_RefConfig.set_param('VariantConditionMismatch', ['n' ...
   'one']);
Platform_Controller_RefConfig.set_param('ProdHWDeviceType', ['Custom Pr' ...
   'ocessor->Custom Processor']);
Platform_Controller_RefConfig.set_param('ProdBitPerChar', 8);
Platform_Controller_RefConfig.set_param('ProdBitPerShort', 16);
Platform_Controller_RefConfig.set_param('ProdBitPerInt', 32);
Platform_Controller_RefConfig.set_param('ProdBitPerLong', 32);
Platform_Controller_RefConfig.set_param('ProdLongLongMode', 'off');
Platform_Controller_RefConfig.set_param('ProdBitPerPointer', 32);
Platform_Controller_RefConfig.set_param('ProdBitPerSizeT', 32);
Platform_Controller_RefConfig.set_param('ProdBitPerPtrDiffT', 32);
Platform_Controller_RefConfig.set_param('ProdLargestAtomicInteger', ['C' ...
   'har']);
Platform_Controller_RefConfig.set_param('ProdLargestAtomicFloat', ['Dou' ...
   'ble']);
Platform_Controller_RefConfig.set_param('ProdIntDivRoundTo', 'Floor');
Platform_Controller_RefConfig.set_param('ProdEndianess', 'LittleEndian');
Platform_Controller_RefConfig.set_param('ProdWordSize', 32);
Platform_Controller_RefConfig.set_param('ProdShiftRightIntArith', 'on');
Platform_Controller_RefConfig.set_param('ProdEqTarget', 'on');
Platform_Controller_RefConfig.set_param('TargetPreprocMaxBitsSint', 32);
Platform_Controller_RefConfig.set_param('TargetPreprocMaxBitsUint', 32);
Platform_Controller_RefConfig.set_param('HardwareBoardFeatureSet', ['Em' ...
   'beddedCoderHSP']);
Platform_Controller_RefConfig.set_param('UpdateModelReferenceTargets', ...
   'IfOutOfDateOrStructuralChange');
Platform_Controller_RefConfig.set_param(['EnableRefExpFcnMdlSchedulingC' ...
   'hecks'], 'on');
Platform_Controller_RefConfig.set_param(['EnableParallelModelReferenceB' ...
   'uilds'], 'on');
Platform_Controller_RefConfig.set_param(['ParallelModelReferenceErrorOn' ...
   'InvalidPool'], 'on');
Platform_Controller_RefConfig.set_param(['ParallelModelReferenceMATLABW' ...
   'orkerInit'], 'None');
Platform_Controller_RefConfig.set_param(['ModelReferenceNumInstancesAll' ...
   'owed'], 'Multi');
Platform_Controller_RefConfig.set_param('PropagateVarSize', ['Infer fro' ...
   'm blocks in model']);
Platform_Controller_RefConfig.set_param('ModelDependencies', '');
Platform_Controller_RefConfig.set_param(['ModelReferencePassRootInputsB' ...
   'yReference'], 'on');
Platform_Controller_RefConfig.set_param(['ModelReferenceMinAlgLoopOccur' ...
   'rences'], 'off');
Platform_Controller_RefConfig.set_param(['PropagateSignalLabelsOutOfMod' ...
   'el'], 'off');
Platform_Controller_RefConfig.set_param(['SupportModelReferenceSimTarge' ...
   'tCustomCode'], 'off');
Platform_Controller_RefConfig.set_param('UseModelRefSolver', 'off');
Platform_Controller_RefConfig.set_param('SimCustomSourceCode', '');
Platform_Controller_RefConfig.set_param('SimUserSources', '');
Platform_Controller_RefConfig.set_param('SimCustomHeaderCode', '');
Platform_Controller_RefConfig.set_param('SimCustomInitializer', '');
Platform_Controller_RefConfig.set_param('SimCustomTerminator', '');
Platform_Controller_RefConfig.set_param('SimReservedNameArray', []);
Platform_Controller_RefConfig.set_param('SimUserIncludeDirs', '');
Platform_Controller_RefConfig.set_param('SimUserLibraries', '');
Platform_Controller_RefConfig.set_param('SimUserDefines', '');
Platform_Controller_RefConfig.set_param('SimCustomCompilerFlags', '');
Platform_Controller_RefConfig.set_param('SimCustomLinkerFlags', '');
Platform_Controller_RefConfig.set_param('SFSimEnableDebug', 'off');
Platform_Controller_RefConfig.set_param('SFSimEcho', 'on');
Platform_Controller_RefConfig.set_param('SimCtrlC', 'on');
Platform_Controller_RefConfig.set_param('SimIntegrity', 'on');
Platform_Controller_RefConfig.set_param('SimParseCustomCode', 'on');
Platform_Controller_RefConfig.set_param(['SimDebugExecutionForCustomCod' ...
   'e'], 'off');
Platform_Controller_RefConfig.set_param('SimAnalyzeCustomCode', 'off');
Platform_Controller_RefConfig.set_param('SimGenImportedTypeDefs', 'off');
Platform_Controller_RefConfig.set_param('CompileTimeRecursionLimit', 50);
Platform_Controller_RefConfig.set_param('EnableRuntimeRecursion', 'on');
Platform_Controller_RefConfig.set_param('EnableImplicitExpansion', 'on');
Platform_Controller_RefConfig.set_param('MATLABDynamicMemAlloc', 'off');
Platform_Controller_RefConfig.set_param(['LegacyBehaviorForPersistentVa' ...
   'rInContinuousTime'], 'off');
Platform_Controller_RefConfig.set_param('CustomCodeFunctionArrayLayout', ...
   []);
Platform_Controller_RefConfig.set_param(['DefaultCustomCodeFunctionArra' ...
   'yLayout'], 'NotSpecified');
Platform_Controller_RefConfig.set_param('CustomCodeUndefinedFunction', ...
   'FilterOut');
Platform_Controller_RefConfig.set_param('CustomCodeGlobalsAsFunctionIO', ...
   'off');
Platform_Controller_RefConfig.set_param(['DefaultCustomCodeDeterministi' ...
   'cFunctions'], 'None');
Platform_Controller_RefConfig.set_param('SimHardwareAcceleration', ['ge' ...
   'neric']);
Platform_Controller_RefConfig.set_param('GPUAcceleration', 'off');
Platform_Controller_RefConfig.set_param('SimTargetLang', 'C');
Platform_Controller_RefConfig.set_param('RemoveResetFunc', 'on');
Platform_Controller_RefConfig.set_param('ExistingSharedCode', '');
Platform_Controller_RefConfig.set_param('TLCOptions', '');
Platform_Controller_RefConfig.set_param('GenCodeOnly', 'on');
Platform_Controller_RefConfig.set_param(['PackageGeneratedCodeAndArtifa' ...
   'cts'], 'on');
Platform_Controller_RefConfig.set_param('PackageName', ['vizgard_code.z' ...
   'ip']);
Platform_Controller_RefConfig.set_param('PostCodeGenCommand', '');
Platform_Controller_RefConfig.set_param('GenerateReport', 'on');
Platform_Controller_RefConfig.set_param('RTWVerbose', 'on');
Platform_Controller_RefConfig.set_param('RetainRTWFile', 'off');
Platform_Controller_RefConfig.set_param('ProfileTLC', 'off');
Platform_Controller_RefConfig.set_param('TLCDebug', 'off');
Platform_Controller_RefConfig.set_param('TLCCoverage', 'off');
Platform_Controller_RefConfig.set_param('TLCAssert', 'off');
Platform_Controller_RefConfig.set_param('RTWUseSimCustomCode', 'off');
Platform_Controller_RefConfig.set_param('CustomSourceCode', '');
Platform_Controller_RefConfig.set_param('CustomHeaderCode', '');
Platform_Controller_RefConfig.set_param('CustomInclude', '');
Platform_Controller_RefConfig.set_param('CustomSource', '');
Platform_Controller_RefConfig.set_param('CustomLibrary', '');
Platform_Controller_RefConfig.set_param('CustomDefine', '');
Platform_Controller_RefConfig.set_param('CustomBLASCallback', '');
Platform_Controller_RefConfig.set_param('CustomLAPACKCallback', '');
Platform_Controller_RefConfig.set_param('CustomFFTCallback', '');
Platform_Controller_RefConfig.set_param('CustomInitializer', '');
Platform_Controller_RefConfig.set_param('CustomTerminator', '');
Platform_Controller_RefConfig.set_param('Toolchain', ['Automatically lo' ...
   'cate an installed toolchain']);
Platform_Controller_RefConfig.set_param('BuildConfiguration', ['Faster ' ...
   'Runs']);
Platform_Controller_RefConfig.set_param('IncludeHyperlinkInReport', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('LaunchReport', 'off');
Platform_Controller_RefConfig.set_param('PortableWordSizes', 'off');
Platform_Controller_RefConfig.set_param('CreateSILPILBlock', 'None');
Platform_Controller_RefConfig.set_param('CodeExecutionProfiling', 'off');
Platform_Controller_RefConfig.set_param('CodeProfilingInstrumentation', ...
   'off');
Platform_Controller_RefConfig.set_param('CodeStackProfiling', 'off');
Platform_Controller_RefConfig.set_param('SILDebugging', 'off');
Platform_Controller_RefConfig.set_param('TargetLangStandard', ['C99 (IS' ...
   'O)']);
Platform_Controller_RefConfig.set_param('GenerateTraceInfo', 'on');
Platform_Controller_RefConfig.set_param('GenerateTraceReport', 'on');
Platform_Controller_RefConfig.set_param('GenerateTraceReportSl', 'off');
Platform_Controller_RefConfig.set_param('GenerateTraceReportSf', 'off');
Platform_Controller_RefConfig.set_param('GenerateTraceReportEml', 'off');
Platform_Controller_RefConfig.set_param('GenerateWebview', 'off');
Platform_Controller_RefConfig.set_param('GenerateCodeMetricsReport', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('GenerateCodeReplacementReport', ...
   'on');
saveVarsTmp{1} = cell(1, 1);
saveVarsTmp{1}{1} = 'Traceability';
Platform_Controller_RefConfig.set_param('ObjectivePriorities', saveVarsTmp{1});
Platform_Controller_RefConfig.set_param('CheckMdlBeforeBuild', 'Off');
Platform_Controller_RefConfig.set_param('GenerateComments', 'off');
Platform_Controller_RefConfig.set_param('CommentStyle', 'Auto');
Platform_Controller_RefConfig.set_param('IgnoreCustomStorageClasses', ['o' ...
   'ff']);
Platform_Controller_RefConfig.set_param('IgnoreTestpoints', 'on');
Platform_Controller_RefConfig.set_param('MaxIdLength', 31);
Platform_Controller_RefConfig.set_param('MangleLength', 1);
Platform_Controller_RefConfig.set_param('SharedChecksumLength', 8);
Platform_Controller_RefConfig.set_param('CustomSymbolStrGlobalVar', ['r' ...
   't$N$M']);
Platform_Controller_RefConfig.set_param('CustomSymbolStrType', '$N$M');
Platform_Controller_RefConfig.set_param('CustomSymbolStrField', '$N$M');
Platform_Controller_RefConfig.set_param('CustomSymbolStrFcn', ['PlatCon' ...
   '_$N$M$F']);
Platform_Controller_RefConfig.set_param('CustomSymbolStrFcnArg', ['rt$I' ...
   '$N$M']);
Platform_Controller_RefConfig.set_param('CustomSymbolStrBlkIO', ['rtb_$' ...
   'N[u]$M']);
Platform_Controller_RefConfig.set_param('CustomSymbolStrTmpVar', '$N$M');
Platform_Controller_RefConfig.set_param('CustomSymbolStrMacro', '$N$M');
Platform_Controller_RefConfig.set_param('CustomSymbolStrEmxType', ['emx' ...
   'Array_$M$N']);
Platform_Controller_RefConfig.set_param('CustomSymbolStrEmxFcn', ['emx$' ...
   'M$N']);
Platform_Controller_RefConfig.set_param('CustomUserTokenString', '');
Platform_Controller_RefConfig.set_param('DefineNamingRule', 'None');
Platform_Controller_RefConfig.set_param('ParamNamingRule', 'None');
Platform_Controller_RefConfig.set_param('SignalNamingRule', 'None');
Platform_Controller_RefConfig.set_param('InternalIdentifier', ['Shorten' ...
   'ed']);
Platform_Controller_RefConfig.set_param('InlinedPrmAccess', 'Literals');
Platform_Controller_RefConfig.set_param('UseSimReservedNames', 'off');
Platform_Controller_RefConfig.set_param('ReservedNameArray', []);
Platform_Controller_RefConfig.set_param('EnumMemberNameClash', 'error');
Platform_Controller_RefConfig.set_param('TargetLibSuffix', '');
Platform_Controller_RefConfig.set_param('TargetPreCompLibLocation', '');
Platform_Controller_RefConfig.set_param('CodeReplacementLibrary', ['Non' ...
   'e']);
Platform_Controller_RefConfig.set_param('UtilityFuncGeneration', ['Shar' ...
   'ed location']);
Platform_Controller_RefConfig.set_param('MultiwordTypeDef', ['System de' ...
   'fined']);
Platform_Controller_RefConfig.set_param('DynamicStringBufferSize', 256);
Platform_Controller_RefConfig.set_param('GenerateFullHeader', 'off');
Platform_Controller_RefConfig.set_param('InferredTypesCompatibility', ['o' ...
   'ff']);
Platform_Controller_RefConfig.set_param('GenerateSampleERTMain', 'off');
Platform_Controller_RefConfig.set_param('IncludeMdlTerminateFcn', 'on');
Platform_Controller_RefConfig.set_param('GRTInterface', 'off');
Platform_Controller_RefConfig.set_param('CombineOutputUpdateFcns', 'on');
Platform_Controller_RefConfig.set_param('CombineSignalStateStructs', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('GroupInternalDataByFunction', ...
   'off');
Platform_Controller_RefConfig.set_param('MatFileLogging', 'off');
Platform_Controller_RefConfig.set_param('SuppressErrorStatus', 'on');
Platform_Controller_RefConfig.set_param('IncludeFileDelimiter', 'Auto');
Platform_Controller_RefConfig.set_param('ERTCustomFileBanners', 'off');
Platform_Controller_RefConfig.set_param('SupportAbsoluteTime', 'off');
Platform_Controller_RefConfig.set_param('PurelyIntegerCode', 'off');
Platform_Controller_RefConfig.set_param('SupportNonFinite', 'off');
Platform_Controller_RefConfig.set_param('SupportComplex', 'off');
Platform_Controller_RefConfig.set_param('SupportContinuousTime', 'off');
Platform_Controller_RefConfig.set_param('SupportNonInlinedSFcns', 'off');
Platform_Controller_RefConfig.set_param('RemoveDisableFunc', 'off');
Platform_Controller_RefConfig.set_param('SupportVariableSizeSignals', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('ParenthesesLevel', 'Nominal');
Platform_Controller_RefConfig.set_param('CastingMode', 'Nominal');
Platform_Controller_RefConfig.set_param('ArrayLayout', 'Column-major');
Platform_Controller_RefConfig.set_param('GenerateSharedConstants', 'on');
Platform_Controller_RefConfig.set_param(['LUTObjectStructOrderExplicitV' ...
   'alues'], 'Size,Breakpoints,Table');
Platform_Controller_RefConfig.set_param(['LUTObjectStructOrderEvenSpaci' ...
   'ng'], 'Size,Breakpoints,Table');
Platform_Controller_RefConfig.set_param('ERTHeaderFileRootName', '$R$E');
Platform_Controller_RefConfig.set_param('ERTSourceFileRootName', '$R$E');
Platform_Controller_RefConfig.set_param('ERTFilePackagingFormat', ['Com' ...
   'pact']);
Platform_Controller_RefConfig.set_param('ExtMode', 'off');
Platform_Controller_RefConfig.set_param('ExtModeTransport', 0);
Platform_Controller_RefConfig.set_param('ExtModeMexFile', 'ext_comm');
Platform_Controller_RefConfig.set_param('ExtModeStaticAlloc', 'off');
Platform_Controller_RefConfig.set_param('GlobalDataDefinition', 'Auto');
Platform_Controller_RefConfig.set_param('GlobalDataReference', 'Auto');
Platform_Controller_RefConfig.set_param('EnableUserReplacementTypes', ['o' ...
   'ff']);
Platform_Controller_RefConfig.set_param('DSAsUniqueAccess', 'off');
Platform_Controller_RefConfig.set_param('ExtModeTesting', 'off');
Platform_Controller_RefConfig.set_param('ExtModeMexArgs', '');
Platform_Controller_RefConfig.set_param('ExtModeIntrfLevel', 'Level1');
Platform_Controller_RefConfig.set_param('RTWCAPISignals', 'off');
Platform_Controller_RefConfig.set_param('RTWCAPIParams', 'off');
Platform_Controller_RefConfig.set_param('RTWCAPIStates', 'off');
Platform_Controller_RefConfig.set_param('RTWCAPIRootIO', 'off');
Platform_Controller_RefConfig.set_param('ERTSrcFileBannerTemplate', ['e' ...
   'rt_code_template.cgt']);
Platform_Controller_RefConfig.set_param('ERTHdrFileBannerTemplate', ['e' ...
   'rt_code_template.cgt']);
Platform_Controller_RefConfig.set_param('ERTDataSrcFileTemplate', ['ert' ...
   '_code_template.cgt']);
Platform_Controller_RefConfig.set_param('ERTDataHdrFileTemplate', ['ert' ...
   '_code_template.cgt']);
Platform_Controller_RefConfig.set_param('ERTCustomFileTemplate', ['exam' ...
   'ple_file_process.tlc']);
Platform_Controller_RefConfig.set_param('EnableDataOwnership', 'off');
Platform_Controller_RefConfig.set_param('SignalDisplayLevel', 10);
Platform_Controller_RefConfig.set_param('ParamTuneLevel', 10);
Platform_Controller_RefConfig.set_param('RateTransitionBlockCode', ['In' ...
   'line']);
Platform_Controller_RefConfig.set_param('PreserveExpressionOrder', ['of' ...
   'f']);
Platform_Controller_RefConfig.set_param('PreserveIfCondition', 'off');
Platform_Controller_RefConfig.set_param('ConvertIfToSwitch', 'on');
Platform_Controller_RefConfig.set_param('PreserveExternInFcnDecls', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param('PreserveStaticInFcnDecls', ['o' ...
   'n']);
Platform_Controller_RefConfig.set_param(['SuppressUnreachableDefaultCas' ...
   'es'], 'on');
Platform_Controller_RefConfig.set_param('EnableSignedLeftShifts', 'on');
Platform_Controller_RefConfig.set_param('EnableSignedRightShifts', 'on');
Platform_Controller_RefConfig.set_param('IndentStyle', 'Allman');
Platform_Controller_RefConfig.set_param('IndentSize', '4');
Platform_Controller_RefConfig.set_param('NewlineStyle', 'Default');
Platform_Controller_RefConfig.set_param('MaxLineWidth', 120);
Platform_Controller_RefConfig.set_param('MaxIdInt64', 'MAX_int64_T');
Platform_Controller_RefConfig.set_param('MinIdInt64', 'MIN_int64_T');
Platform_Controller_RefConfig.set_param('MaxIdUint64', 'MAX_uint64_T');
Platform_Controller_RefConfig.set_param('MaxIdInt32', 'MAX_int32_T');
Platform_Controller_RefConfig.set_param('MinIdInt32', 'MIN_int32_T');
Platform_Controller_RefConfig.set_param('MaxIdUint32', 'MAX_uint32_T');
Platform_Controller_RefConfig.set_param('MaxIdInt16', 'MAX_int16_T');
Platform_Controller_RefConfig.set_param('MinIdInt16', 'MIN_int16_T');
Platform_Controller_RefConfig.set_param('MaxIdUint16', 'MAX_uint16_T');
Platform_Controller_RefConfig.set_param('MaxIdInt8', 'MAX_int8_T');
Platform_Controller_RefConfig.set_param('MinIdInt8', 'MIN_int8_T');
Platform_Controller_RefConfig.set_param('MaxIdUint8', 'MAX_uint8_T');
Platform_Controller_RefConfig.set_param('BooleanTrueId', 'true');
Platform_Controller_RefConfig.set_param('BooleanFalseId', 'false');
Platform_Controller_RefConfig.set_param(['TypeLimitIdReplacementHeaderF' ...
   'ile'], '');
Platform_Controller_RefConfig.set_param('CovModelRefEnable', 'off');
Platform_Controller_RefConfig.set_param('RecordCoverage', 'off');
Platform_Controller_RefConfig.set_param('CovEnable', 'off');
clear saveVarsTmp;

