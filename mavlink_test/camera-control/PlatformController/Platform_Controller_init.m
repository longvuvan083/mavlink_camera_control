%% Define the Project paths
setpath_Dev;

%% Two-axis Platform Model Specs
SampleTime = 1/30;
SampleTimeFast = SampleTime/10;
EOD.Freq = 12;
EOD.Damping = 0.95;
EOD.AbsoluteHeading = -80 *pi/180;      % Azimuth Zero in world bearing.

FoV_ratio = 1920/(60*pi/180);

% VelFfwdSOLP_Damp = 0.7;
% VelFfwdSOLP_Wn = 2*pi*1.0;  % model already includes a gain of 2*pi !?!
TgtVel = 300;
TgtMinRange = 1000;
                        % Select the Target Motion behaviour.
TARGET_TYPE = 0;        % 0=sinusoidal, 1=step, 2=const vel, 3=const accel.

%% Load the Model Reference Configuration
Platform_Controller_ModelReferencingConfig;     % CRITICAL to CodeGen results!!!

%% PID Controller Coeffs  (this method creates a structure in c-code for the parameter set and defines the name of the structure type)
temp.PID_P = single(0.724);     % 0.724
temp.PID_I = single(0.25);       % 0.046
temp.PID_D = single(0.009);      % 0.009
temp.Control_Gain = single(0.5);
temp.NoOfRateLevels = uint16(255);
temp.PlatformMaxRate = single(74);
temp.BleedEnable = true;
temp.BleedRate = single(14);
temp.RoundDitherEnable = false;
temp.AxesToTest= Enum_AxesToTest.X_AXIS;

Controller_param = Simulink.Parameter(temp);        % create a unified structure to hold parameters
Controller_param.CoderInfo.StorageClass = 'ExportedGlobal';

% create bus from struct
temp = Simulink.Bus.createObject(Controller_param.Value);        % create a struct definition as well.
Controller_param_type = eval([temp.busName '.copy']);            % eval(temp.busName);
Controller_param.DataType = 'Bus: Controller_param_type';
eval(['clear ' temp.busName])
clear temp;

% load pure buses
Timing_bus;
Status_bus;


%% Modify setting so code for model is generated in specified Codegen subfolder
fileGenCfg = Simulink.fileGenControl('getConfig');
fileGenCfg.CodeGenFolder = [pwd '\Codegen'];
fileGenCfg.CacheFolder = [pwd '\Codegen'];
Simulink.fileGenControl('setConfig', 'config', fileGenCfg);


% Director1.ControllerPID_I = 0.2 
% Director1.ControllerPID_D = 0.0092 
% Director1.ControllerPID_P = 0.72416
% Director1.ControllerGain = 0.5								
% Director1.NoOfRateLevels = 255
% Director1.PlatformMaxRate = 74
% Director1.BleedEnable = 0           								
% Director1.BleedRate = 30
% Director1.RoundDitherEnable = 0






