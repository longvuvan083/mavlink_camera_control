#include "./tcp-client/client.h"
#include <memory>
#include <thread>
int main() {
    // Cam manager
    boost::asio::io_service ios1, ios_;
    std::unique_ptr<Client> cam_client; //Client(ios, "", 0, 0, "real", false);
    // cam_client = std::unique_ptr<Client>(new Client(ios1, "", 0, 0, "", false));
    // cam_client->connectToManager("192.168.0.203", 10024, 0);

    // std::string ip = "192.160.1.10";
    std::string ip = "192.168.0.203";
    cam_client = std::unique_ptr<Client>(new Client(ios1, "", 0, 0, "real", false));
	cam_client->setParameters(ip, 10024, 0);
	// cam_client->loadLut(config.camcontrol.lut_address);
	cam_client->updateInterface(4);
	cam_client->setCameraInterval(13);

    cam_client->connectToManager(ip, 10024, 0);
    // uint8_t* packet;
    std::vector<char> msg;
    // for (int i = 0; i < 15; i++) 
    // // while (true)
    //     cam_client->panLeft(msg, 5, 1);
    // for (int i = 0; i < 10; i++) 
    //     cam_client->panRight(msg, 5, 1);
    // for (int i = 0; i < 10; i++) 
    //     cam_client->tiltUp(msg, 5, 1);
    // for (int i = 0; i < 10; i++) 
    //     cam_client->tiltPanUpLeft(msg,5, 5, 1);
    // for (int i = 0; i < 10; i++) 
    //     cam_client->tiltPanDownRight(msg,5, 5, 1);
    // cam_client->fovAbsolute(msg,  60, 1);
    // for (int i = 0; i < 5; i++) 
        // cam_client->zoomOut(msg, 1);
        // cam_client->stopCamera(msg, 1);
    // for (int i = 0; i < 10; i++) 
    // cam_client->zoomIn(msg, 1);
    std::thread t_query;
    t_query = std::thread([&]() {
        while (true) {
            cam_client->testQuery();
        }
        std::cout << "t_query exit\n";
    });

    std::thread t_read;
    t_read = std::thread([&]() {
        while (true) {
        cam_client->handleReadUDP();
        }
        std::cout << "t_query exit\n";
    });
    if (t_query.joinable()) t_query.join();
    if (t_read.joinable()) t_read.join();

   
    return 0;
}