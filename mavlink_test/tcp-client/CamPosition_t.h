#ifndef CAM_POSITION_H
#define CAM_POSITION_H
#include <iostream>
#include <sstream>
#include <iomanip>

struct CamPosition_t
{
	CamPosition_t() : tilt{ 0 }, tilt_nan{ true }, pan{ 0 }, pan_nan{ true }, fov{ 0 }, fov_nan{ true } {}
	float tilt;
	bool tilt_nan = true;
	float pan;
	bool pan_nan = true;
	float fov;
	bool fov_nan = true;
	float zoom;
	bool zoom_nan = true;
	friend std::ostream& operator<<(std::ostream& os, const CamPosition_t& pos)
	{
		os << "Pan: " << (pos.pan_nan ? "nan" : (std::to_string(pos.pan) + " deg"))
			<< " | Tilt: " << (pos.tilt_nan ? "nan" : (std::to_string(pos.tilt) + " deg"));
		if (!pos.zoom_nan)
			os << " | Zoom: " << (std::to_string(pos.fov) + "%");
		else
			os << " | FOV: " << (pos.fov_nan ? "nan" : (std::to_string(pos.zoom) + " deg"));
		return os;
	}
	std::string str(float north_bearing_offset)
	{
		std::ostringstream oss;

		// Print Pan
		float pan2stream = pan - north_bearing_offset;
		while (pan2stream < 0)
			pan2stream += 360.0f;
		while (pan2stream > 360.0f)
			pan2stream -= 360.0f;
		oss << "Pan: ";
		if (pan_nan)
			oss << "nan";
		else
			oss << std::fixed << std::setprecision(2) << pan2stream << " deg";
		// Print Tilt
		oss << " | Tilt: ";
		if (tilt_nan)
			oss << "nan";
		else
			oss << std::fixed << std::setprecision(2) << tilt << " deg";
		// Print Fov
		if (!zoom_nan)
		{
			oss << " | Zoom: " << std::fixed << std::setprecision(2) << zoom << " %";
		}
		else
		{
			oss << " | FOV: ";
			if (fov_nan)
				oss << "nan";
			else
				oss << std::fixed << std::setprecision(2) << fov << " deg";
		}
		return oss.str();
	}
};

#endif // CAM_POSITION_H
