#include "client.h"
#include <boost/lambda/lambda.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <optional>
#include <iterator>
#include <algorithm>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <bitset>
#include <boost/system/error_code.hpp>
#include <boost/optional.hpp>
#include <boost/system.hpp>
#include <boost/bind.hpp>
// Linux headers
#include <fcntl.h>	 // Contains file controls like O_RDWR
#include <errno.h>	 // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h>	 // write(), read(), close()
#include <thread>
#include <vector>
#include <memory>
#include <chrono>

unsigned long get_baud(unsigned long baud)
{
    switch (baud) {
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    default: 
        return B0;
    }
}

char get_zoom_msg(int zoom) {
	return zoom == 0 ? 0x00 : (zoom > 0 ? 0x20 : 0x40);
}

int parse(const char* res, int len)
{
	int return_index = -1, i = 0;
	while (i < len)
	{
		if ((*(res + i) & 0xff) == 0xff)
		{
			if ((*(res + i + 1) & 0xff) == 0x01 && (*(res + i + 2) & 0xff) == 0x00 && (*(res + i + 3) & 0xff) == 0x01)
			{
				i += 4;
				continue;
			}
			else
			{
				return_index = i;
				break;
			}
		}
		++i;
	}
	std::cout << std::endl;
	return return_index;
}

// Constructor
Client::Client(boost::asio::io_service& ios, string ip, int port, int priority, std::string lut_address, bool verbose) : // io context
	m_io_service(ios),
	// socket creation
	m_active_socket(ios),
	// connection parameters
	m_ip_address(ip),
	m_target_port(port),
	m_connection_priority(priority),
	// timeout
	m_deadline_timer(ios),
	m_active_socket_udp(ios),

	// lut
	m_lut()
	{
		// cout << "Client started!";
		setVerbose(verbose);
		const int timeout = 200;
		::setsockopt(m_active_socket.native_handle(), SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);
		// If an address to LUT file is specified, try to load it
		if (lut_address != "")
		{
			loadLut(lut_address);
		}
		// connectToManager(ip, port, priority);
	}



void Client::setCameraInterval(int interval) {
	camera_interval = interval;
}

void Client::setParameters(string ip, int port, int priority)
{
	m_ip_address = ip;
	m_target_port = port;
	m_connection_priority = priority;
}

void Client::setVerbose(bool verbose = false)
{
	Client::m_verbose_debug = verbose;
}

std::string Client::write_serial_nores(std::vector<char>& msg) {
	// m_verbose_debug = 1;
	int check_writeable;
    // if (config.director.verbose_debug)
	if (true)
    {
        // cout << "Msg to be sent: " << string(msg.begin(), msg.end()) << std::endl;
		for (char c : msg)
		printf("%x-", (uint8_t) c);
		// printf("\\x%x", (uint8_t) c);
		std::cout << "\n";
    }
   check_writeable = write(serial_port, (void *)msg.data(), sizeof(msg));
	if (check_writeable < 0){ m_connected_status = false; }
	// std::this_thread::sleep_for(std::chrono::milliseconds(config.director.camera_interval));
	std::this_thread::sleep_for(std::chrono::milliseconds(13));
	return "OK";
}

std::string Client::write_serial_nores(std::string& msg) {
    // std::lock_guard<std::mutex> lock(m_mutex);
	// m_verbose_debug = 1;
	int check_writeable;
    // if (config.director.verbose_debug)
	// if (m_verbose_debug)
	if (true)
	{
		std::cout << "Msg to be sent: " << msg << std::endl;
	}
    check_writeable = write(serial_port, (void *)msg.c_str(), sizeof(msg));
	if (check_writeable < 0){ m_connected_status = false; }
	// std::this_thread::sleep_for(std::chrono::milliseconds(config.director.camera_interval));
	std::this_thread::sleep_for(std::chrono::milliseconds(13));
	return "OK";
}

int Client::connectToManager(const std::string& ip, int port, int priority)
{
	Platform_Controller_initialize();
	if (m_verbose_debug)
	{
		cout << "attempting to connect socket | IP:" << ip << ", port:" << port << ", priority:" << priority << endl;
	}
	if (control_interface != 4) {
		try
		{
			m_active_socket.connect(tcp::endpoint(boost::asio::ip::address::from_string(ip), port));
			// cout << "Socket connected" << endl;
			m_connected_status = true;
		}
		catch (const boost::system::system_error& ex)
		{
			cout << "Socket connection failed" << endl;
			cout << "Boost sys error" << ex.what() << endl;
			m_connected_status = false;
		}
		catch (std::exception& e)
		{
			cout << "Socket connection failed" << endl;
			cout << "Exception caught" << e.what() << endl;
			m_connected_status = false;
		}

		if (m_connected_status == true)
		{
			std::vector<char> msg = std::vector<char>(7);
			testConnection(msg);		// Returns cam manager id and version
			setPriority(msg, priority); // Sets priority
		}

	}
	else {
		try {
			m_active_socket_udp.set_option(boost::asio::ip::udp::socket::reuse_address(true));
			boost::system::error_code ec;
			m_active_socket_udp.bind(udp::endpoint(boost::asio::ip::address_v4::any(), port));
 			m_active_socket_udp.connect(udp::endpoint(ip::address::from_string(ip), port));
			// udp::resolver resolver(ios);
			// udp::resolver::query query(udp::v4(), ip.c_str(), port);
			// udp::endpoint receiver_endpoint = *resolver.resolve(query);
			

			remote_endpoint = udp::endpoint(udp::endpoint(ip::address::from_string(ip), port));
			m_connected_status = true;

		}
		catch (const boost::system::system_error& ex) {
			cout << "Socket connection failed" << endl;
			cout << "Boost sys error" << ex.what() << endl;
			m_connected_status = false;
		}
		catch (std::exception& e)
		{
			cout << "Socket connection failed" << endl;
			cout << "Exception caught" << e.what() << endl;
			m_connected_status = false;
		}
		if (m_connected_status == true)
		{

			// std::vector<char> msg = std::vector<char>(7);
			// testConnection(msg);		// Returns cam manager id and version
			// setPriority(msg, priority); // Sets priority
			
		}
		std::cout <<  "m_connected_status = " << m_connected_status << "\n";
	}
};

std::string Client::sendUDPnores(std::vector<uint8_t>& msg) {
	if (m_verbose_debug)
	{
		if (m_verbose_debug)
		{
			// cout << "Sending message (no response expected): " << msg << endl;
		}
	}
	// request/message from client
	if (m_verbose_debug)
	{
		// cout << "Msg to be sent: " << msg << endl;
	}

	boost::system::error_code error;
	// std::cout << "Data send = " << "\n";
	// for (int i = 0 ; i < msg.size(); i++ ){
	// 	printf("%x-", (msg[i]));
	// }
	// std::cout << "\n";
	int data_send = m_active_socket_udp.send_to(boost::asio::buffer(msg), remote_endpoint, 0, error);
	if (!error)
	{
		if (m_verbose_debug)
		{
			cout << "Client sent message!" << endl;
		}
	}
	else
	{
		if (m_verbose_debug)
		{
			cout << "send failed: " << error.message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	if (m_verbose_debug)
	{
		cout << "End of sending" << endl;
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	return "OK";
}

void Client::handleReadUDP() {
	if (m_verbose_debug)
	{
		if (m_verbose_debug)
		{
			// cout << "Sending message (no response expected): " << msg << endl;
		}
	}
	// request/message from client
	if (m_verbose_debug)
	{
		// cout << "Msg to be sent: " << msg << endl;
	}
	boost::system::error_code error;
	std::vector<uint8_t> recv_msg(1024);
	// std::array<char, 1024> recv_buffer;
	std::string camip = "192.168.0.203";
	remote_endpoint = udp::endpoint(ip::address::from_string(camip), 10024);
	
	// std::cout<<"sender_endpoint = " << remote_endpoint <<std::endl;
	try {
		// int data_recv = m_active_socket_udp.async_receive_from(boost::asio::buffer(recv_msg, 1024), 0, remote_endpoint); 
		// m_active_socket_udp.async_receive_from(
        //     boost::asio::buffer(recv_msg), remote_endpoint,
        //     boost::bind(&Client::handleRecv, this,
        //         boost::asio::placeholders::error,
        //         boost::asio::placeholders::bytes_transferred));
		int data_recv = m_active_socket_udp.receive_from(boost::asio::buffer(recv_msg, 1024), remote_endpoint); 
	}
	catch (const boost::system::system_error& ex) {
		cout << "Socket recv failed" << endl;
		cout << "Boost sys error" << ex.what() << endl;
		m_connected_status = false;
	}
	std::cout << "Data recived = " << "\n\n";
	for (int i = 0 ; i < recv_msg.size(); i++ ){
		printf("%x-", (recv_msg[i]));
	}
	// for (char c : recv_buffer)
	// 	printf("%x-", (uint8_t) c);
	// std::cout << "\n\n";
	// std::cout.write(recv_buffer.data(),1024);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

void Client::handleRecv(const boost::system::error_code& error, size_t bytes_transferred) {
	if (error) {
		std::cout << "Receive failed: " << error.message() << "\n";
	}
}
int Client::connectToManager(const std::string& ip, int port, int priority, bool& exit_flag)
{
	Platform_Controller_initialize();
	if (m_verbose_debug)
	{
		cout << "attempting to connect socket | IP:" << ip << ", port:" << port << ", priority:" << priority << endl;
	}
	boost::optional<boost::system::error_code> connect_result;
	bool& _m_connected_status = this->m_connected_status;
	try
	{
		m_active_socket.async_connect(tcp::endpoint(boost::asio::ip::address::from_string(ip), port),
			[&_m_connected_status](const boost::system::error_code& error)
			{
				if (error)
				{
					//   std::cout << "REFUSED\n";
					_m_connected_status = false;
				}
				else
				{
					//   std::cout << "CONNECTED\n";
					_m_connected_status = true;
				}
			});
		// cout << "---- Socket wait connect" << endl;
		while (m_io_service.run_one() && !exit_flag)
		{
			// cout << "LINE 205 " << exit_flag << endl;
			if (m_connected_status)
			{
				break;
			}
			// cout << "LINE 210 " << exit_flag << endl;
		}
		// cout << "LINE 212 " << exit_flag << endl;
		if (exit_flag)
		{
			// cout << "LINE 215 " << exit_flag << endl;
			return 1;
		}
	}
	catch (const boost::system::system_error& ex)
	{
		cout << "Socket connection failed" << endl;
		cout << "Boost sys error" << endl;
		m_connected_status = false;
	}
	catch (std::exception& e)
	{
		cout << "Socket connection failed" << endl;
		cout << "Exception caught" << endl;
		m_connected_status = false;
	}
	if (m_connected_status == true)
	{
		std::vector<char> msg = std::vector<char>(7);
		testConnection(msg); // Returns cam manager id and version
		// cout << "testConnection" << endl;
		setPriority(msg, priority); // Sets priority
									// cout << "Sets priority" << endl;
	}
};

void Client::OnConnect(const boost::system::error_code& ec)
{
	std::cout << "[ OnConnect ] " << std::endl;
	if (ec)
		m_connected_status = false;
	else
		m_connected_status = true;
}

void Client::testConnection(std::vector<char>& msg)
{
	// cout << "Testing Connection and Cam Manager Version" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = 0x01;
		msg[2] = 0x00;
		msg[3] = 0x45;
		msg[4] = (100 >= 254) ? 0x08 : 0x05;
		msg[5] = 0x00;
		msg[6] = msg[1] + msg[2] + msg[3] + msg[4] + msg[5];
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {}
}

void Client::setPriority(std::vector<char>& msg, int priority, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		// cout << "Setting PRIORITY to: " << priority << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x04;
		msg[4] = 0x00;
		msg[5] = priority % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {}
}
std::string Client::write_serial_(std::vector<char>& msg) {
    std::lock_guard<std::mutex> lock(m_mutex);
	// m_verbose_debug = 1;
	int check_writeable;
    // if (config.director.verbose_debug)
	if (m_verbose_debug)
    {
        cout << "Msg to be sent: " ; //<< string(msg.begin(), msg.end()) << std::endl;
		for (char c : msg)
		  printf("%x", (uint8_t) c);
		std::cout << "\n";
    }
    // check_writeable = write(serial_port, (void *)msg.data(), sizeof(msg));
	check_writeable = write(serial_port, (void *)msg.data(), sizeof(msg));

	if (check_writeable < 0){ m_connected_status = false; }
    char read_buf [10];
    memset(&read_buf, '\0', sizeof(read_buf));
    // int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));
	int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));

	std::this_thread::sleep_for(std::chrono::milliseconds(13));
    // n is the number of bytes read. n may be 0 if no bytes were received, and can also be -1 to signal an error.
    if (num_bytes < 0) {
        printf("Error reading: %s", strerror(errno));
		return "";
    }
    else {
		return std::string(read_buf, num_bytes);
	}
}
std::string Client::write_serial_(std::string& msg) {
	std::lock_guard<std::mutex> lock(m_mutex);
	// m_verbose_debug = 1;
	int check_writeable;
	if (this->m_verbose_debug){std::cout << "Msg to be sent: " << msg << std::endl;}

    // check_writeable = write(serial_port, (void *)msg.c_str(), msg.length());
	check_writeable = write(serial_port, (void *)msg.c_str(), msg.length());
	if (check_writeable < 0){ m_connected_status = false; }
    char read_buf [100];
    memset(&read_buf, '\0', sizeof(read_buf));
    int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));
    // n is the number of bytes read. n may be 0 if no bytes were received, and can also be -1 to signal an error.
	std::this_thread::sleep_for(std::chrono::milliseconds(13));
    if (num_bytes < 0) {
			printf("Error reading: %s", strerror(errno));
			return "";
	}
    else {
		return std::string(read_buf);
	}
}

std::string Client::queryPan(std::vector<char>& msg, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		// cout << "Querying PAN" << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x51;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// printf("%x %x %x %x %x %x %x\n", msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6]);
		std::string out;
		if (control_interface == 1) out = sendTcp(msg);
		else if (control_interface == 2) out = write_serial_(msg);
		return (out);
	}
	else if (control_interface == 3) {
		// cout << "Querying PAN" << std::endl;
		std::string msg_("[QOS:PT.AZ.AB::**]");
		std::string response = write_serial_(msg_);
		return response;
	}
}

std::string Client::queryTilt(std::vector<char>& msg, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		// cout << "Querying TILT" << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x53;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		std::string out;
		if (control_interface == 1) out = sendTcp(msg);
		else if (control_interface == 2) out = write_serial_(msg);
		return (out);
	}
	else if (control_interface == 3) {
		// cout << "Querying TILT" << std::endl;
		std::string msg_("[QOS:PT.EL.AB::**]");
		std::string response = write_serial_(msg_);
		return response;
	}
}

std::string Client::queryZoom(std::vector<char>& msg, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		// cout << "Querying ZOOM" << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x55;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		std::string out;
		if (control_interface == 1) out = sendTcp(msg);
		else if (control_interface == 2) out = write_serial_(msg);
		return (out);
	}
	else if (control_interface == 3) {
		// cout << "Querying ZOOM" << std::endl;
		std::string msg_("[QOS:LR.ZM.AB::**]");
		std::string response = write_serial_(msg_);
		return response;
	}
}

int Client::getZoom(std::vector<char>& msg, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		std::string data;
		std::uint8_t data0;
		std::uint8_t data1;
		// display the current camera zoom / FoV level.
		data = queryZoom(msg, cam_address);
		// if data is received
		if (data != "")
		{
			data = data.substr(4, 2); // filters data bytes received
									// cast bytes to unsigned int
			data0 = data[0];
			data1 = data[1];

			int zoom_raw = int((data0 << 8) | data1); // combines bytes via bitwise shift

			return zoom_raw;
		}
		else
		{
			return 0;
		}
	}
}

std::string Client::queryPosition(int cam_address, float north_bearing_offset)
{

	std::vector<char> msg = std::vector<char>(7);

	std::string data;
	std::uint8_t data0;
	std::uint8_t data1;
	std::string response = "";
	std::stringstream stream;

	try
	{
		if (control_interface == 1 || control_interface == 2) {
			// display the current camera pan level.
			data = queryPan(msg);
			// if data is received
			if (data != "")
			{
				data = data.substr(4, 2); // filters data bytes received
				// cast bytes to unsigned int
				data0 = data[0];
				data1 = data[1];

				float pan_raw = float((data0 << 8) | data1) / 100; // combines bytes via bitwise shift and converts to degrees

				// This filters out interference from received OK messages - TODO should be able to optimize and remove
				while (data0 == 0xff && data1 == 0x01)
				{
					// while (pan_raw == 652.81) {
					// cout << "MITIGATION active" << endl;
					//  display the current camera pan level.
					data = queryPan(msg);
					// if data is received
					if (data != "")
					{
						data = data.substr(4, 2); // filters data bytes received
						// cast bytes to unsigned int
						data0 = data[0];
						data1 = data[1];

						pan_raw = float((data0 << 8) | data1) / 100; // combines bytes via bitwise shift and converts to degrees
					}
					else
					{
						response += "Pan: N/A ";
					}
				}

				float pan2stream = pan_raw - north_bearing_offset;
				while (pan2stream < 0)
					pan2stream += 360.0f;
				while (pan2stream > 360.0f)
					pan2stream -= 360.0f;
				// rounds to two decimal places
				stream << std::fixed << std::setprecision(2) << pan2stream;
				std::string pan = stream.str();
				response += "Pan: " + pan + " deg ";
			}
			else
			{
				response += "Pan: N/A ";
			}

			// display the current camera tilt level.
			data = queryTilt(msg);
			// if data is received
			if (data != "")
			{
				data = data.substr(4, 2); // filters data bytes received
				// cast bytes to unsigned int
				data0 = data[0];
				data1 = data[1];

				float tilt_raw = float((data0 << 8) | data1) / 100; // combines bytes via bitwise shift

				// This filters out interference from received OK messages - TODO should be able to optimize and remove
				while (data0 == 0xff && data1 == 0x01)
				{
					// while (tilt == -329.xx) {
					// cout << "MITIGATION active" << endl;
					//  display the current camera pan level.
					data = queryTilt(msg);
					// if data is received
					if (data != "")
					{
						data = data.substr(4, 2); // filters data bytes received
						// cast bytes to unsigned int
						data0 = data[0];
						data1 = data[1];

						tilt_raw = float((data0 << 8) | data1) / 100; // combines bytes via bitwise shift and converts to degrees
					}
					else
					{
						response += "Pan: N/A ";
					}
				}

				tilt_raw = (tilt_raw < 180 ? tilt_raw * -1 : 360 - tilt_raw); // converts to tilt level

				// rounds to two decimal places
				stream.str(std::string());
				stream << std::fixed << std::setprecision(2) << tilt_raw;
				std::string tilt = stream.str();
				response += "| Tilt: " + tilt + " deg ";
			}
			else
			{
				response += "| Tilt: N/A ";
			}

			// display the current camera zoom / FoV level.
			data = queryZoom(msg);
			// if data is received
			if (data != "")
			{
				data = data.substr(4, 2); // filters data bytes received
										// cast bytes to unsigned int
				data0 = data[0];
				data1 = data[1];

				int zoom_raw = int((data0 << 8) | data1); // combines bytes via bitwise shift
				// This filters out interference from received OK messages - TODO should be able to optimize and remove
				while (data0 == 0xff && data1 == 0x01)
				{
					// while (zoom == xx) {
					// cout << "MITIGATION active" << endl;
					//  display the current camera pan level.
					data = queryZoom(msg);
					// if data is received
					if (data != "")
					{
						data = data.substr(4, 2); // filters data bytes received
						// cast bytes to unsigned int
						data0 = data[0];
						data1 = data[1];

						zoom_raw = int((data0 << 8) | data1) / 100; // combines bytes via bitwise shift and converts to degrees
					}
					else
					{
						response += "Pan: N/A ";
					}
				}

				// simple display
				stream.str(std::string());
				stream << zoom_raw;
				std::string zoom = stream.str();
				// response += "| Zoom: " + zoom + ""; // ZOOM not visualised for now

				// display the current camera FoV levels.
				// get FoV
				float fov_raw = zoomToFov(zoom_raw, m_lut);
				// rounds to two decimal places
				stream.str(std::string());
				stream << std::fixed << std::setprecision(2) << fov_raw;
				std::string fov = stream.str();
				if (current_fov == 0)
					current_fov = std::stoi(fov);
				response += "| FoV: " + fov + " deg";
				//    fov = get_fov(zoom, load_lut(lut_address))
				//    print("fov: ", fov)
			}
			else
			{
				response += "| FoV: N/A";
			}
			if (m_verbose_debug) {
				cout << "Query response: " << response << endl;
			}
			return response;
		}
		else if (control_interface == 3) {}
	}
	catch (const boost::system::system_error &ex)
	{
		if (m_verbose_debug) {
			cout << "Boost sys error - could not complete query" << endl;
		}
		m_connected_status = false;
		return "";
	}
	catch (...) {
		m_connected_status = false;
		return "";
	}

}

std::string Client::queryPosition(CamPosition_t &camPosition, int cam_address, float north_bearing_offset)
{
	std::vector<char> msg = std::vector<char>(7);
	std::string response_tcp;
	std::string response_string = "";
	std::string response = "";
	std::stringstream stream;

	try
	{
		if (control_interface == 1 || control_interface == 2) {
			response_tcp = queryPan(msg, cam_address);
			response_tcp += queryTilt(msg, cam_address);
			response_tcp += queryZoom(msg, cam_address);
			std::vector<boost::uint8_t> data_v;
			for (int i = 0; i < response_tcp.size(); ++i) {
				data_v.push_back((boost::uint8_t) * (response_tcp.c_str() + i));
			}

			for (boost::uint8_t res : data_v)
			{
				switch (ps)
				{
				case ParsingState::WaitingHeader:
					if (res == RESPONSE_HEADER)
					{
						data = 0;
						data1 = 0;
						data2 = 0;
						checksum = 0;
						ps = ParsingState::WaitingAdressCam;
						ptz = DataPanTiltZoom::Unknown;
					}
					break;

				case ParsingState::WaitingAdressCam:
					checksum += res;
					ps = ParsingState::WaitingResponseCode1;
					break;

				case ParsingState::WaitingResponseCode1:
					if (res == RESPONSE_OK_MSB || res == RESPONSE_QUERY_PAN_MSB || res == RESPONSE_QUERY_TILT_MSB || res == RESPONSE_QUERY_ZOOM_MSB)
						ps = ParsingState::WaitingResponseCode2;
					else
					{
						ps = ParsingState::WaitingHeader;
					}
					checksum += res;

					break;

				case ParsingState::WaitingResponseCode2:
					if (res == RESPONSE_OK_LSB)
					{
						ps = ParsingState::WaitingHeader;
					}
					else if (res == RESPONSE_QUERY_PAN_LSB)
					{
						ps = ParsingState::WaitingData1;
						ptz = DataPanTiltZoom::Pan;
					}
					else if (res == RESPONSE_QUERY_TILT_LSB)
					{
						ps = ParsingState::WaitingData1;
						ptz = DataPanTiltZoom::Tilt;
					}
					else if (res == RESPONSE_QUERY_ZOOM_LSB)
					{
						ps = ParsingState::WaitingData1;
						ptz = DataPanTiltZoom::Zoom;
					}
					else
					{
						ps = ParsingState::WaitingHeader;
					}
					checksum += res;
					break;

				case ParsingState::WaitingData1:
					data1 = res;
					ps = ParsingState::WaitingData2;
					checksum += res;
					break;

				case ParsingState::WaitingData2:
					data2 = res;
					ps = ParsingState::WaitingChecksum;
					checksum += res;
					break;

				case ParsingState::WaitingChecksum:
					if (res == (checksum % 256))
					{
						uint16_t data = ((data1 << 8) | data2);

						if (ptz == DataPanTiltZoom::Pan)
						{
							camPosition.pan = static_cast<float>(data) / 100.0f;
							while (camPosition.pan > 180.0)
								camPosition.pan -= 360.0f;
							camPosition.pan_nan = false;
						}
						else if (ptz == DataPanTiltZoom::Tilt)
						{
							float tilt_raw = static_cast<float>(data) / 100.0f;
							camPosition.tilt = (tilt_raw < 180 ? tilt_raw * -1 : 360 - tilt_raw); // converts to tilt level
							camPosition.tilt_nan = false;
						}

						else if (ptz == DataPanTiltZoom::Zoom)
						{
							camPosition.fov = zoomToFov(data, m_lut);
							stream.str(std::string());
							stream << std::fixed << std::setprecision(2) << camPosition.fov;
							std::string fov = stream.str();
							if (current_fov == 0 || current_fov == 2)
								current_fov = std::stoi(fov);
							camPosition.fov_nan = false;
						}

						ps = ParsingState::WaitingHeader;
					}
					else
					{
						ps = ParsingState::WaitingHeader;
					}
					break;

				default:
					ps = ParsingState::WaitingHeader;
					break;
				}
			}
			// std::vector<uint8_t> data_v{reinterpret_cast<uint8_t>(data.c_str())};
			// response_string += std::to_string(data_v.size());
			// camPosition.pan = 0;
			// camPosition.tilt = 0;
			// camPosition.fov = 0;
		}
		else if (control_interface == 3) {
			response = queryPan(msg, cam_address);
			response += queryTilt(msg, cam_address);
			response += queryZoom(msg, cam_address);

			for (int i=0; i<response.length(); ++i)
			{
				switch (ps3)
				{
				case ParsingState3::WaitingStartOfFrame:
					if (response[i] == '[')
					{
						ros = "";
						cmd = "";
						vdata.clear();
						ps3 = ParsingState3::WaitingROS;
						ptz = DataPanTiltZoom::Unknown;
					}
					break;

				case ParsingState3::WaitingROS:
					if (response[i] == ':')
						if (ros == "ROS")
							ps3 = ParsingState3::WaitingCommand;
						else
							ps3 = ParsingState3::WaitingStartOfFrame;
					else
						ros += response[i];
					break;

				case ParsingState3::WaitingCommand:
					if (response[i] == ':')
						if (cmd == "PT.AZ.AB")
						{
							ptz = DataPanTiltZoom::Pan;
							ps3 = ParsingState3::WaitingData;
						}
						else if (cmd == "PT.EL.AB")
						{
							ptz = DataPanTiltZoom::Tilt;
							ps3 = ParsingState3::WaitingData;
						}
						else if (cmd == "LR.ZM.AB")
						{
							ptz = DataPanTiltZoom::Zoom;
							ps3 = ParsingState3::WaitingData;
						}
						else
							ps3 = ParsingState3::WaitingStartOfFrame;
						
					else
						cmd += response[i];

					break;

				case ParsingState3::WaitingData:
					if (response[i] == ':')
					{
						vdata.push_back(std::stof(_data));
						_data = "";
						if (vdata.size() > 0)
						{
							// ps3 = ParsingState3::WaitingChecksum;
							ps3 = ParsingState3::WaitingStartOfFrame;
							if (ptz == DataPanTiltZoom::Unknown)
								ps3 = ParsingState3::WaitingStartOfFrame;
							else if (ptz == DataPanTiltZoom::Pan)
							{
								camPosition.pan_nan = false;
								camPosition.pan = vdata[0];
							}
							else if (ptz == DataPanTiltZoom::Tilt)
							{
								camPosition.tilt_nan = false;
								camPosition.tilt = vdata[0];
							}
							else if (ptz == DataPanTiltZoom::Zoom)
							{
								// camPosition.zoom_nan = false;
								// camPosition.zoom = vdata[0];
								camPosition.fov = zoomToFov((int) vdata[0], m_lut);
								// camPosition.fov = zoomToFov((data), m_lut);
								stream.str(std::string());
								stream << std::fixed << std::setprecision(2) << camPosition.fov;
								std::string fov = stream.str();
								if (current_fov == 0 || current_fov == 2)
									current_fov = std::stoi(fov);
								camPosition.fov_nan = false;
							}
							else
								ps3 = ParsingState3::WaitingStartOfFrame;
								
						}
						else
							ps3 = ParsingState3::WaitingStartOfFrame;
					}
					else if (response[i] == ',')
					{
						vdata.push_back(std::stof(_data));
						_data = "";
					}
					else
						_data += response[i];
					break;
				default:
					ps3 = ParsingState3::WaitingStartOfFrame;
					break;
				}
			}
		
		}
	}
	catch (const boost::system::system_error &ex) {
		if (m_verbose_debug)
		{
			cout << "Boost sys error - could not complete query" << endl;
		}
		m_connected_status = false;
		return "";
	}
	catch (...) {
		m_connected_status = false;
		return "";
	}

	return camPosition.str(north_bearing_offset);
}

void Client::panLeft(std::vector<char>& msg, int pan_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving LEFT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x04 + get_zoom_msg(is_zooming);
		msg[4] = pan_speed % 256;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:-" << std::fixed << std::setprecision(0) << (round(abs(pan_speed) * 100)) / 100.0 << ",0.00:**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		float roll_rate = 0.5;
		// chia 20 tam thoi.... fix sau
		roll_rate = pan_speed  / MAVLINK_CAM_CONTROL_RATE;
		roll_rate = (roll_rate > 1) ?  1 :  roll_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, roll_rate, 0, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::tiltPanUpLeft(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving UPLEFT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x0c + get_zoom_msg(is_zooming);
		msg[4] = pan_speed % 256;
		msg[5] = tilt_speed % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:-" << std::fixed << std::setprecision(0) << (round(abs(pan_speed) * 100)) / 100.0 << "," << (round(abs(tilt_speed) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		// chia 20 tam thoi.... fix sau
		float pitch_rate = tilt_speed  / MAVLINK_CAM_CONTROL_RATE;
		pitch_rate = (pitch_rate < 1) ?  1 :  pitch_rate;
		float roll_rate  =  pan_speed  / MAVLINK_CAM_CONTROL_RATE;
		roll_rate = (roll_rate < 1) ?  1 :  roll_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, roll_rate, pitch_rate, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::tiltUp(std::vector<char>& msg, int tilt_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving RIGHT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x08 + get_zoom_msg(is_zooming);
		msg[4] = 0x00;
		msg[5] = tilt_speed % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:0.00," << std::fixed << std::setprecision(0) << (round(abs(tilt_speed) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		float pitch_rate = 0.5;
		// chia 20 tam thoi.... fix sau
		pitch_rate = tilt_speed  / MAVLINK_CAM_CONTROL_RATE;
		pitch_rate = (pitch_rate < 1) ?  1 :  pitch_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, 0, pitch_rate, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::tiltPanUpRight(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving UPRIGHT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x0a + get_zoom_msg(is_zooming);
		msg[4] = pan_speed % 256;
		msg[5] = tilt_speed % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:" << std::fixed << std::setprecision(0) << (round(abs(pan_speed) * 100)) / 100.0 << "," << (round(abs(tilt_speed) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		// chia 20 tam thoi.... fix sau
		float pitch_rate = tilt_speed  / MAVLINK_CAM_CONTROL_RATE;
		pitch_rate = (pitch_rate < 1) ?  1 :  pitch_rate;
		float roll_rate  =  - pan_speed  / MAVLINK_CAM_CONTROL_RATE;
		roll_rate = (roll_rate < -1) ?  -1 :  roll_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, roll_rate, pitch_rate, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::panRight(std::vector<char>& msg, int pan_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving RIGHT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x02 + get_zoom_msg(is_zooming);
		msg[4] = pan_speed % 256;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:" << std::fixed << std::setprecision(0) << (round(abs(pan_speed) * 100)) / 100.0 << ",0.00:**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		float roll_rate = 0.5;
		// chia 20 tam thoi.... fix sau
		roll_rate = - pan_speed  / MAVLINK_CAM_CONTROL_RATE;
		roll_rate = (roll_rate < -1) ?  -1 :  roll_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, roll_rate, 0, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::tiltPanDownLeft(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving DOWNLEFT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x14 + get_zoom_msg(is_zooming);
		msg[4] = pan_speed % 256;
		msg[5] = tilt_speed % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:-" << std::fixed << std::setprecision(0) << (round(abs(pan_speed) * 100)) / 100.0 << ",-" << (round(abs(tilt_speed) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		// chia 20 tam thoi.... fix sau
		float pitch_rate = - tilt_speed  / MAVLINK_CAM_CONTROL_RATE;
		pitch_rate = (pitch_rate < -1) ?  -1 :  pitch_rate;
		float roll_rate  =  pan_speed  / MAVLINK_CAM_CONTROL_RATE;
		roll_rate = (roll_rate < 1) ?  1 :  roll_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, roll_rate, pitch_rate, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::tiltDown(std::vector<char>& msg, int tilt_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving DOWN" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x10 + get_zoom_msg(is_zooming);
		msg[4] = 0x00;
		msg[5] = tilt_speed % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:0.00,-" << std::fixed << std::setprecision(0) << (round(abs(tilt_speed) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		float pitch_rate = 0.5;
		// chia 20 tam thoi.... fix sau
		pitch_rate = - tilt_speed  / MAVLINK_CAM_CONTROL_RATE;
		pitch_rate = (pitch_rate < -1) ?  -1 :  pitch_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, 0, pitch_rate, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::tiltPanDownRight(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	cout << "Moving DOWNRIGHT" << "  " << control_interface << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x12 + get_zoom_msg(is_zooming);
		msg[4] = pan_speed % 256;
		msg[5] = tilt_speed % 256;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		oss << "[COS:PT.RT:" << std::fixed << std::setprecision(0) << (round(abs(pan_speed) * 100)) / 100.0 << ",-" << (round(abs(tilt_speed) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		// chia 20 tam thoi.... fix sau
		float pitch_rate = - tilt_speed  / MAVLINK_CAM_CONTROL_RATE;
		pitch_rate = (pitch_rate < -1) ?  -1 :  pitch_rate;
		float roll_rate  =  - pan_speed  / MAVLINK_CAM_CONTROL_RATE;
		roll_rate = (roll_rate < -1) ?  -1 :  roll_rate;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, roll_rate, pitch_rate, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}

}

void Client::zoomIn(std::vector<char>& msg, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving ZOOMIN" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x20;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::ostringstream oss;
		float zoom_speed_abs = 1.0;
		oss << "[COS:LR.ZM.RT:" << std::fixed << std::setprecision(0) << (round(abs(50) * 100)) / 100.0 << ":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		float zoom_in = 1;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, 0, 0, zoom_in , 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::zoomOut(std::vector<char>& msg, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// cout << "Moving ZOOMOUT" << endl;
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x40;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		float zoom_speed_abs = 1.0;
		std::ostringstream oss;
		oss << "[COS:LR.ZM.RT:-" << std::fixed << std::setprecision(0) << (round(abs(50) * 100)) / 100.0 <<":**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		float zoom_out = 2;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, 0, 0, zoom_out , 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::stopCamera(std::vector<char>& msg, int cam_address)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x00;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		std::string msg_("[COS:PT.RT:0.00,0.00:**]");
		write_serial_nores(msg_);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;
		data_send = MavlinkDigiCamMsg(Set_Gimbal, 0, 0, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}
void Client::PanTiltAbsolute(float pan, float tilt) {
	if (control_interface == 4) { 
		std::vector<uint8_t> data_send;
		data_send = MavlinkDigiCamMsg(Set_System_Mode, LocalPosition, pan, tilt, 0, 0, 0);
		
		sendUDPnores(data_send);
	}
}

// Note: input is in hundredths of a degree between 0 (pointing directly behind default) and 17999 (90deg pointing up)
void Client::panAbsolute(std::vector<char>& msg, int pan, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		pan %= 36000;
		char lsb = pan % 256;
		char msb = (pan >> 8) % 256;

		// cout << "Moving PAN absolute to: " << pan << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x4B;
		msg[4] = msb;
		msg[5] = lsb;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		float pan_f = ((float) pan) / 100;
		while(pan_f < 0)
			pan_f += 360;
		while(pan_f >= 360)
			pan_f -= 360;
		std::ostringstream oss;
		oss << "[COS:PT.AZ.AB:" << std::fixed << std::setprecision(2) << (round(pan_f * 100)) / 100.0 << ",90.0:**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
}

// Note: input is in hundredths of a degree between -9000 (90deg pointing down) and 9000 (90deg pointing up)
void Client::tiltAbsolute(std::vector<char>& msg, int tilt, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		tilt %= 36000;
		// Converting tilt input to hex using bitshift
		char lsb = tilt % 256;
		char msb = (tilt >> 8) % 256;

		// cout << "Moving TILT absolute to: " << tilt << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x4D;
		msg[4] = msb;
		msg[5] = lsb;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3) {
		tilt *= -1;
		tilt %= 36000;
		float tilt_f;
		if (tilt < 9000)
			tilt_f = (float) tilt / 100.0;
		else if (tilt > 27000)
			tilt_f = (float) (tilt - 36000) / 100.0;
		if (tilt_f < -49.99) tilt_f = -50;
		if (tilt_f > 49.99) tilt_f = 50;
		std::ostringstream oss;
		oss << "[COS:PT.EL.AB:" << std::fixed << std::setprecision(2) << (round(tilt_f * 100)) / 100.0 << ",90.0:**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
}

void Client::autoFocusOn(std::vector<char>& msg, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x2b;
		msg[4] = 0x00;
		msg[5] = 0x00;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;

		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
}

void Client::autoFocusOff(std::vector<char>& msg, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x2b;
		msg[4] = 0x00;
		msg[5] = 0x01;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;

		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
}

void Client::autoFocus(std::vector<char>& msg, int cam_address, int time_)
{
	autoFocusOn(msg);

	std::this_thread::sleep_for(std::chrono::seconds(time_));

	autoFocusOff(msg);
}


//
//// Note: input is in hundredths of a degree between -18000 (pointing directly behind default) and 1799 (pointing almost behind)
// void Client::panAbsoluteDeg(std::vector<char>& msg, int pan, int cam_address)
//{
//	std::cout << "[This is pan to move] " << pan << std::endl;
//	if (-18000 <= pan < 18000) {
//
//		pan < 0 ? pan = -(36000 - pan) : pan = pan; // Transformation to hundredths of degrees for command
//		panAbsoluteRaw(msg, pan, cam_address);
//	}
//	else
//	{
//		throw std::invalid_argument("Invalid pan degrees input: " + pan);
//	}
// }
//
//// Note: input is in raw hundredths of a degree starting from 0 up
// void Client::panAbsoluteRaw(std::vector<char>& msg, int pan, int cam_address)
//{
//	if (0 <= pan) {
//
//		// Converting pan input to hex using bitshift
//		char lsb = pan % 256;
//		char msb = (pan >> 8) % 256;
//
//		cout << "Moving PAN absolute to: " << pan << endl;
//		msg[0] = 0xff;
//		msg[1] = cam_address % 256;
//		msg[2] = 0x00;
//		msg[3] = 0x4B;
//		msg[4] = msb;
//		msg[5] = lsb;
//		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
//		sendTcp(msg, 4);
//	}
//	else
//	{
//		throw std::invalid_argument("Invalid raw pan input: " + pan);
//	}
// }
//
//// Note: input is in hundredths of a degree between -9000 (90deg pointing down) and 9000 (90deg pointing up)
// void Client::tiltAbsoluteDeg(std::vector<char>& msg, int tilt, int cam_address)
//{
//	std::cout << "[This is tilt to move] " << tilt << std::endl;
//	if (-9000 <= tilt <= 9000) {
//		tilt < 0 ? tilt = -(tilt) : tilt = (36000 - tilt); // Transformation to hundredths of degrees for command
//		tiltAbsoluteRaw(msg, tilt, cam_address);
//	}
//	else
//	{
//		throw std::invalid_argument("Invalid tilt degrees input: " + tilt);
//	}
// }
//
//// Note: input is in raw hundredths of a degree starting from 0 up
// void Client::tiltAbsoluteRaw(std::vector<char>& msg, int tilt, int cam_address)
//{
//	if (0 <= tilt) {
//		// Converting tilt input to hex using bitshift
//		char lsb = tilt % 256;
//		char msb = (tilt >> 8) % 256;
//
//		cout << "Moving TILT absolute to: " << tilt << endl;
//		msg[0] = 0xff;
//		msg[1] = cam_address % 256;
//		msg[2] = 0x00;
//		msg[3] = 0x4D;
//		msg[4] = msb;
//		msg[5] = lsb;
//		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
//		sendTcp(msg, 4);
//	}
//	else
//	{
//		throw std::invalid_argument("Invalid raw tilt input: " + tilt);
//	}
// }

void Client::zoomAbsolute(std::vector<char>& msg, int zoom, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		// Converting tilt input to hex using bitshift
		char lsb = zoom % 256;
		char msb = (zoom >> 8) % 256;

		// cout << "Moving ZOOM absolute to: " << zoom << endl;
		msg[0] = 0xff;
		msg[1] = cam_address % 256;
		msg[2] = 0x00;
		msg[3] = 0x4F;
		msg[4] = msb;
		msg[5] = lsb;
		msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
		// std::cout << "[ LOG ] zoomAbsolute sent: ";
		// for (int i = 0; i < 7; ++i)
		// {
		// 	std::cout << "\\x" << std::setfill('0') << std::hex << std::setw(2) << static_cast<unsigned int>(msg[i]);
		// }
		// std::cout << std::endl;
		// sendTcp(msg, 4);
		if (control_interface == 1) sendTcpNoRes(msg);
		else if (control_interface == 2) write_serial_nores(msg);
	}
	else if (control_interface == 3)
	{
		if (zoom < 0) zoom = 0;
		if (zoom > 100) zoom = 99.99;
		std::ostringstream oss;
		oss << "[COS:LR.ZM.AB:" << std::fixed << std::setprecision(2) << (round((float) zoom * 100)) / 100.0 << ",90.0:**]";
		std::string msg_(oss.str());
		write_serial_nores(msg_);
	}
	
}

void Client::fovAbsolute(std::vector<char>& msg, float fov, int cam_address)
{
	// Calculate zoom level
	int zoom = fovToZoom(fov, m_lut);
	if (control_interface == 1 || control_interface == 2) {
		// Use absolute zoom
		zoomAbsolute(msg, zoom, cam_address);
	}
	else if (control_interface == 3) {
		zoomAbsolute(msg, zoom, cam_address);
	}
	else if (control_interface == 4) {
		std::vector<uint8_t> data_send;

		data_send = MavlinkDigiCamMsg(Set_FOV, fov, 0, 0, 0, 0, 0);
		sendUDPnores(data_send);
	}
}

void Client::changeZoomByFoV(std::vector<char>& msg, float fov_change, int cam_address)
{
	if (control_interface == 1 || control_interface == 2) {
		// Calculate new FoV based on the current zoom and Fov
		float new_fov = zoomToFov(getZoom(msg, cam_address), m_lut) + fov_change;

		// Use absolute FoV command to move towards new FoV
		fovAbsolute(msg, new_fov, cam_address);
	}
}

std::vector<float> Client::moveAbs(int x_target, int y_target, int x_current, int y_current, CamPosition_t camPosition, int camera_address) {
	std::vector<float> newPanTilt;

	if (control_interface == 1 || control_interface == 2) {
		std::vector<char> msg = std::vector<char>(7);

		//std:string camera_status_tmp = cam_client_->queryPosition(camPosition);
					// std::cout << "EVENT_LBUTTONDOUBLEBLCLK" << std::endl;
		float deltaHorizontal = atan(abs(x_target - static_cast<int>(x_current)) * tan(camPosition.fov * M_PI / 360.0) / x_current);
		//std::cout << "[ camPosition.pan ]: " << -1 * camPosition.pan << std::endl;
		float newPan = camPosition.pan + deltaHorizontal * 180.0 * M_1_PI * ((x_target - x_current) < 0 ? -1 : 1);
		//std::cout << "[ new pan ]: " << newPan << std::endl;
		float fov_vertical_2 = atan(y_current * tan(camPosition.fov * M_PI / 360.0) / x_current);
		//std::cout << "[ fov_vertical_2 ]: " << fov_vertical_2 * M_1_PI * 180 << std::endl;
		float deltaVertical = atan(abs(y_target - static_cast<int>(y_current)) * tan(fov_vertical_2) / y_current);
		//std::cout << "[ deltaVertical ]:  " << deltaVertical * M_1_PI * 180 << std::endl;
		float newTilt = -1 * camPosition.tilt + deltaVertical * M_1_PI * 180.0 * ((y_target - y_current) < 0 ? -1 : 1) + 360.0;
		//std::cout << "[ camPosition.tilt ]: " << -1 * camPosition.tilt << std::endl;
		//std::cout << "[ newTilt ]: " << newTilt<<"\t" << static_cast<int>(newTilt * 100) << std::endl;

		newPanTilt.push_back(newPan);
		newPanTilt.push_back(newTilt);

		//if (manager_status == "ONLINE") {
		//newPan += config.director.north_bearing_offset;
		while (newPan < 0)
			newPan += 360.0f;
		while (newPan > 360.0f)
			newPan -= 360.0f;

		panAbsolute(msg, static_cast<int>(newPan * 100), camera_address);
		tiltAbsolute(msg, static_cast<int>(newTilt * 100), camera_address);

		while (newTilt < -180.0f)
			newTilt += 360.0f;
		while (newTilt > 180.0f)
			newTilt -= 360.0f;

		newPanTilt.push_back(newPan);
		newPanTilt.push_back(-1 * newTilt);
	}
	return newPanTilt;
}

void Client::test(int speed) {
	std::vector<char> msg = std::vector<char>(7);
	panRight(msg, speed, 1);
}

void Client::moveTowardsCenter(int x_target, int y_target, int x_current, int y_current, std::string& camera_mode, float& x_speed_, float& y_speed_, int tolerance_radius, float max_size_vector, int max_speed, int bouder_for_extra, float random_number_for_extra, int magic_point, int camera_address, bool loging_camera)
{
	// std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
	// auto duration = now.time_since_epoch();
	// auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

	// std::cout << "1: " << millis << "\n";
	std::vector<char> msg = std::vector<char>(7);
	float rate = 1;

	// Get x, y offsets:
	int x_offset = x_target - x_current; // positive = move right; negative = move left
	int y_offset = y_target - y_current; // positive = move down; negative = move up
	
	int extra_speed_x = 0;
	int extra_speed_y = 0;
	bool x_higher = std::abs(x_offset) > std::abs(y_offset);
	if (x_offset != 0 && y_offset != 0) {
		rate = std::abs(x_offset) / std::abs(y_offset);
	}
	// std::cout << "bouder_for_extra: " << bouder_for_extra << " x_offset: " << x_offset << " bouder_for_extra: " << bouder_for_extra << " magic_point: " << magic_point << "\n";

	if (bouder_for_extra != 0 && std::abs(x_offset) <= bouder_for_extra && std::abs(x_offset) > magic_point)
	{
		extra_speed_x = random_number_for_extra / std::abs(x_offset);
		// std::cout << "extra_speed_x---\n";
	}

	if (bouder_for_extra != 0 && std::abs(y_offset) <= bouder_for_extra && std::abs(y_offset) > magic_point)
	{
		extra_speed_y = random_number_for_extra / std::abs(y_offset);
		// std::cout << "extra_speed_y---\n";
	}

	if ((abs(x_offset) / max_size_vector) <= extra_speed_x || (abs(y_offset) / max_size_vector) <= extra_speed_y) rate = 1;
	// Speed to be used
	float speed;

	// std::cout << "extra x: " << extra_speed_x << " extra y: " << extra_speed_y << "\n";

	if (m_verbose_debug)
	{
		cout << "x offset: " << x_offset << " | y offset: " << y_offset << endl;
	}

	// check if offset is within tolerance
	if (abs(x_offset) <= tolerance_radius && abs(y_offset) <= tolerance_radius)
	{
		// return stop command
		stopCamera(msg, camera_address);
		camera_mode = "Standby";

	} // if x offset is zero, move vertically
	else if (x_offset == 0)
	{
		if (y_offset > 0)
		{
			// get speed
			speed = y_offset / max_size_vector;
			speed = speed + extra_speed_y;
			speed = speed < max_speed ? speed : max_speed;
			// move down
			y_speed_ = -1 * speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "tilt : " << millis << "\n";*/
			tiltDown(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done tilt: " << millis << "\n";*/

		}
		else
		{
			// get speed
			speed = abs(y_offset) / max_size_vector;
			speed = speed + extra_speed_y;
			speed = speed < max_speed ? speed : max_speed;
			// move up
			y_speed_ = speed;
			/*	now = std::chrono::steady_clock::now();
				duration = now.time_since_epoch();
				millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
				std::cout << "tilt : " << millis << "\n";*/
			tiltUp(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done tilt: " << millis << "\n";*/

		}
	} // if y offset is zero, move horizontally
	else if (y_offset == 0)
	{

		if (x_offset > 0)
		{
			// get speed
			speed = x_offset / max_size_vector;
			speed = speed + extra_speed_x;
			speed = speed < max_speed ? speed : max_speed;
			// move right
			x_speed_ = speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan: " << millis << "\n";*/
			panRight(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan: " << millis << "\n";*/

		}
		else
		{
			// get speed
			speed = abs(x_offset) / max_size_vector;
			speed = speed + extra_speed_x;
			speed = speed < max_speed ? speed : max_speed;
			// move left
			x_speed_ = -1 * speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan: " << millis << "\n";*/
			panLeft(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan: " << millis << "\n";*/

		}
	} // else move diagonally
	else
	{
		int x_speed = (abs(x_offset) / max_size_vector);
		x_speed = x_speed + extra_speed_x;
		x_speed = x_speed < max_speed ? x_speed : max_speed;

		int y_speed = (abs(y_offset) / max_size_vector);
		y_speed = y_speed + extra_speed_y;

		y_speed = y_speed < max_speed ? y_speed : max_speed;

		if (x_higher && x_higher != 0) {
			y_speed = x_speed / rate;
			x_speed = std::round(x_speed);
			if (y_speed > 2 && y_speed + 3 <= max_speed) y_speed = y_speed + 3;
			else if (y_speed > 2 && y_speed + 3 > max_speed) y_speed = max_speed;
			y_speed = std::round(y_speed);

		}
		else if (!x_higher && y_speed != 0) {
			x_speed = y_speed * rate;
			x_speed = std::round(x_speed);
			if (y_speed > 2 && y_speed + 3 <= max_speed) y_speed = y_speed + 3;
			else if (y_speed > 2 && y_speed + 3 > max_speed) y_speed = max_speed;
			y_speed = std::round(y_speed);

		}

		if (x_offset > 0 && y_offset > 0)
		{
			// move right down
			if (x_speed == 0) x_speed = 1;
			if (y_speed == 0) y_speed = 1;

			x_speed_ = x_speed;
			y_speed_ = -1 * y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanDownRight(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/

		}
		else if (x_offset > 0 && y_offset <= 0)
		{
			// move right up
			if (x_speed == 0) x_speed = 1;
			if (y_speed == 0) y_speed = 1;

			x_speed_ = x_speed;
			y_speed_ = y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanUpRight(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/

		}
		else if (x_offset < 0 && y_offset > 0)
		{
			// move left down
			if (x_speed == 0) x_speed = 1;
			if (y_speed == 0) y_speed = 1;

			x_speed_ = -1 * x_speed;
			y_speed_ = -1 * y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanDownLeft(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/

		}
		else
		{
			// move left up
			if (x_speed == 0) x_speed = 1;
			if (y_speed == 0) y_speed = 1;

			x_speed_ = -1 * x_speed;
			y_speed_ = y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanUpLeft(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/
		}

	}
}

void Client::moveTowardsCenterImprove(int x_speed, int y_speed, int camera_address)
{
	std::vector<char> msg = std::vector<char>(7);
	if (x_speed == 0)
	{
		if (y_speed > 0)
			tiltDown(msg, std::abs(y_speed), camera_address);
		else
			tiltUp(msg, std::abs(y_speed), camera_address);
	} // if y offset is zero, move horizontally
	else if (y_speed == 0)
	{
		if (x_speed > 0)
			panRight(msg, std::abs(x_speed), camera_address);
		else
			panLeft(msg, std::abs(x_speed), camera_address);
	} // else move diagonally
	else
	{
		if (x_speed > 0 && y_speed > 0)
			tiltPanDownRight(msg, std::abs(x_speed), std::abs(y_speed), camera_address);
		else if (x_speed > 0 && y_speed <= 0)
			tiltPanUpRight(msg, std::abs(x_speed), std::abs(y_speed), camera_address);
		else if (x_speed < 0 && y_speed > 0)
			tiltPanDownLeft(msg, std::abs(x_speed), std::abs(y_speed), camera_address);
		else
			tiltPanUpLeft(msg, std::abs(x_speed), std::abs(y_speed), camera_address);
	}
}


void callback(
	const boost::system::error_code& error,
	std::size_t bytes_transferred,
	std::shared_ptr<boost::asio::ip::tcp::socket> socket,
	std::string str)
{
	if (error)
	{
		std::cout << error.message() << '\n';
	}
	else if (bytes_transferred == str.length())
	{
		std::cout << "Message is sent successfully!" << '\n';
	}
	else
	{
		socket->async_send(
			boost::asio::buffer(str.c_str() + bytes_transferred, str.length() - bytes_transferred),
			std::bind(callback, std::placeholders::_1, std::placeholders::_2, socket, str));
	}
}

std::string Client::sendTcp(const std::vector<char>& msg, size_t at_least)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	// request/message from client
	if (m_verbose_debug)
	{
		cout << "Msg to be sent: " << string(msg.begin(), msg.end()) << endl;
	}

	boost::system::error_code error;

	boost::asio::write(m_active_socket, boost::asio::buffer(msg), error);
	if (!error)
	{
		if (m_verbose_debug)
		{
			cout << "Client sent message!" << endl;
		}
	}
	else
	{
		if (m_verbose_debug)
		{
			cout << "send failed: " << error.message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	if (m_verbose_debug)
	{
		cout << "End of sending" << endl;
	}

	// boost::asio::steady_timer timeout_timer(m_io_service, std::chrono::milliseconds(200));
	// timeout_timer.async_wait([&]() { m_active_socket.cancel(); });

	// getting response from server
	boost::system::error_code read_error;

	// boost::asio::read(m_active_socket, receive_buffer, boost::asio::transfer_at_least(at_least), read_error);

	boost::optional<boost::system::error_code> timer_result;
	boost::asio::deadline_timer timer(m_io_service);
	timer.expires_from_now(boost::posix_time::seconds(3));
	timer.async_wait([&timer_result](const boost::system::error_code& error)
		{ timer_result.reset(error); });

	boost::asio::streambuf receive_buffer;
	boost::optional<boost::system::error_code> read_result;
	boost::asio::async_read(m_active_socket, receive_buffer, boost::asio::transfer_at_least(at_least), [&read_result](const boost::system::error_code& error, size_t)
		{ read_result.reset(error); });

	m_io_service.reset();
	while (m_io_service.run_one())
	{
		if (read_result)
		{
			timer.cancel();
		}
		else if (timer_result)
		{
			cout << "Receive timeout" << endl;
			m_connected_status = false;
			m_active_socket.cancel();
			return "";
		}
	}

	if (m_verbose_debug)
	{
		cout << "Receiving done" << endl;
	}
	if (*read_result && *read_result != boost::asio::error::eof)
	{
		if (m_verbose_debug)
		{
			cout << "Receive failed: " << read_result->message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	else
	{
		const char* data_c = buffer_cast<const char*>(receive_buffer.data());
		/*if (receive_buffer.size()==4 || receive_buffer.size() !=7)
		{
			std::cout << "[ LOG ] Data received: ";
			for (int i = 0; i < receive_buffer.size(); ++i)
			{
				std::cout << "\\x" << std::setfill('0') << std::hex << std::setw(2) << static_cast<unsigned int>(*(data_c + i));
			}
			std::cout << std::endl;
		}*/
		// for (int i = 0; i < receive_buffer.size(); ++i)
		////for (int i = receive_buffer.size() == 11 ? 4 : 0; i < receive_buffer.size(); ++i)
		//{
		//	printf(" %x", *(data_c + i));
		//}
		// string data((receive_buffer.size() <= 7) ? data_c : (data_c + receive_buffer.size() - 7), receive_buffer.size());
		string data(data_c, receive_buffer.size());
		/*if (receive_buffer.size()==4 || receive_buffer.size() !=7)
			cout << "Received:" << data << ", len: " << std::dec << data.length() << endl;*/
		m_connected_status = true;
		// cout << "Returning" << endl;
		return data;
	}
};

std::string Client::sendTcpNoRes(const std::vector<char>& msg)
{
	// std::lock_guard<std::mutex> lock(m_mutex);
	// request/message from client
	if (m_verbose_debug)
	{
		cout << "Msg to be sent: " << string(msg.begin(), msg.end()) << endl;
	}

	boost::system::error_code error;

	boost::asio::write(m_active_socket, boost::asio::buffer(msg), error);
	if (!error)
	{
		if (m_verbose_debug)
		{
			cout << "Client sent message!" << endl;
		}
	}
	else
	{
		if (m_verbose_debug)
		{
			cout << "send failed: " << error.message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	if (m_verbose_debug)
	{
		cout << "End of sending" << endl;
	}

	return "OK";
};

std::string Client::sendTcp(const std::string& msg, size_t at_least)
{
	sendTcp(msg, 1, m_ip_address, m_target_port, at_least);
};

std::string Client::sendTcpNoRes(const std::string& msg)
{
	sendTcpNoRes(msg, 1, m_ip_address, m_target_port);
};

int sendTcpAsync(const std::string& msg, int timeout, const std::string& ip, int port)
{
	try
	{
		boost::asio::io_context io_context;

		boost::asio::ip::tcp::endpoint endpoint{
			boost::asio::ip::make_address(ip),
			3303 };

		std::shared_ptr<boost::asio::ip::tcp::socket> socket{ new boost::asio::ip::tcp::socket{io_context} };
		socket->connect(endpoint);

		std::cout << "Connect to " << endpoint << " successfully!\n";

		std::string str{ "\xff\x01\x00\x08\x00\x2a\x33" };
		socket->async_send(
			boost::asio::buffer(str),
			std::bind(callback, std::placeholders::_1, std::placeholders::_2, socket, str));
		socket->get_executor().context();
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << '\n';
		return -1;
	}
}

std::string Client::sendTcp(const std::string& msg, int timeout, const std::string& ip, int port, size_t at_least)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (m_verbose_debug)
	{
		cout << "Sending message: " << msg << endl;
	}

	// request/message from client
	if (m_verbose_debug)
	{
		cout << "Msg to be sent: " << msg << endl;
	}

	boost::system::error_code error;

	boost::asio::write(m_active_socket, boost::asio::buffer(msg), error);
	if (!error)
	{
		if (m_verbose_debug)
		{
			cout << "Client sent message!" << endl;
		}
	}
	else
	{
		if (m_verbose_debug)
		{
			cout << "send failed: " << error.message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	if (m_verbose_debug)
	{
		cout << "End of sending" << endl;
	}

	// getting response from server
	boost::asio::streambuf receive_buffer;
	if (m_verbose_debug)
	{
		cout << "buffer created" << endl;
	}
	try
	{
		boost::asio::read(m_active_socket, receive_buffer, boost::asio::transfer_at_least(at_least), error);
	}
	catch (std::exception& e)
	{
		if (m_verbose_debug)
		{
			cout << "Exception caught when reading" << endl;
		}
		m_connected_status = false;
		return "";
	}
	if (m_verbose_debug)
	{
		cout << "Receiving done" << endl;
	}
	if (error && error != boost::asio::error::eof)
	{
		if (m_verbose_debug)
		{
			cout << "Receive failed: " << error.message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	else
	{
		const char* data_c = buffer_cast<const char*>(receive_buffer.data());
		if (receive_buffer.size() == 4 || receive_buffer.size() != 7)
		{
			std::cout << "[ LOG ] Data received: ";
			for (int i = 0; i < receive_buffer.size(); ++i)
			{
				std::cout << "\\x" << std::setfill('0') << std::hex << std::setw(2) << static_cast<unsigned int>(*(data_c + i));
			}
			std::cout << std::endl;
		}
		int index = parse(data_c, receive_buffer.size());
		string data(data_c + index, receive_buffer.size() != 4 ? receive_buffer.size() : 7);
		if (receive_buffer.size() == 4 || receive_buffer.size() != 7)
			cout << "Received:" << data << ", len: " << std::dec << data.length() << endl;
		m_connected_status = true;
		// cout << "Returning" << endl;
		return data;
	}
}

std::string Client::sendTcpNoRes(const std::string& msg, int timeout, const std::string& ip, int port)
{
	// std::lock_guard<std::mutex> lock(m_mutex);
	if (m_verbose_debug)
	{
		if (m_verbose_debug)
		{
			cout << "Sending message (no response expected): " << msg << endl;
		}
	}
	// request/message from client
	if (m_verbose_debug)
	{
		cout << "Msg to be sent: " << msg << endl;
	}

	boost::system::error_code error;

	boost::asio::write(m_active_socket, boost::asio::buffer(msg), error);
	if (!error)
	{
		if (m_verbose_debug)
		{
			cout << "Client sent message!" << endl;
		}
	}
	else
	{
		if (m_verbose_debug)
		{
			cout << "send failed: " << error.message() << endl;
		}
		m_connected_status = false;
		return "";
	}
	if (m_verbose_debug)
	{
		cout << "End of sending" << endl;
	}

	return "OK";
};

void Client::setSerial(std::string serial_name_, int baud_rate_) {
	serial_name = serial_name_;
	baud_rate = baud_rate_;
}

std::vector<std::vector<float>> Client::loadLut(std::string filename)
{
	// Initialise variables
	std::vector<float> zoom;
	std::vector<float> fov;
	std::vector<std::vector<float>> lut;

	// Two LUT presets below not requiring file
	if (filename == "real" || filename == "Axis")
	{
		zoom.push_back(5000);
		fov.push_back(2.00);

		zoom.push_back(10000);
		fov.push_back(60.00);
	}
	else if (filename == "sim" || filename == "Sony")
	{
		// static cam

		zoom.push_back(0);
		fov.push_back(60.00);

		zoom.push_back(3231);
		fov.push_back(51.68);

		zoom.push_back(4391);
		fov.push_back(49.79);

		zoom.push_back(5666);
		fov.push_back(47.87);

		zoom.push_back(7579);
		fov.push_back(45.93);

		zoom.push_back(9492);
		fov.push_back(43.29);

		zoom.push_back(12362);
		fov.push_back(39.92);

		zoom.push_back(17145);
		fov.push_back(34.38);

		zoom.push_back(20971);
		fov.push_back(30.12);

		zoom.push_back(23521);
		fov.push_back(27.95);

		zoom.push_back(26710);
		fov.push_back(24.44);

		zoom.push_back(30809);
		fov.push_back(20.81);

		zoom.push_back(36275);
		fov.push_back(16.23);

		zoom.push_back(43928);
		fov.push_back(10.76);

		zoom.push_back(49029);
		fov.push_back(7.31);

		zoom.push_back(55406);
		fov.push_back(4.24);

		zoom.push_back(58427);
		fov.push_back(3.47);

		zoom.push_back(61783);
		fov.push_back(2.70);

		zoom.push_back(65535);
		fov.push_back(1.70);
	} // else loading from file
	else
	{
		std::ifstream file(filename);

		if (!file.is_open())
		{
			cout << "ERROR reading LUT" << endl;
		}
		else
		{
			for (std::string line; std::getline(file, line);)
			{
				line = line.substr(0, line.find("#"));
				if (!line.empty())
				{

					// split loaded lines to zoom and fov
					size_t space_delimiter_pos = line.find("\t");

					// load zoom
					std::string zoom_val = line.substr(0, space_delimiter_pos);
					zoom_val = zoom_val.erase(0, zoom_val.find_first_not_of("\t "));
					zoom_val = zoom_val.erase(zoom_val.find_last_not_of("\t ") + 1);

					// loads fov
					std::string fov_val = line.substr(space_delimiter_pos + 1);
					fov_val = fov_val.erase(0, fov_val.find_first_not_of("\t "));
					fov_val = fov_val.erase(fov_val.find_last_not_of("\t ") + 1);

					// loading to respective vectors
					if (m_verbose_debug)
					{
						cout << "LUT loading zoom: [" << zoom_val << "] and fov: [" << fov_val << "]" << endl;
					}
					zoom.push_back(stoi(zoom_val));
					fov.push_back(stof(fov_val));
				}
			}
		}
	}
	lut.push_back(zoom);
	lut.push_back(fov);

	// Set LUT to be contained in object memory
	m_lut = lut;

	for (auto& fov_ : fov)
	{
		if (max_fov == 0)
			max_fov = fov_;
		if (min_fov == 0)
			min_fov = fov_;
		if (fov_ > max_fov)
			max_fov = fov_;
		if (fov_ < min_fov)
			min_fov = fov_;
	}

	return lut;
}

float Client::zoomToFov(int zoom, std::vector<std::vector<float>> lut)
{
	// cout << "Zoom to FOV init, zoom: [" << zoom << "]" << endl;

	for (int i = 0; i < lut[0].size(); i++)
	{
		// cout << "i: " << i << " (" << i + 1 << "/" << lut[0].size() << ")" << endl;
		if (i != 0)
		{
			// cout << "Comparing lut[0][i] >= zoom > lut[0][i - 1] : " << lut[0][i] << " >= " << zoom << " > " << lut[0][i - 1] << endl;
		}
		if (i == 0)
		{
			// cout << "i=0; comparing min zoom with equivalent min lut: " << lut[0][0] << endl;
			// If zoom is below minimal zoom, set to min
			if (zoom <= lut[0][0])
			{
				// cout << "returning min zoom equivalent fov: " << lut[1][0] << endl;
				return lut[1][0];
			}
		}
		else if (lut[0][i] >= zoom && zoom > lut[0][i - 1])
		{
			// Fov is the ratio of zoom level range in, times the gap between respective fov levels(negative),
			// plus the larger fov value
			// cout << "TRUE, returning fov: " << ((zoom - lut[0][i - 1]) / (lut[0][i] - lut[0][i - 1])) * (lut[1][i] - lut[1][i - 1]) + lut[1][i - 1] << endl;
			return ((zoom - lut[0][i - 1]) / (lut[0][i] - lut[0][i - 1])) * (lut[1][i] - lut[1][i - 1]) + lut[1][i - 1];
		}
	}
	// If zoom is beyond maximal zoom, set to max
	// cout << "Zoom beyond max, returning equivalent fov" << endl;
	return lut[1][(lut[0].size()) - 1];
}

// TODO some tests
float Client::fovToZoom(float fov, std::vector<std::vector<float>> lut)
{
	// Is fov descending or ascending?
	bool is_ascending = lut[1][0] < lut[1][lut[0].size() - 1] ? true : false;
	// cout << "asc: " << is_ascending << endl;
	for (int i = 0; i < lut[0].size(); i++)
	{
		if (is_ascending)
		{
			if (i != 0)
			{
				// cout << "Comparing lut[1][i] >= fov > lut[1][i - 1] : " << lut[1][i] << " >= " << fov << " > " << lut[1][i - 1] << endl;
			}
			if (i == 0)
			{
				if (fov <= lut[1][0])
				{
					return lut[0][0];
				}
			}
			else if (lut[1][i] >= fov && fov > lut[1][i - 1])
			{
				return float(int(((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1]));
			}
		}
		else
		{
			if (i != 0)
			{
				// cout << "Comparing lut[1][i] < fov <= lut[1][i - 1] : " << lut[1][i] << " < " << fov << " <= " << lut[1][i - 1] << endl;
			}
			if (i == 0)
			{
				if (fov >= lut[1][0])
				{
					// cout << "RETURNING: " << lut[0][0] << endl;
					return lut[0][0];
				}
			}
			else if (lut[1][i] < fov && fov <= lut[1][i - 1])
			{
				// cout << "RETURNING: " << float(int(((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1])) << endl;
				return float(int(((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1]));
			}
		}
	}
	cout << "RETURNING: " << lut[0][(lut[0].size()) - 1];
	return lut[0][(lut[0].size()) - 1];
	// If zoom is beyond maximal zoom, set to max
	// cout << "Zoom beyond max, returning equivalent fov" << endl;
}

bool dont_allow_move_pan = false;
void Client::moveCameraByErrors(float pan_target, float tilt_target, float current_pan, float current_tilt, int speed, int speed_y,  int camera_address) {
	std::string w_current_pan = "";
	std::string where_tilt = "";
	std::string w_target_pan;
	float pan_error_offset = pan_target - current_pan;
	float tilt_error_offset = current_tilt - tilt_target;
	//float tilt_error_offset = int(current_tilt) - int(tilt_target);
	w_current_pan = checkAnglePosition(current_pan);
	w_target_pan = checkAnglePosition(pan_target);
	std::vector<char> msg = std::vector<char>(7);
	// std::cout << "[LOG] \t current_pan= " << current_pan << " target PAN= " << pan_target << " current_tilt= " << current_tilt << " tilt_target=" << tilt_target << "\n\n";
	
	 //only move pan
	if (pan_error_offset != 0 && abs(tilt_error_offset) < limit_move_ && dont_allow_move_pan == false && abs(abs(current_pan) - abs(pan_target)) > limit_move_) {
		if (w_target_pan == "1")
		{
			// std::cout << " [LOG] 1 \n";
			// std::cout << " [LOG] current_pan = " << current_pan << " - " << " pan_target = " << pan_target << " \n";
			if ((w_current_pan == "1" || w_current_pan == "2")) {
				if (current_pan >= pan_target) {
					// std::cout << " [LOG] xoay trai \n";
					panLeft(msg, speed, camera_address);
				}
				else {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
			}
			if (w_current_pan == "3") {
				if (abs(pan_target - current_pan) > 180) {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
				else {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
			}
			if (w_current_pan == "4") {
				panRight(msg, speed, camera_address);
				// std::cout << " [LOG] xoay phai \n";
			}
		}
		if (w_target_pan == "2") {
			// std::cout << " [LOG] 2 \n";
			// std::cout << " [LOG] current_pan = " << current_pan << " - " << " pan_target = " << pan_target << " \n";
			if (w_current_pan == "2" || w_current_pan == "3")
			{
				if (current_pan >= pan_target) {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
				else {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
			}
			if (w_current_pan == "4") {
				if (abs(pan_target - current_pan) > 180) {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
				else {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
			}
			if (w_current_pan == "1") {
				if (current_pan < pan_target) {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
			}
		}

		if (w_target_pan == "3") {
			// std::cout << " [LOG] 3 \n";
			// std::cout << " [LOG] current_pan = " << current_pan << " - " << " pan_target = " << pan_target << " \n";
			if (w_current_pan == "3" || w_current_pan == "4" || w_current_pan == "2")
			{
				if (current_pan >= pan_target) {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
				else {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
			}
			if (w_current_pan == "1") {
				if (current_pan > 30) {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
				else if (current_pan <= 30 && abs(pan_target - current_pan) > 180) {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
			}
		}
		if (w_target_pan == "4") {
			// std::cout << " [LOG] 4 \n";
			// std::cout << " [LOG] current_pan = " << current_pan << " - " << " pan_target = " << pan_target << " \n";
			if (w_current_pan == "4" || w_current_pan == "3")
			{
				if (current_pan >= pan_target) {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
				else {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
			}
			if (w_current_pan == "1") {
				{
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
			}
			if (w_current_pan == "2") {
				if (abs(pan_target - current_pan) > 180) {
					panLeft(msg, speed, camera_address);
					// std::cout << " [LOG] xoay trai \n";
				}
				else {
					panRight(msg, speed, camera_address);
					// std::cout << " [LOG] xoay phai \n";
				}
			}
		}

	}
	else 
	if (abs(pan_error_offset) < 0.5 && abs(tilt_error_offset) > limit_move_ && allow_move == true)
	{
		// std::cout << "[LOG] change TILT \n";
		if (tilt_target < current_tilt && abs((current_tilt) - (tilt_target)) > limit_move_) {
			// std::cout << "[LOG] move TILT down \n";
			tiltDown(msg, speed_y, 1);
		}
		if (tilt_target > current_tilt && abs((current_tilt) - (tilt_target)) > limit_move_) {
			// std::cout << "[LOG] move TILT up\n";
			tiltUp(msg, speed_y, 1);
		}
	}
	else if (abs(pan_error_offset) > 5  && tilt_target < current_tilt && abs((current_tilt) - (tilt_target)) >= limit_move_) {
		// std::cout << "[LOG] MOVE DOWN \t(current_tilt) =" << current_tilt << " Log " << " abs(tilt_target)) =" << tilt_target << "\n";
		// std::cout << "move_one_diagonal= " << move_one_diagonal << "\n";
		if (pan_target < current_pan && move_one_diagonal == "1") {
			// std::cout << "log tiltPanDownLeft \n";
			tiltPanDownLeft(msg, speed, speed_y, 1);
		}
		if (pan_target >= current_pan && move_one_diagonal == "2") {
			// std::cout << "log tiltPanDownRight \n";
			tiltPanDownRight(msg, speed, speed_y, 1);
		}
		
	}
	else if (abs(pan_error_offset) > 5 && tilt_target >= current_tilt && abs((current_tilt) - (tilt_target)) >= limit_move_) {
		// std::cout << "[LOG] MOVE UP \t(current_tilt) =" << current_tilt << " Log " << " (tilt_target) =" << tilt_target << "\n";
		if (pan_target < current_pan && move_one_diagonal == "3") {
			// std::cout << "log tiltPanUpLeft \n";
			tiltPanUpLeft(msg, speed, speed_y, 1);
		}
		if (pan_target >= current_pan && move_one_diagonal == "4") {
			// std::cout << "log tiltPanUpRight \n";
			tiltPanUpRight(msg, speed, speed_y, 1);
		}
	}
 	std::this_thread::sleep_for(std::chrono::milliseconds(250));
}
float Client::getCurrentPan(CamPosition_t camPosition, float north_bearing_offset) {
	float pan = camPosition.pan;
	float pan2stream = pan - north_bearing_offset;
	while (pan2stream < 0)
		pan2stream += 360.0f;
	while (pan2stream > 360.0f)
		pan2stream -= 360.0f;
	return pan2stream;
}
std::string Client::checkAnglePosition(int angle_) {
	if (angle_ >= 0 && angle_ <= 90) return "1";
	if (angle_ > 90 && angle_ <= 180) return "2";
	if (angle_ > 180 && angle_ <= 270) return "3";
	if (angle_ > 270 && angle_ <= 360) return "4";
}

void Client::setZoomSpeed(std::vector<char>& msg, int cam_addr, int speed) {
	msg[0] = 0xff;
	msg[1] = cam_addr % 256;
	msg[2] = 0x00;
	msg[3] = 0x25;
	msg[4] = 0x00;
	if (speed == 0) msg[5] = 0x00;
	if (speed == 1) msg[5] = 0x01;
	if (speed == 2) msg[5] = 0x02;
	if (speed == 3) msg[5] = 0x03;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	if (control_interface == 1) sendTcpNoRes(msg);
	else if (control_interface == 2) write_serial_nores(msg);
}

void Client::testQuery() {
	float roll = 0; 
	float pitch = 0;
	float yaw = 0;
	std::cout << "[LOG] STILL QUERY \n";
	// Automatic Send this message after connected 
	std::vector<uint8_t> AttitudeMsg, GlobalPosIntMsg, GPSRawIntMsg, SysStatusMsg, HeartbeatMsg;

	std::vector<uint8_t> data_send;

	data_send = MavlinkDigiCamMsg(Set_Gimbal, 0, 0, 0, 0, 0, 0);
	sendUDPnores(data_send);

	AttitudeMsg = MavlinkAttitudeMsg(0, 0, 0); 

	sendUDPnores(AttitudeMsg);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	bool drone_armed = false;
	HeartbeatMsg =  MavlinkHeartbeatMsg(drone_armed);
    sendUDPnores(HeartbeatMsg);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	float drone_lat, drone_lon, drone_alt;
	drone_lat = 32.19416;
	drone_lon = 34.8815231; 
	drone_alt = 150;
	GlobalPosIntMsg = MavlinkGlobalPosIntMsg(drone_lat,drone_lon, drone_alt, 100, 0, 0, 0);

	sendUDPnores(GlobalPosIntMsg);
	
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	int sat_count = 12;
	GPSRawIntMsg = MavlinkGPSRawIntMsg(sat_count);

	sendUDPnores(GPSRawIntMsg);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	float batt_voltage = 16.5;
	SysStatusMsg = MavlinkSysStatusMsg(batt_voltage); 

    sendUDPnores(SysStatusMsg);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

Client::~Client() {
	// cout << "Client destroyed";
};
