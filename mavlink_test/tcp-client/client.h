#pragma once
#include <boost/lambda/lambda.hpp>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/deadline_timer.hpp>
#include "CamPosition_t.h"
#include "commands.h"
#include "mavlink_camera.h"

// #include "../vizgard/config.h"
#include "../camera-control/PlatformController/Codegen/Platform_Controller_ert_rtw/Platform_Controller.h"
// #include "../position/mavlink/mavlink_vizgard.h"
#define FAIL_CONNECT 1
#define SUCCESS_CONNECT 0
#define MAVLINK_CAM_CONTROL_RATE 10.0
using namespace boost::asio;
using ip::tcp;
using ip::udp;
using std::cout;
using std::endl;
using std::string;

extern bool dont_allow_move_pan;
extern float checkValWithOffset(float panval);



enum class ConnectionState
{
	Connected = 0,
	Waiting = 1,
	Refused = 2,
	Closed = 3,
};

enum class ParsingState
{
	WaitingHeader = 0,
	WaitingAdressCam = 1,
	WaitingInfoCam = 2,
	WaitingResponseCode1 = 3,
	WaitingResponseCode2 = 4,
	WaitingData1 = 5,
	WaitingData2 = 6,
	WaitingChecksum = 7,
	EndOfParsing = 8,
};

enum class ParsingState3
{
	WaitingStartOfFrame = 0,
	WaitingROS = 1,
	WaitingColon = 2,
	WaitingCommand = 3,
	WaitingData = 4,
	WaitingChecksum = 5,
	WaitingEndOfFrame = 6,
	EndOfFrame = 7
};

enum class DataPanTiltZoom
{
	Unknown = 0,
	Pan = 1,
	Tilt = 2,
	Zoom = 3,
};


class Client : public Mavlink_Camera
{
private:
	std::string m_version = "0.2.11";
	int control_interface;

	bool m_verbose_debug = false;
	bool m_connected_status = false;
	// serial camera 
	std::string serial_name;
	int baud_rate;
	int serial_port;

	int camera_interval;
	// Connection details
	std::string m_ip_address;
	int m_target_port;
	int m_connection_priority;
	// Connection context
	boost::asio::io_service &m_io_service;
	tcp::socket m_active_socket;
	udp::socket m_active_socket_udp;
	// Internal setup
	std::vector<std::vector<float>> m_lut;
	mutable std::mutex m_mutex;
	deadline_timer m_deadline_timer;
	int current_camera;
	// new parse response
	uint16_t checksum;
	uint8_t data1;
	uint8_t data2;
	uint16_t data;
	DataPanTiltZoom ptz;
	ParsingState ps;
	// new parse response 3
	std::vector<float> vdata;
	std::string _data;
	std::string ros;
	std::string cmd;
	ParsingState3 ps3;



public:
	Client(boost::asio::io_service& ios, std::string ip, int port, int priority = 10, std::string lut_address = "sim", bool verbose = false); // Constructor
	Client(boost::asio::io_service& ios, std::string ip, int port, int priority = 10, std::string lut_address = "sim");						  // Constructor
	Client(boost::asio::io_service& ios, std::string lut_address = "sim", bool verbose = false);											  // Constructor
	~Client();																																  // Destructor

	// camera serial 
	void setSerial(std::string serial_name_, int baud_rate_);
	void updateInterface(int control_interface_) {
		control_interface = control_interface_;
	};
	int initConnectionSerial();
	bool check_connection_status();
	// Serial sending
	std::string write_serial_(std::vector<char>& msg);
	std::string write_serial_(std::string &msg);
	std::string write_serial_nores(std::vector<char>& msg);
	std::string write_serial_nores(std::string& msg);

	// MAVLINK CAMERA
	int mavlink_protocol_ver = 2;
    int mavlink_header_length;
    uint8_t mavlink_start_of_frame;
    int mavlink_msg_id_location;
    const char* upd_addess;
    int udpport;
	udp::endpoint remote_endpoint;
	void PanTiltAbsolute(float pan = 100, float tilt = 100);

	void connectUDP(std::string ip, int port);
	std::string sendUDPnores(std::vector<uint8_t>& msg);
	void testQuery();
	void handleRecv(const boost::system::error_code& error, size_t bytes_transferred);
	void handleReadUDP();
	////// 

	// Basic getters
	std::string getIp() { return m_ip_address; }
	int getPort() { return m_target_port; }
	bool getStatus() { return m_connected_status; }
	std::string getVersion() { return m_version; }
	std::vector<std::vector<float>> getLUT() { return m_lut; }

	// Other stuff
	float current_fov = 0;
	float max_fov;
	float min_fov;
	int is_zooming = 0;
	int zoom_times = 10;
	int limit_move_ = 0;
	bool allow_move = false;
	std::string move_one_diagonal = "";
	std::string test_str = "";
	// Setup functions
	void setParameters(std::string ip, int port, int priority); // Setting up parameters
	void setVerbose(bool verbose);
	int connectToManager(const std::string& ip, int port, int priority);
	int connectToManager(const std::string& ip, int port, int priority, bool& exit_flag);
	void OnConnect(const boost::system::error_code& ec);
	void setCameraInterval(int interval);
	// Camera manager messages
	void testConnection(std::vector<char>& msg);
	void setPriority(std::vector<char>& msg, int priority = 100, int cam_address = 1);

	// Query messages
	std::string queryPosition(int cam_address = 1, float north_bearing_offset = 0.0f);
	std::string queryPosition(CamPosition_t &camPosition, int cam_address = 1, float north_bearing_offset = 0.0f);
	std::string queryPan(std::vector<char> &msg, int cam_address = 1);
	std::string queryTilt(std::vector<char> &msg, int cam_address = 1);
	std::string queryZoom(std::vector<char> &msg, int cam_address = 1);

	// Getters for basic information
	int getZoom(std::vector<char> &msg, int cam_address = 1);

	// PTZ command messagess relative
	void panLeft(std::vector<char> &msg, int pan_speed = 1, int cam_address = 1);
	void tiltPanUpLeft(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void tiltUp(std::vector<char> &msg, int tilt_speed = 1, int cam_address = 1);
	void tiltPanUpRight(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void panRight(std::vector<char> &msg, int pan_speed = 1, int cam_address = 1);
	void tiltPanDownLeft(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void tiltDown(std::vector<char> &msg, int tilt_speed = 1, int cam_address = 1);
	void tiltPanDownRight(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void zoomIn(std::vector<char> &msg, int cam_address = 1);
	void zoomOut(std::vector<char> &msg, int cam_address = 1);
	void autoFocusOn(std::vector<char>& msg, int cam_address = 1);
	void autoFocusOff(std::vector<char>& msg, int cam_address = 1);
	void autoFocus(std::vector<char>& msg, int cam_address = 1, int time_=3);


	void stopCamera(std::vector<char> &msg, int cam_address = 1);

	// PTZ command messages absolute in degrees and raw form (zoom is equivalent)
	// void panAbsoluteDeg(std::vector<char>& msg, int pan = 9000, int cam_address = 1);
	// void panAbsoluteRaw(std::vector<char>& msg, int pan = 0, int cam_address = 1);

	// void tiltAbsoluteDeg(std::vector<char>& msg, int tilt = 0, int cam_address = 1);
	// void tiltAbsoluteRaw(std::vector<char>& msg, int tilt = 0, int cam_address = 1);

	void panAbsolute(std::vector<char> &msg, int pan = 9000, int cam_address = 1);
	void tiltAbsolute(std::vector<char> &msg, int tilt = 0, int cam_address = 1);
	void zoomAbsolute(std::vector<char> &msg, int zoom = 1, int cam_address = 1);

	// Extra FoV commands
	void fovAbsolute(std::vector<char> &msg, float fov = 1, int cam_address = 1);
	void changeZoomByFoV(std::vector<char> &msg, float fov_change = 1, int cam_address = 1);

	// Zoom to FoV conversion
	std::vector<std::vector<float>> loadLut(std::string filename);
	float zoomToFov(int zoom, std::vector<std::vector<float>> lut);
	float fovToZoom(float fov, std::vector<std::vector<float>> lut);

	// Centering
	void moveTowardsCenter(int x_target, int y_target, int x_current, int y_current, std::string& camera_mode, float& x_speed, float& y_speed, int tolerance_radius = 10, float max_size_vector = 100, int max_speed = 20, int bouder_for_extra = 0, float random_number_for_extra = 0, int magic_point = 0, int camera_address = 1, bool loging_camera = false);
	void moveTowardsCenterImprove(int x_speed, int y_speed, int camera_address = 1);

	std::vector<float> moveAbs(int x_target, int y_target, int x_current, int y_current, CamPosition_t camPosition, int camera_address = 1);
	void test(int speed);

	// TCP sending
	std::string sendTcp(const std::vector<char>& msg, size_t at_least = 7);
	std::string sendTcpNoRes(const std::vector<char>& msg);

	std::string sendTcp(const std::string& msg, size_t at_least = 7);
	std::string sendTcp(const std::string& msg, int timeout, const std::string& ip, int port, size_t at_least = 7);
	std::string sendTcpNoRes(const std::string& msg);
	std::string sendTcpNoRes(const std::string& msg, int timeout, const std::string& ip, int port);

	void readWithTimeout(tcp::socket& s, const boost::asio::streambuf& buffers, const boost::asio::deadline_timer::duration_type& expiry_time);
	void moveCameraByErrors(float pan_target, float tilt_target, float current_pan, float current_tilt, int speed,int speed_y, int camera_address);
	void setZoomSpeed(std::vector<char>& msg, int cam_addr, int speed);
	float getCurrentPan(CamPosition_t camPosition, float north_bearing_offset);
	std::string checkAnglePosition(int angle_);
};
