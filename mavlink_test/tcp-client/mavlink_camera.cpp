// #include "mavlink_vizgard.h"

// // Constructor
// Client::Client(boost::asio::io_service& ios, string ip, int port, int priority, std::string lut_address, bool verbose) : // io context
// 	m_io_service(ios),
// 	// socket creation
// 	m_active_socket_udp(ios),
// 	// connection parameters
// 	m_ip_address(ip),
// 	m_target_port(port),
// 	m_connection_priority(priority),
// 	// timeout
// 	m_deadline_timer(ios),
// 	// lut
// 	m_lut()
// {
//     // cout << "Client started!";
// 	setVerbose(verbose);
// 	const int timeout = 200;
// 	::setsockopt(m_active_socket_udp.native_handle(), SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);

// 	// If an address to LUT file is specified, try to load it
// 	// if (lut_address != "")
// 	// 	loadLut(lut_address);

// 	// connectToManager(ip, port, priority);
// }


// int Client::connectToManager(const std::string& ip, int port, int priority)
// {
// 	// Platform_Controller_initialize();
// 	if (m_verbose_debug)
// 	{
// 		cout << "attempting to connect socket | IP:" << ip << ", port:" << port << ", priority:" << priority << endl;
// 	}
// 	try
// 	{
// 		m_active_socket_udp.connect(udp::endpoint(boost::asio::ip::address::from_string(ip), port));
// 		// cout << "Socket connected" << endl;
// 		m_connected_status = true;
// 	}
// 	catch (const boost::system::system_error& ex)
// 	{
// 		// cout << "Socket connection failed" << endl;
// 		// cout << "Boost sys error" << endl;
// 		m_connected_status = false;
// 	}
// 	catch (std::exception& e)
// 	{
// 		// cout << "Socket connection failed" << endl;
// 		// cout << "Exception caught" << endl;
// 		m_connected_status = false;
// 	}

// 	// if (m_connected_status == true)
// 	// {
// 	// 	std::vector<char> msg = std::vector<char>(7);
// 	// 	testConnection(msg);		// Returns cam manager id and version
// 	// 	setPriority(msg, priority); // Sets priority
// 	// }
// };


// void Client::setVerbose(bool verbose = false)
// {
// 	Client::m_verbose_debug = verbose;
// }

// void Client::sendUdpNoRes(uint8_t* tx_packet)
// {
	
// }
// Client::~Client() {
// 	cout << "Client destroyed";
// };

#include "mavlink_camera.h"
#include <thread>
unsigned long get_baud_(unsigned long baud)
{
    switch (baud) {
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    default: 
        return B0;
    }
}


///////////////////////////////////////////////// Gimbal control stuff ///////////////////////////////////////////////////////////////////////////
bool Mavlink_Camera::connect_udp_protol(const char* upd_addess, int udpport) {
	std::cout << "[LOG] 127 \n";
    connection_type = 1;
    sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    //struct sockaddr_in fromAddr;
    ssize_t recsize;
    int bytes_sent;
    uint16_t len;
    int i = 0;
    //int success = 0;
    unsigned int temp = 0;

    if (sock < 0) {
        perror("socket");
        return 1;
    }
    // allow multiple sockets to use the same PORT number
    u_int yes = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0) {
        perror("Reusing ADDR failed");
        return 1;
    }
	std::cout << "[LOG] 2 \n";
    /* Bind the socket to rx_port - necessary to receive packets */
	memset(&locAddr, 0, sizeof(locAddr));
	locAddr.sin_family = AF_INET;
	locAddr.sin_addr.s_addr = INADDR_ANY;
	locAddr.sin_port = htons(udpport);  // API//OffBoard

	/* Bind the socket to port 14551 - necessary to receive packets from qgroundcontrol */ 
	if (-1 == bind(sock,(struct sockaddr *)&locAddr, sizeof(struct sockaddr)))
    {
		perror("error bind failed");
        
		close(sock);
        std::cout << "clsoe socket \n";
        return 1;
		// exit(EXIT_FAILURE);
    } 

   std::cout << "[LOG] 3 \n";
#if (defined __QNX__) | (defined __QNXNTO__)
	if (fcntl(sock, F_SETFL, O_NONBLOCK | FASYNC) < 0)
#else
	if (fcntl(sock, F_SETFL, O_NONBLOCK | O_ASYNC) < 0)
#endif
     {
		fprintf(stderr, "error setting nonblocking: %s\n", strerror(errno));
		// close(sock);
		// exit(EXIT_FAILURE);
        return 1;
    }
    

    memset(&gcAddr, 0, sizeof(gcAddr));
	gcAddr.sin_family = AF_INET;
	gcAddr.sin_addr.s_addr = inet_addr(upd_addess);
	gcAddr.sin_port = htons(udpport); // QGroundControl / Other GCS Port: Set Default
	std::cout << "[LOG] 4 \n";
    return 0;
}


void Mavlink_Camera::MavCom_read_Message() {
    if (connection_type ==1) {
        ssize_t recsize;
        socklen_t fromlen = sizeof(gcAddr);
        unsigned int temp = 0; 
        // 1024 is buffer len
        memset(buf1, 0, 1024);
        // time out 
        fd_set fds;
        int n;
        struct timeval tv;

        FD_ZERO(&fds);
        FD_SET(sock, &fds);
        n = select(sock+1, &fds, NULL, NULL, &tv);
        if (n == 0)
        {
            std::cout << " time out connect mavlink camera ... \n";
            is_conneted = false;
        }
        recsize = recvfrom(sock, (void *)buf1, 1024, 0, (struct sockaddr *)&gcAddr, &fromlen);

    }
}

void Mavlink_Camera::MavComm_Send_Message(uint8_t* buf_send) {
    int bytes_sent; 
    if (connection_type == 1) {

        bytes_sent = sendto(sock, buf_send,sizeof(buf_send), 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
    }
    else if (connection_type == 2) {

    }


}

/****************************************************************************************************************************
*                                                 COMMAND LONG FUNCTIONS
****************************************************************************************************************************/
/****************************************************************************************************************************
*                                                      doDigicamControl()
*                                                      
* Description : Sending the required doSetRoi command.
* 
* Arguments   : uint8_t packet - reference to the byte array that will get the constructed packet to send.
*
* Returns     : none
* 
****************************************************************************************************************************/
// void Mavlink_Comms::doSetRoi(uint8_t packet)
// {
//     float lat, lon, alt;
//     /* Validation of Latitude Longitude and Altitude */
//     if (!float.TryParse(p1TextBox.Text, out lat))
//     {
//         MessageBox.Show("Invalid Latitude", "Error");
//         return;
//     }
//     if (lat < -90.0 || lat > 90.0)
//     {
//         MessageBox.Show("Invalid Latitude (Value must be between -90 to +90)", "Error");
//         return;
//     }
//     if (!float.TryParse(p2TextBox.Text, out lon))
//     {
//         MessageBox.Show("Invalid Longitude", "Error");
//         return;
//     }
//     if (lon < -180.0 || lon > 180.0)
//     {
//         MessageBox.Show("Invalid Longitude (Value must be between -180 to +180)", "Error");
//         return;
//     }
//     if (!float.TryParse(p3TextBox.Text, out alt))
//     {
//         MessageBox.Show("Invalid Altitude", "Error");
//         return;
//     }
//     mav_protocol.MavlinkSendROILocationMsg(ref packet, lat, lon, alt);
// }

	/****************************************************************************************************************************
*                                                      doDigicamControl()
*                                                      
* Description : Sending the required DO_DIGICAM_CONTROL command.
* 
* Arguments   : uint8_t packet - reference to the byte array that will get the constructed packet to send.
*
* Returns     : none
* 
****************************************************************************************************************************/
void Mavlink_Camera::doDigicamControl(uint8_t packet) {

}

/****************************************************************************************************************************
*                                            MavlinkCrcCalculate()
*
* Description : calculates CRC for Mavlink Protocol
*
* Arguments   : byte[] pBuffer - buffer pointer
* 				int length - buffer length
*
* Returns     : CRC
*
****************************************************************************************************************************/
ushort Mavlink_Camera::MavlinkCrcCalculate(uint8_t* pBuffer, int length)
{
	ushort crcTmp;
	int i;

	if (length < 1)
	{
		return 0xffff;
	}

	crcTmp = X25_INIT_CRC;

	for (i = 1; i < length; i++) // skips header
	{
		crcTmp = MavlinkCrcAccumulate(pBuffer[i], crcTmp);
	}

	return (crcTmp);
}

ushort Mavlink_Camera::MavlinkCrcAccumulate(uint8_t b, ushort crc)
{
	uint8_t ch = (uint8_t)(b ^ (uint8_t)(crc & 0x00ff));
	ch = (uint8_t)(ch ^ (ch << 4));

	return (ushort)((crc >> 8) ^ ((ushort)ch << 8) ^ ((ushort)ch << 3) ^ ((ushort)ch >> 4));
}

std::vector<uint8_t> Mavlink_Camera::MavlinkDigiCamMsg(float os_cmd, float os_p1, float os_p2, float os_p3, float os_p4, float os_p5 = 0, float os_p6 = 0) {
	/* Mavlink Digicam Packet */
	uint8_t* mavlink_cmd_long_packet;  
	mavlink_protocol_ver = 0x02;
	SetMavProtoValuesByVersion();
	// const uint8_t MAVLINK_START_OF_FRAME = 0xFE;  
	
	std::vector<uint8_t> tx_packet;
	tx_packet = std::vector<uint8_t>{
		MAVLINK_2_START_OF_FRAME, 0x21,0x00, 0x00, 0x00, 0xFF, 0x00, 0x4C,0x00,0x00, /* Mavlink Header */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param1 - Os Command */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param2 - Os Param 1 */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param3 - Os Param 2 */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param4 - Os Param 3 */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param5 - Os Param 4 */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param6 - Os Param 5 */
		0x00, 0x00, 0x00, 0x00,                                                     /* Param7 - Os Param 6 */
		0xCB, 0x00,                                                                 /* Command - DO_DIGICAM_CONTROL */
		0x01,                                                                       /* Target System */
		0x00,                                                                       /* Target Component */
		0x00,                                                                       /* Confirmation */
		0x00, 0x00                                                                  /* Mavlink Message CRC */
	};
	

	ushort mavlink_crc;
	uint8_t* os_cmd_byteArray= reinterpret_cast<uint8_t*>((void*)&os_cmd);
	uint8_t* os_p1_byteArray = reinterpret_cast<uint8_t*>((void*)&os_p1);
	uint8_t* os_p2_byteArray = reinterpret_cast<uint8_t*>((void*)&os_p2);
	uint8_t* os_p3_byteArray = reinterpret_cast<uint8_t*>((void*)&os_p3);
	uint8_t* os_p4_byteArray = reinterpret_cast<uint8_t*>((void*)&os_p4);
	uint8_t* os_p5_byteArray = reinterpret_cast<uint8_t*>((void*)&os_p5);
	uint8_t* os_p6_byteArray = reinterpret_cast<uint8_t*>((void*)&os_p6);
	
	memcpy(tx_packet.data() + mavlink_header_length ,   os_cmd_byteArray, 4);
	memcpy(tx_packet.data() + mavlink_header_length + 4 , os_p1_byteArray, 4);
	memcpy(tx_packet.data() + mavlink_header_length + 8 , os_p2_byteArray, 4);
	memcpy(tx_packet.data() + mavlink_header_length + 12, os_p3_byteArray, 4);
	memcpy(tx_packet.data() + mavlink_header_length + 16, os_p4_byteArray, 4);
	memcpy(tx_packet.data() + mavlink_header_length + 20, os_p5_byteArray, 4);
	memcpy(tx_packet.data() + mavlink_header_length + 24, os_p6_byteArray, 4);
	// printf("mavlink_cmd_long_packet =");
	
	/* calculate the mavlink message CRC */
	mavlink_crc = MavlinkCrcCalculate(tx_packet.data(), 45 - 2); /* All message bytes execpt the CRC bytes */
	mavlink_crc = MavlinkCrcAccumulate(mavproto.MAVLINK_CMD_LONG_CRC, mavlink_crc);
	/* put the calculated CRC in the mavlink message */
	tx_packet[45 - 2] = (uint8_t)mavlink_crc;
	tx_packet[45 - 1] = (uint8_t)(mavlink_crc >> 8);

	// printf("mavlink_cmd_long_packet =");
	// for (int idx = 0; idx < 45; idx++) {
	//     printf(" %x ", (uint8_t)*(mavlink_cmd_long_packet+idx));
	// }
	// std::cout << "\n";

	/* copy the ready packet to the tx_packet */
	// tx_packet = mavlink_cmd_long_packet.ToArray();
	
	/* Send the message */
	// MavComm_Send_Message(mavlink_cmd_long_packet);

	return tx_packet;
}

std::vector<uint8_t> Mavlink_Camera::MavlinkAttitudeMsg(float roll, float pitch, float yaw) {
    /* Mavlink Attitude Packet -    
    * The following values are used by the Obervation System: Roll, Pitch, Yaw */
    std::vector<uint8_t> mavlink_attitude_packet;
    const uint8_t MAVLINK_2_START_OF_FRAME = 0xFD;
    SetMavProtoValuesByVersion();
    mavlink_attitude_packet = std::vector<uint8_t>
    {
        MAVLINK_2_START_OF_FRAME, 0x1C, 0x00, 0x00, 0x00, 0x01, 0x01, 0x1E, 0x00, 0x00, /* Mavlink Header */
        0x00, 0x00, 0x00, 0x00,                                                         /* Time Since Boot */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,         /* Roll, Pitch, Yaw */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,         /* Roll Speed, Pitch Speed, Yaw Speed */          
        0x00, 0x00                                                                      /* Mavlink Message CRC */
    };

    ushort mavlink_crc;
    roll = roll*M_PI/180;
    pitch = pitch*M_PI/180;
    yaw = yaw*M_PI/180;
    uint8_t* roll_byteArray  = reinterpret_cast<uint8_t*>((void*)&roll); 
    uint8_t* pitch_byteArray = reinterpret_cast<uint8_t*>((void*)&pitch);  
    uint8_t* yaw_byteArray   = reinterpret_cast<uint8_t*>((void*)&yaw);  

    /* copy the values to the outgoing message */
    memcpy(mavlink_attitude_packet.data() + mavlink_header_length + 4,   roll_byteArray, 4);
    memcpy(mavlink_attitude_packet.data() + mavlink_header_length + 8 , pitch_byteArray, 4);
    memcpy(mavlink_attitude_packet.data() + mavlink_header_length + 12 , yaw_byteArray, 4);

    /* calculate the mavlink message CRC */
    mavlink_crc = MavlinkCrcCalculate(mavlink_attitude_packet.data(), 40 - 2); /* All message bytes execpt the CRC bytes */
    mavlink_crc = MavlinkCrcAccumulate(mavproto.MAVLINK_ATTITUDE_MSG_CRC, mavlink_crc);

    /* put the calculated CRC in the mavlink message */
    mavlink_attitude_packet[40 - 2] = (uint8_t)mavlink_crc;
    mavlink_attitude_packet[40 - 1] = (uint8_t)(mavlink_crc >> 8);
    /* Send the message */
    return mavlink_attitude_packet;
}

std::vector<uint8_t> Mavlink_Camera::MavlinkGlobalPosIntMsg(float lat, float lon, int alt, int rel_alt, float vx, float vy, float vz) {
     /* Mavlink Global Position Packet - 
    The following values are used by the Obervation System: Latitude, Longitude, Altitude, Relative Altitude, Vx, Vy, Vz */
    SetMavProtoValuesByVersion();
    std::vector<uint8_t> mavlink_global_pos_int_packet;
    mavlink_protocol_ver = 0x02;
    mavlink_global_pos_int_packet =  std::vector<uint8_t>{
        MAVLINK_2_START_OF_FRAME, 0x1C, 0x00, 0x00, 0x00, 0x01, 0x01, 0x21, 0x00, 0x00, /* Mavlink Header */
        0x00, 0x00, 0x00, 0x00,                                                         /* Time Since Boot */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                 /* Latitude, Longitude */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                 /* Altitude, Relative Altitude */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                 /* Vx, Vy, Vz, Heading */   
        0x00, 0x00                                                                      /* Mavlink Message CRC */
    };
    
    ushort mavlink_crc;

    int lat_int = (int)(lat * 10000000.0);
    int lon_int = (int)(lon * 10000000.0);
    short vx_short = (short)(vx * 100.0);
    short vy_short = (short)(vy * 100.0);
    short vz_short = (short)(vz * 100.0);
    int alt_int = (int)(alt * 1000.0);
    int rel_alt_int = (int)(rel_alt * 1000.0);

    uint8_t* lat_byteArray =     reinterpret_cast<uint8_t*>((void*)&lat_int);
    uint8_t* lon_byteArray =     reinterpret_cast<uint8_t*>((void*)&lon_int);
    uint8_t* alt_byteArray =     reinterpret_cast<uint8_t*>((void*)&alt_int);
    uint8_t* rel_alt_byteArray = reinterpret_cast<uint8_t*>((void*)&rel_alt_int);
    uint8_t* vx_byteArray =      reinterpret_cast<uint8_t*>((void*)&vx_short);
    uint8_t* vy_byteArray =      reinterpret_cast<uint8_t*>((void*)&vy_short);
    uint8_t* vz_byteArray =      reinterpret_cast<uint8_t*>((void*)&vz_short);

    /* copy the values the outgoing message */
    memcpy(mavlink_global_pos_int_packet.data() +mavlink_header_length + 4,   lat_byteArray, 4);
    memcpy(mavlink_global_pos_int_packet.data() + mavlink_header_length + 8,  lon_byteArray, 4);
    memcpy(mavlink_global_pos_int_packet.data() + mavlink_header_length + 12, alt_byteArray, 4);
    memcpy(mavlink_global_pos_int_packet.data() + mavlink_header_length + 16, rel_alt_byteArray, 4);
    memcpy(mavlink_global_pos_int_packet.data() + mavlink_header_length + 20, vx_byteArray, 4);
    memcpy(mavlink_global_pos_int_packet.data() + mavlink_header_length + 22, vy_byteArray, 4);
    memcpy(mavlink_global_pos_int_packet.data() + mavlink_header_length + 24, vz_byteArray, 4);

    /* calculate the mavlink message CRC */
    mavlink_crc = MavlinkCrcCalculate(mavlink_global_pos_int_packet.data(), mavlink_global_pos_int_packet.size() - 2); /* All message bytes execpt the CRC bytes */
    mavlink_crc = MavlinkCrcAccumulate(MAVLINK_GBL_POS_INT_CRC, mavlink_crc);

    /* put the calculated CRC in the mavlink message */
    mavlink_global_pos_int_packet[mavlink_global_pos_int_packet.size() - 2] = (uint8_t)mavlink_crc;
    mavlink_global_pos_int_packet[mavlink_global_pos_int_packet.size() - 1] = (uint8_t)(mavlink_crc >> 8);
    return mavlink_global_pos_int_packet;
}

std::vector<uint8_t> Mavlink_Camera::MavlinkGPSRawIntMsg(int sat_count) {
    /* Mavlink GPS Raw int Packet - 
    The following values are used by the Obervation System: Satellites Visable */
    std::vector<uint8_t> mavlink_gps_raw_int_packet;
    SetMavProtoValuesByVersion();
    mavlink_gps_raw_int_packet = std::vector<uint8_t>
    {
        MAVLINK_2_START_OF_FRAME, 0x1E, 0x00, 0x00, 0x00, 0x01, 0x01, 0x18, 0x00, 0x00, /* Mavlink Header */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                 /* TimeStamp */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,         /* Latitude, Longitude, Altitude */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                 /* Eph, Epv, Vel, Cog */
        0x00,                                                                           /* Fix Type */   
        0x00,                                                                           /* Satellites Visable */
        0x00, 0x00                                                                      /* Mavlink Message CRC */
    };
    

    ushort mavlink_crc;

    /* update the satelite count in the outgoing message */
    mavlink_gps_raw_int_packet[mavlink_header_length + 29] = (uint8_t)sat_count;

    /* calculate the mavlink message CRC */
    mavlink_crc = MavlinkCrcCalculate(mavlink_gps_raw_int_packet.data(), mavlink_gps_raw_int_packet.size() - 2); /* All message bytes execpt the CRC bytes */
    mavlink_crc = MavlinkCrcAccumulate(MAVLINK_GPS_RAW_INT_CRC, mavlink_crc);

    /* put the calculated CRC in the mavlink message */
    mavlink_gps_raw_int_packet[mavlink_gps_raw_int_packet.size() - 2] = (uint8_t)mavlink_crc;
    mavlink_gps_raw_int_packet[mavlink_gps_raw_int_packet.size() - 1] = (uint8_t)(mavlink_crc >> 8);
    return mavlink_gps_raw_int_packet;
}

std::vector<uint8_t> Mavlink_Camera::MavlinkSysStatusMsg( float batt_voltage) {
    /* Mavlink System Status Packet - 
    The following values are used by the Obervation System: Battery Voltage */
    std::vector<uint8_t> mavlink_sys_status_packet;
    SetMavProtoValuesByVersion();
    mavlink_sys_status_packet = std::vector<uint8_t>
    {
        MAVLINK_2_START_OF_FRAME, 0x1F, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00, /* Mavlink Header */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,         /* OCS Present, OCS Enabled, OCS Health */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                 /* Load, Battery Voltage, Battery Current, Drop Rate */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                     /* Error Counters */
        0x00,                                                                           /* Battery Remaining */   
        0x00, 0x00                                                                      /* Mavlink Message CRC */
    };
    
    ushort mavlink_crc;

    short batt_voltage_short = (short)(batt_voltage * 1000.0);
    uint8_t* batt_voltage_byteArray =  reinterpret_cast<uint8_t*>((void*)&batt_voltage_short);

    /* update the battery voltage in the outgoing message */
    memcpy(mavlink_sys_status_packet.data() +mavlink_header_length + 14,   batt_voltage_byteArray, 4);

    /* calculate the mavlink message CRC */
    mavlink_crc = MavlinkCrcCalculate(mavlink_sys_status_packet.data(), mavlink_sys_status_packet.size() - 2); /* All message bytes execpt the CRC bytes */
    mavlink_crc = MavlinkCrcAccumulate(MAVLINK_SYS_STAT_CRC, mavlink_crc);

    /* put the calculated CRC in the mavlink message */
    mavlink_sys_status_packet[mavlink_sys_status_packet.size() - 2] = (uint8_t)mavlink_crc;
    mavlink_sys_status_packet[mavlink_sys_status_packet.size() - 1] = (uint8_t)(mavlink_crc >> 8);
    return mavlink_sys_status_packet;
}

std::vector<uint8_t> Mavlink_Camera::MavlinkHeartbeatMsg( bool drone_armed) {
    /* Mavlink Heart Beat Packet - 
    The following values are used by the Obervation System: Base Mode */
    std::vector<uint8_t> mavlink_heart_beat_packet;
    SetMavProtoValuesByVersion();
    mavlink_heart_beat_packet = std::vector<uint8_t>
    {
        MAVLINK_2_START_OF_FRAME, 0x09, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00,/* Mavlink Header */
        0x00, 0x00, 0x00, 0x00,                                                         /* Custom Mode */
        0x00, 0x03,                                                                     /* Mav Type, AutoPilot Type */ 
        0x00, 0x00, 0x00,                                                               /* Base Mode, System Status Flag, Mavlink Version */
        0x00, 0x00                                                                      /* Mavlink Message CRC */
    };
    ushort mavlink_crc;
    /* update the drone armed state */
    if (drone_armed)
        mavlink_heart_beat_packet[mavlink_header_length + 6] |= MAVLINK_DRONE_ARMED_MASK;
    else
        mavlink_heart_beat_packet[mavlink_header_length + 6] &= (MAVLINK_DRONE_ARMED_MASK ^ 0xFF);
    /* calculate the mavlink message CRC */
    mavlink_crc = MavlinkCrcCalculate(mavlink_heart_beat_packet.data(), 21 - 2); /* All message bytes execpt the CRC bytes */
    mavlink_crc = MavlinkCrcAccumulate(MAVLINK_HEART_BEAT_CRC, mavlink_crc);
    /* put the calculated CRC in the mavlink message */
    mavlink_heart_beat_packet[21 - 2] = (uint8_t)mavlink_crc;
    mavlink_heart_beat_packet[21 - 1] = (uint8_t)(mavlink_crc >> 8);

    // std::cout << "HeartbeatMsg = " << "\n";
	// for (int i = 0 ; i < mavlink_heart_beat_packet.size(); i++ ){
	// 	printf("%x-", (mavlink_heart_beat_packet[i]));
	// }
	// std::cout << "\n";
    return mavlink_heart_beat_packet;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

