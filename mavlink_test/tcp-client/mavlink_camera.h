// #pragma once
// #include <iostream>
// #include <iterator>
// #include <algorithm>
// #include <iostream>
// #include <boost/array.hpp>
// #include <boost/asio.hpp>
// #include <boost/bind.hpp>
// #include <boost/format.hpp>
// #include <boost/lambda/lambda.hpp>
// #include <boost/asio/error.hpp>
// #include <boost/asio/high_resolution_timer.hpp>
// #include <boost/asio/deadline_timer.hpp>

// using namespace boost::asio;
// using ip::udp;
// using namespace std;
// #define BUFFERSIZE 1024

// // class Mavlink_comms {
// // private:

// // public:
// //     Mavlink_comms(boost::asio::io_service& ioc) : resolver_(ioc), ws_(ioc) { }

// //     void run(char const *host, char const* port) {
// //         udp::resolver resolver (_io_service);
// //         udp::resolver::query query(udp:: v4 (), host, port);
// //         udp::resolver::iterator endpoint_iterator = resolver.resolve(query);
// //         udp::endpoint server_endpoint = (udp::endpoint) *resolver.resolve(query);
// //         udp::endpoint local_endpoint;
// //         boost::asio::ip::udp::socket client_socket(io_service, local_endpoint);
// //         sender_endpoint = *endpoint_iterator;

// //     }

// //     void handle_async_recv(const boost::system::error_code& error, std::size_t ) {
// //         if (error)
// //             std::cout << error.message() << std::endl;
// //         else {
// //             std::cout << "Datagram received" << std::endl;
// //         }
// //     }   

// //     void handle_async_send(boost::shared_ptr<std::string> /* message */, const boost::system::error_code& error, std::size_t ) {
// //         if (error)
// //             std::cout << error.message() << std::endl;
// //         else {
// //             std::cout << "Datagram sent" << std::endl;
// //         }
// //     }

// //     void recv_datagram() {
// //         client_socket.async_receive_from(
// //                 boost::asio::buffer(recv_buffer),
// //                 remote_endpoint,
// //                 boost::bind(&handle_async_recv, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
// //         );
// //     }

// //     void send_datagram() {
// //         client_socket.async_send_to(
// //                 boost::asio::buffer(*message),
// //                 server_endpoint,
// //                 boost::bind(&handle_async_send, message, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
// //         );
// //     }
// // };

// // class udp_server {
// // public:
// //     udp_server(boost::asio::io_service& io_service): socket_(io_service, udp::endpoint(udp::v4(), 13)) {}
// //     udp::socket socket_;
// //     udp::endpoint remote_endpoint_;
// //     boost::array<char, BUFFERSIZE> recv_buffer_;
// // private:
// //     void start_receive()
// //     {
// //         socket_.async_receive_from(
// //                 boost::asio::buffer(recv_buffer_), remote_endpoint_,
// //                 boost::bind(&udp_server::handle_receive, this,
// //                     boost::asio::placeholders::error));
// //     }

// //     void handle_receive(const boost::system::error_code& error)
// //     {
// //         if (!error || error == boost::asio::error::message_size)
// //         {
// //             boost::shared_ptr<std::string> message(new std::string(make_daytime_string()));

// //             socket_.async_send_to(boost::asio::buffer(*message), remote_endpoint_, boost::bind(&udp_server::handle_async_send, this, message));

// //             start_receive();
// //         }
// //     }

// //     void handle_async_send(boost::shared_ptr<std::string> /*message*/)
// //     {
// //     }

// // };
// class MavProto {
//     // private: 

// public:

//     // const ushort X25_INIT_CRC = 0xFFFF;
//     const uint8_t MAVLINK_START_OF_FRAME = 0xFE;
//     const uint8_t MAVLINK_2_START_OF_FRAME = 0xFD;
//     const int MAVLINK_HEADER_LENGTH = 6;
//     const int MAVLINK_2_HEADER_LENGTH = 10;
//     const uint8_t MAVLINK_DRONE_ARMED_MASK = 0x80;
//     int mavlink_header_length;
//     enum MavReportType
//     {
//         SystemReport = 0,
//         LosReport,
//         GndCrsReport,
//         SnapshotReport = 5,
//         SDCardReport,
//         VideoReport,
//         LosRateReport,
//         ObjectDetectionReport,
//         IMUReport,
//         FireDetectionReport,
//         TrackingReport,
//         LPRReport,
//         ARMarkerReport,
//         ParameterReport,
//         CarCountReport,
//         OGLRReport,
//         VMDReport,
//     };
//     enum CommType
//     {
//         CommOverIP,
//         CommOverSerial,
//     };
//     enum MavMountMode
//     {
//         Retract = 0,
//         Neutral,
//     };
//     enum ParameterReportParameterIds
//     {
//         VideoDestinationIP,
//         VideoDesinationPort,
//         BandwidthLimitEnable,
//         BandwidthLimit,
//         StreamBitrate,
//         RecordBitrate,
//         EthernetIPAddress,
//         EthernetSubnetMask,
//         EthernetGatewayIP,
//         EthernetMTU,
//         EnableRollDerotation,
//         EnableAutoRec,
//         AutoRecINSTimeout,
//         EnableAESEncryption,
//         AES128Key,
//         RemoteIP,
//         RemotePort,
//         LocalPort,
//         ExecuteMavlinkFlightPlan,
//         ForceRollToHorizon,
//         OSDMode,
//         OSDShowGndCrsInfo,
//         OSDShowCarftGPSInfo,
//         OSDShowCarftFlightMode,
//         OSDShowCarftAirSpeed,
//         OSDMinimapType,
//         VGAResizingThreshold,
//         VGAOutputMode,
//         ReportSystemID,
//         ReportComponentID,
//         AckSystemID,
//         AckComponentID,
//         EnableMavlinkSequenceNumber,
//         StreamGOPSize,
//         StreamQuantizationValue,
//         ObjectDetectionEnabled,
//         ObjectDetectionDetectorType,
//         ObjectDetectionAutoTrackEnabled,
//         ObjectDetectionAutoZoomOnTarget,
//         ObjectDetectionLoiterAPOnDetection,
//         ALPREnabled,
//         ALPRCountryProfile,
//         AILicenseStatus,
//     };

//     enum ParameterReportStatusArgs
//     {
//         StatusFail,
//         StatusSuccess,
//     };

//     /* Mavlink Command Constants */
//     const uint8_t MAVLINK_ATTITUDE_MSG_CRC = 0x27;
//     const uint8_t MAVLINK_GBL_POS_INT_CRC = 0x68;
//     const uint8_t MAVLINK_GPS_RAW_INT_CRC = 0x18;
//     const uint8_t MAVLINK_SYS_STAT_CRC = 0x7C;
//     const uint8_t MAVLINK_HEART_BEAT_CRC = 0x32;
//     const uint8_t MAVLINK_SYS_TIME_CRC = 0x89;
//     const uint8_t MAVLINK_CMD_LONG_MSGID = 0x4C;
//     const uint8_t MAVLINK_CMD_LONG_CRC = 0x98;
//     const uint8_t MAVLINK_CMD_LONG_MSG_LEN = 0x21;
//     const uint8_t MAVLINK_CMD_ACK_MSGID = 0x4D;
//     const uint8_t MAVLINK_CMD_ACK_CRC = 0x8F;
//     const uint8_t MAVLINK_CMD_ACK_MSG_LEN = 0x03;
//     const uint8_t MAVLINK_EXT_V2_MSGID = 0xF8;
//     const uint8_t MAVLINK_EXT_V2_MSG_CRC = 0x08;
//     const uint8_t MAVLINK_EXT_V2_SYS_REPORT_MSG_LEN = 0x3D;
//     const uint8_t MAVLINK_EXT_V2_LOS_REPORT_MSG_LEN = 0x36;
//     const uint8_t MAVLINK_EXT_V2_GND_REPORT_MSG_LEN = 0x12;
//     const uint8_t MAVLINK_EXT_V2_SDCARD_REPORT_MSG_LEN = 0x0B;
//     const uint8_t MAVLINK_EXT_V2_VIDEO_REPORT_MSG_LEN = 0x0A;
//     const uint8_t MAVLINK_EXT_V2_SNAPSHOT_REPORT_MSG_LEN = 0x33;
//     const uint8_t MAVLINK_EXT_V2_LOS_RATE_REPORT_MSG_LEN = 0x26;
//     const uint8_t MAVLINK_EXT_V2_IMU_REPORT_MSG_LEN = 0x4C;
//     const uint8_t MAVLINK_EXT_V2_FIRE_REPORT_MSG_LEN = 0x28;
//     const uint8_t MAVLINK_EXT_V2_TRACKING_REPORT_MSG_LEN = 0x3E;
//     const uint8_t MAVLINK_EXT_V2_PARAMETER_REPORT_MSG_LEN = 0x09;
//     const uint8_t MAVLINK_EXT_V2_OGLR_REPORT_MSG_LEN = 0x2A;
//     const uint8_t MAVLINK_EXT_V2_CAR_COUNT_REPORT_MSG_LEN = 0x12; 

    
// };


// struct SysReport
// {
//     float roll;
//     float pitch;
//     float fov;
//     uint8_t trackerStatus;
//     uint8_t recordingStatus;
//     uint8_t activeSensor;
//     uint8_t irSensorPolarity;
//     uint8_t systemMode;
//     uint8_t laserStatus;
//     short trackerROI_x;
//     short trackerROI_y;
//     float single_yaw_cmd;
//     uint8_t snapshot_busy;
//     float cpu_temp;
//     float camera_ver;
//     int   trip2_ver;
//     ushort bit_status;
//     uint8_t status_flags;
//     uint8_t camera_type;
//     float roll_rate;
//     float pitch_rate;
//     float cam_temp;
//     float roll_derot_angle;
// };

// enum MavCommands
// {
//     DO_SET_ROI_LOCATION,
//     DO_SET_ROI_NONE,
//     DO_MOUNT_CONTROL,
//     DO_DIGICAM_CONTROL
// };

// enum NvCommands
// {
//     Set_System_Mode,
//     Take_Snapshot,
//     Set_Rec_State,
//     Set_Sensor,
//     Set_FOV,
//     Set_Sharpness,
//     Set_Gimbal,
//     Do_BIT,
//     Set_IR_Polarity,
//     Set_SingleYaw_Mode,
//     Set_Follow_Mode,
//     Do_NUC,
//     Set_Report_Frequency,
//     Clear_Retract_Lock,
//     Set_System_Time,
//     Set_IR_Color,
//     Set_Joystick_Mode,
//     Set_Gnd_Crossing_Alt,
//     Set_Roll_Derotation,
//     Set_Laser,
//     Reboot_System,
//     Reconfigure_Video_BitRate,
//     Set_Tracking_Icon,
//     Set_Zoom,
//     Freeze_Video,
//     Pilot_View = 26,
//     RVT_Position,
//     Snapshot_Interval,
//     Update_Remote_IP,
//     Update_Video_IP,
//     Configuration_Cmd,
//     Erase_SD,
//     Set_Rate_Multiplyer,
//     Set_IR_Gain_Level,
//     Set_Vid_Stream_Tx_State,
//     Set_Day_White_Balance,
//     Set_Laser_Mode,
//     Set_Hold_Coordinate_Mode,
//     Update_Show_Cross_On_Idle,
//     Camera_Test,
//     Set_AI_Detection,
//     Resume_AI_Scan,
//     Set_Fly_Above,
//     Set_Camera_Stabilization = 50,
//     Update_Stream_Bitrate,
//     Do_Iperf_Test,
//     Set_Geo_AVG,
//     Detection_Control,
//     AR_Marker_Control,
//     Geo_Map_Control,
//     Stream_Control,
//     VMD_Control,
//     Multiple_GCS_Control,
//     Update_INS_Calibration,
//     Multiple_Trackers_Control,
// };

// enum NvSystemModes
// {
//     Stow,
//     Pilot,
//     HoldCoordinate,
//     Observation,
//     LocalPosition,
//     GlobalPosition,
//     GRR,
//     Tracking,
//     EPR,
//     Nadir,
//     Nadir_Scan,
//     TwoDScan,
//     PTC,
//     UnstzbilizedPosition,
// };

// class Mavlink_CameraProto {

// };

// class Client
// {
// private:
// 	std::string m_version = "0.2.11";
// 	int control_interface;

// 	bool m_verbose_debug = false;
// 	bool m_connected_status = false;
// 	// serial camera 
// 	std::string serial_name;
// 	int baud_rate;
// 	int serial_port;

// 	// MAVLINK CAMERA 
// 	int mavlink_protocol_ver = 2;
//     int mavlink_header_length;
//     uint8_t mavlink_start_of_frame;
//     int mavlink_msg_id_location;
//     const char* upd_addess;
//     int udpport;
// 	/// 
// 	int camera_interval;
// 	// Connection details
// 	std::string m_ip_address;
// 	int m_target_port;
// 	int m_connection_priority;
// 	// Connection context
// 	boost::asio::io_service &m_io_service;
// 	udp::socket m_active_socket_udp;
// 	// Internal setup
// 	std::vector<std::vector<float>> m_lut;
// 	mutable std::mutex m_mutex;
// 	deadline_timer m_deadline_timer;
// 	int current_camera;
// 	// new parse response
// 	uint16_t checksum;
// 	uint8_t data1;
// 	uint8_t data2;
// 	uint16_t data;
// 	// DataPanTiltZoom ptz;
// 	// ParsingState ps;
// 	// new parse response 3
// 	std::vector<float> vdata;
// 	std::string _data;
// 	std::string ros;
// 	std::string cmd;
// 	// ParsingState3 ps3;

// public:
// 	Client(boost::asio::io_service& ios, std::string ip, int port, int priority = 10, std::string lut_address = "sim", bool verbose = false); // Constructor
// 	~Client(); // Destructor
// 	void setVerbose(bool verbose);
// 	int connectToManager(const std::string& ip, int port, int priority);

// 	// camera serial 
// 	// void setSerial(std::string serial_name_, int baud_rate_);
// 	// void updateInterface(int control_interface_) {
// 	// 	control_interface = control_interface_;
// 	// };
// 	// int initConnectionSerial();
// 	// bool check_connection_status();
// 	void sendUdpNoRes(uint8_t* tx_packet);

// };


/**
 * @file mavlink_vizgard.h
 *
 * @brief Serial, UDP interface definition
 *
 * Functions for opening, closing, reading and writing via serial and UDP ports
 *
 * @author longvu
 */
#ifndef MAVLINK_VIZGARD_H_
#define MAVLINK_VIZGARD_H_
#define FAIL_CONNECT 1
#include <cstdlib>
#include <stdio.h>   // Standard input/output definitions
#include <unistd.h>  // UNIX standard function definitions
#include <fcntl.h>   // File control definitions
#include <termios.h> // POSIX terminal control definitions
#include <pthread.h> // This uses POSIX Threads
#include <signal.h>
#include <mutex>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <math.h>

#if (defined __QNX__) | (defined __QNXNTO__)
/* QNX specific headers */
#include <unix.h>
#else
/* Linux / MacOS POSIX timer headers */
#include <sys/time.h>
#include <time.h>
#include <arpa/inet.h>
#include <stdbool.h> /* required for the definition of bool in C99 */
#endif
#include <iostream>
#include <string>
#include <vector>
#define BUFFER_LENGTH 2041 
#define BUFFER_CAMERA_LENGTH 1024 

class MavProto {
    // private: 
public:

    const ushort X25_INIT_CRC = 0xFFFF;
    const uint8_t MAVLINK_START_OF_FRAME = 0xFE;
    const uint8_t MAVLINK_2_START_OF_FRAME = 0xFD;
    const int MAVLINK_HEADER_LENGTH = 6;
    const int MAVLINK_2_HEADER_LENGTH = 10;
    const uint8_t MAVLINK_DRONE_ARMED_MASK = 0x80;
    int mavlink_header_length;
    enum MavReportType
    {
        SystemReport = 0,
        LosReport,
        GndCrsReport,
        SnapshotReport = 5,
        SDCardReport,
        VideoReport,
        LosRateReport,
        ObjectDetectionReport,
        IMUReport,
        FireDetectionReport,
        TrackingReport,
        LPRReport,
        ARMarkerReport,
        ParameterReport,
        CarCountReport,
        OGLRReport,
        VMDReport,
    };
    enum CommType
    {
        CommOverIP,
        CommOverSerial,
    };
    enum MavMountMode
    {
        Retract = 0,
        Neutral,
    };
    enum ParameterReportParameterIds
    {
        VideoDestinationIP,
        VideoDesinationPort,
        BandwidthLimitEnable,
        BandwidthLimit,
        StreamBitrate,
        RecordBitrate,
        EthernetIPAddress,
        EthernetSubnetMask,
        EthernetGatewayIP,
        EthernetMTU,
        EnableRollDerotation,
        EnableAutoRec,
        AutoRecINSTimeout,
        EnableAESEncryption,
        AES128Key,
        RemoteIP,
        RemotePort,
        LocalPort,
        ExecuteMavlinkFlightPlan,
        ForceRollToHorizon,
        OSDMode,
        OSDShowGndCrsInfo,
        OSDShowCarftGPSInfo,
        OSDShowCarftFlightMode,
        OSDShowCarftAirSpeed,
        OSDMinimapType,
        VGAResizingThreshold,
        VGAOutputMode,
        ReportSystemID,
        ReportComponentID,
        AckSystemID,
        AckComponentID,
        EnableMavlinkSequenceNumber,
        StreamGOPSize,
        StreamQuantizationValue,
        ObjectDetectionEnabled,
        ObjectDetectionDetectorType,
        ObjectDetectionAutoTrackEnabled,
        ObjectDetectionAutoZoomOnTarget,
        ObjectDetectionLoiterAPOnDetection,
        ALPREnabled,
        ALPRCountryProfile,
        AILicenseStatus,
    };

    enum ParameterReportStatusArgs
    {
        StatusFail,
        StatusSuccess,
    };

    /* Mavlink Command Constants */
    const uint8_t MAVLINK_ATTITUDE_MSG_CRC = 0x27;
    const uint8_t MAVLINK_GBL_POS_INT_CRC = 0x68;
    const uint8_t MAVLINK_GPS_RAW_INT_CRC = 0x18;
    const uint8_t MAVLINK_SYS_STAT_CRC = 0x7C;
    const uint8_t MAVLINK_HEART_BEAT_CRC = 0x32;
    const uint8_t MAVLINK_SYS_TIME_CRC = 0x89;
    const uint8_t MAVLINK_CMD_LONG_MSGID = 0x4C;
    const uint8_t MAVLINK_CMD_LONG_CRC = 0x98;
    const uint8_t MAVLINK_CMD_LONG_MSG_LEN = 0x21;
    const uint8_t MAVLINK_CMD_ACK_MSGID = 0x4D;
    const uint8_t MAVLINK_CMD_ACK_CRC = 0x8F;
    const uint8_t MAVLINK_CMD_ACK_MSG_LEN = 0x03;
    const uint8_t MAVLINK_EXT_V2_MSGID = 0xF8;
    const uint8_t MAVLINK_EXT_V2_MSG_CRC = 0x08;
    const uint8_t MAVLINK_EXT_V2_SYS_REPORT_MSG_LEN = 0x3D;
    const uint8_t MAVLINK_EXT_V2_LOS_REPORT_MSG_LEN = 0x36;
    const uint8_t MAVLINK_EXT_V2_GND_REPORT_MSG_LEN = 0x12;
    const uint8_t MAVLINK_EXT_V2_SDCARD_REPORT_MSG_LEN = 0x0B;
    const uint8_t MAVLINK_EXT_V2_VIDEO_REPORT_MSG_LEN = 0x0A;
    const uint8_t MAVLINK_EXT_V2_SNAPSHOT_REPORT_MSG_LEN = 0x33;
    const uint8_t MAVLINK_EXT_V2_LOS_RATE_REPORT_MSG_LEN = 0x26;
    const uint8_t MAVLINK_EXT_V2_IMU_REPORT_MSG_LEN = 0x4C;
    const uint8_t MAVLINK_EXT_V2_FIRE_REPORT_MSG_LEN = 0x28;
    const uint8_t MAVLINK_EXT_V2_TRACKING_REPORT_MSG_LEN = 0x3E;
    const uint8_t MAVLINK_EXT_V2_PARAMETER_REPORT_MSG_LEN = 0x09;
    const uint8_t MAVLINK_EXT_V2_OGLR_REPORT_MSG_LEN = 0x2A;
    const uint8_t MAVLINK_EXT_V2_CAR_COUNT_REPORT_MSG_LEN = 0x12; 

    
};

struct Mavlink_Data_Query {
    std::string time_;
    int32_t lat =0;
    int32_t lon = 0;
    int32_t attitude =0;
    float yaw = 0;
    float pitch = 0;
    float roll = 0;
    float speed = 0;
    uint16_t heading = 0;
}; 

void reset_struct(Mavlink_Data_Query &mavlink_data_query) ;

struct SysReport
{
    float roll;
    float pitch;
    float fov;
    uint8_t trackerStatus;
    uint8_t recordingStatus;
    uint8_t activeSensor;
    uint8_t irSensorPolarity;
    uint8_t systemMode;
    uint8_t laserStatus;
    short trackerROI_x;
    short trackerROI_y;
    float single_yaw_cmd;
    uint8_t snapshot_busy;
    float cpu_temp;
    float camera_ver;
    int   trip2_ver;
    ushort bit_status;
    uint8_t status_flags;
    uint8_t camera_type;
    float roll_rate;
    float pitch_rate;
    float cam_temp;
    float roll_derot_angle;
};

enum MavCommands
{
    DO_SET_ROI_LOCATION,
    DO_SET_ROI_NONE,
    DO_MOUNT_CONTROL,
    DO_DIGICAM_CONTROL
};

enum NvCommands
{
    Set_System_Mode,
    Take_Snapshot,
    Set_Rec_State,
    Set_Sensor,
    Set_FOV,
    Set_Sharpness,
    Set_Gimbal,
    Do_BIT,
    Set_IR_Polarity,
    Set_SingleYaw_Mode,
    Set_Follow_Mode,
    Do_NUC,
    Set_Report_Frequency,
    Clear_Retract_Lock,
    Set_System_Time,
    Set_IR_Color,
    Set_Joystick_Mode,
    Set_Gnd_Crossing_Alt,
    Set_Roll_Derotation,
    Set_Laser,
    Reboot_System,
    Reconfigure_Video_BitRate,
    Set_Tracking_Icon,
    Set_Zoom,
    Freeze_Video,
    Pilot_View = 26,
    RVT_Position,
    Snapshot_Interval,
    Update_Remote_IP,
    Update_Video_IP,
    Configuration_Cmd,
    Erase_SD,
    Set_Rate_Multiplyer,
    Set_IR_Gain_Level,
    Set_Vid_Stream_Tx_State,
    Set_Day_White_Balance,
    Set_Laser_Mode,
    Set_Hold_Coordinate_Mode,
    Update_Show_Cross_On_Idle,
    Camera_Test,
    Set_AI_Detection,
    Resume_AI_Scan,
    Set_Fly_Above,
    Set_Camera_Stabilization = 50,
    Update_Stream_Bitrate,
    Do_Iperf_Test,
    Set_Geo_AVG,
    Detection_Control,
    AR_Marker_Control,
    Geo_Map_Control,
    Stream_Control,
    VMD_Control,
    Multiple_GCS_Control,
    Update_INS_Calibration,
    Multiple_Trackers_Control,
};

enum NvSystemModes
{
    Stow,
    Pilot,
    HoldCoordinate,
    Observation,
    LocalPosition,
    GlobalPosition,
    GRR,
    Tracking,
    EPR,
    Nadir,
    Nadir_Scan,
    TwoDScan,
    PTC,
    UnstzbilizedPosition,
};


class Mavlink_Camera : public MavProto {
private: 
    std::string serialport_in;
    int connection_type = 0;
public: 
    int mavlink_protocol_ver = 2;
    int mavlink_header_length;
    uint8_t mavlink_start_of_frame;
    int mavlink_msg_id_location;
    const char* upd_addess;
    int udpport;
    MavProto mavproto;
    struct sockaddr_in gcAddr; 
    struct sockaddr_in locAddr;
    uint8_t buf[BUFFER_LENGTH];
    uint8_t buf1[BUFFER_CAMERA_LENGTH];
    int sock;

    bool is_conneted = false;
    bool connect_udp_protol(const char* upd_addess, int udpport);
    // MAVLINK CAMERA STUFF 
    void MavCom_read_Message();
    void MavComm_Send_Message(uint8_t* packet);

    void doSetRoi(uint8_t packet);
    void doDigicamControl(uint8_t buf_send);
    
    std::vector<uint8_t> MavlinkDigiCamMsg(float os_cmd, float os_p1, float os_p2, float os_p3, float os_p4, float os_p5, float os_p6 );
    std::vector<uint8_t> MavlinkAttitudeMsg(float roll, float pitch, float yaw);
    std::vector<uint8_t> MavlinkGlobalPosIntMsg(float lat, float lon, int alt, int rel_alt, float vx, float vy, float vz);
    std::vector<uint8_t> MavlinkGPSRawIntMsg( int sat_count);
    std::vector<uint8_t> MavlinkSysStatusMsg( float batt_voltage);
    std::vector<uint8_t> MavlinkHeartbeatMsg( bool drone_armed);


    ushort MavlinkCrcCalculate(uint8_t* pBuffer, int length);
    ushort MavlinkCrcAccumulate(uint8_t b, ushort crc);

    void SetMavProtoValuesByVersion() {
        if (mavlink_protocol_ver == 2) {
            mavlink_header_length = this->MAVLINK_2_HEADER_LENGTH;
            mavlink_start_of_frame = this->MAVLINK_2_START_OF_FRAME;
            mavlink_msg_id_location = this->MAVLINK_2_HEADER_LENGTH - 3;
        }
    }

};






#endif // SERIAL_PORT_H_